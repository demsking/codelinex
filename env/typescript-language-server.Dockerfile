FROM node:lts-slim

LABEL version="1.0.0"
LABEL description="TypeScript & JavaScript Language Server for CodeLineX"
LABEL maintainer="demsking@gmail.com"

RUN npm install -g typescript-language-server typescript

CMD ["/usr/local/bin/typescript-language-server", "--stdio", "--log-level", "1"]
