<?php

$description = "This is a example of strpos method to search string using strpos.";
$pos = strpos($description, "strpos");

//$pos is false when the string is not found.
//$pos === 0 means string is found at position 0.
//Always check for false condition not 0.
if( $pos === false ){
    echo "Confirmed! The String is not found" . PHP_EOL;
}else{
    echo "String is found at Position: $pos" . PHP_EOL;
}

//Case in-sensitive search
$pos = stripos($description, "STRPOS");

//stripos - i indicates case-insensitive.
if( $pos === false ){
    echo "Confirmed! The String is not found" . PHP_EOL;
}else{
    echo "String is found at Position: $pos" . PHP_EOL;
}

//strrpos() - Find the position of the LAST occurrence of a substring in a string
$pos = strrpos($description, "strpos");

//strrpos() - search the last position
if( $pos === false ){
    echo "Confirmed! The String is not found" . PHP_EOL;
}else{
    echo "String is found at Position: $pos" . PHP_EOL;
}

//strripos() - Find the position of the LAST occurrence of a substring in a string
$pos = strripos($description, "STRPOS");

//strripos() - search the last position
if( $pos === false ){
    echo "Confirmed! The String is not found" . PHP_EOL;
}else{
    echo "String is found at Position: $pos" . PHP_EOL;
}
