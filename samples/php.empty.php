<?php

$firstname = "John";
$lastname = "";
$address = null;

echo "FirstName: $firstname and  LastName: $lastname and Address: $address" . PHP_EOL;

if( empty( $firstname ) ) {
    echo "Input value FirstName: $firstname is Empty" . PHP_EOL;
}else{
    echo "Input value FirstName: $firstname is NOT Empty" . PHP_EOL;
}


if( empty( $lastname ) ) {
    echo "Input value LastName: $lastname is Empty" . PHP_EOL;
}else{
    echo "Input value LastName: $lastname is NOT Empty" . PHP_EOL;
}

if( empty( $address ) ) {
    echo "Input value Address: $address is Empty" . PHP_EOL;
}else{
    echo "Input value Address: $address is NOT Empty" . PHP_EOL;
}
