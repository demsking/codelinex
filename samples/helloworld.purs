module Main where

import Prelude

import Control.Monad.Eff (Eff)
import Data.Foldable (fold)
import TryPureScript (DOM, h1, h2, p, text, list, indent, link, render, code)

main :: Eff (dom :: DOM) Unit
main =
    render $ fold
      [ h1 (text "Try PureScript!")
      , p (text "Try out the examples below, or create your own!")
      , h2 (text "Examples")
      , list (map fromExample examples)
      , h2 (text "Try PureScript Libraries")
      , list [ link "?backend=thermite" (text "Try Thermite") 
               <> text ", a front-end library for PureScript which uses React"
             , link "?backend=slides" (text "Try Slides") 
               <> text ", an EDSL in PureScript for creating presentations"
             , link "?backend=flare" (text "Try Flare") 
               <> text ", a special-purpose, reactive UI library"
             , link "?backend=mathbox" (text "Try Mathbox") 
               <> text ", a PureScript wrapper for the "
               <> link "https://gitgud.io/unconed/mathbox" (text "Mathbox")
               <> text " JavaScript library"
             , link "?backend=behaviors" (text "Try purescript-behaviors") 
               <> text ", a functional reactive programming library"
             ]
      , h2 (text "Share Your Code")
      , p (text "Code can be loaded from a GitHub Gist. To share code, simply include the Gist ID in the URL as follows:")
      , indent (p (code (text "  try.purescript.org?gist=gist-id")))
      , p (fold
          [ text "The Gist should contain a file named "
          , code (text "Main.purs")
          , text " containing your PureScript code."
          ])
      ]
  where
    fromExample { title, gist } =
      link ("?gist=" <> gist) (text title)

    examples =
      [ { title: "Algebraic Data Types"
        , gist: "37c3c97f47a43f20c548"
        }
      , { title: "Loops"
        , gist: "cfdabdcd085d4ac3dc46"
        }
      , { title: "Operators"
        , gist: "3044550f29a7c5d3d0d0"
        }
      , { title: "Records"
        , gist: "b80be527ada3eab47dc5"
        }
      , { title: "Recursion"
        , gist: "ff49cc7dc85923a75613"
        }
      , { title: "Do Notation"
        , gist: "47c2d9913c5dbda1e963"
        }
      , { title: "Type Classes"
        , gist: "1a3b845e8c6defde659a"
        }
      , { title: "Generic Programming"
        , gist: "3f735aa2a652af592101"
        }
      , { title: "QuickCheck"
        , gist: "69f7f94fe4ff3bd47f4b"
        }
      ]