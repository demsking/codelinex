# CodeLineX

The powerful lightweight editor for the Web

![CodeLineX Screenshot](https://gitlab.com/demsking/codelinex/raw/dev/screenshots/Multiple%20Views.png)

## Features

**Application Features**
- window splitting (horizontal & vertical)
- multi-document interface (MDI)
- project session support

**General Features**
- encoding support (Unicode and lots of others)
- bi-directional text rendering support
- line ending support (Windows, Unix, Mac), including auto detection
- network transparency (open and edit remote files)

**Advanced Editor Features**
- bookmarking system
- scroll bar marks
- line modification indicators
- code folding
- symbols outline

**Syntax Highlighting**
- highlighting support for close to 300 languages
- bracket matching
- smart on-the-fly spell checking
- highlighting of selected words

**Programming Features**
- auto indentation
- smart comment and uncomment handling
- auto completion with argument hints
- vi input mode
- rectangular block selection mode

**Search & Replace**
- incremental search, also known as “find as you type”
- support for multiline search & replace
- regular expression support
<!-- - search & replace in multiple opened files or files on disk -->

**Backup and Restore**
- backups on save
- swap files to recover data on system crash
- undo / redo system

## Build

```sh
# Create the codeline:ubuntu image
docker build . -t codeline:ubuntu

# Compile Setup
mkdir build
docker run -it --rm -v $(pwd):/home/dev/codeline --workdir /home/dev/codeline/build codeline:ubuntu cmake ..

# Compile CodeLine
docker run -it --rm -v $(pwd):/home/dev/codeline --workdir /home/dev/codeline/build codeline:ubuntu make

# Packaging
docker run -it --rm -v $(pwd):/home/dev/codeline --workdir /home/dev/codeline/build codeline:ubuntu make pack
```

## Contribute

Contributions to CodeLineX are welcome. Here is how you can contribute to CodeLineX:

1. [Submit bugs or a feature request](https://gitlab.com/demsking/codelinex/issues) and help us verify fixes as they are checked in
2. Write code for a bug fix or for your new awesome feature
3. Write test cases for your changes
4. [Submit merge requests](https://gitlab.com/demsking/codelinex/merge_requests) for bug fixes and features and discuss existing proposals

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner, and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Copyright (C) 2018 Sébastien Demanou.

Under the GNU General Public License.

Everyone is permitted to copy, modify and distribute CodeLineX. See
[LICENSE](https://gitlab.com/demsking/codelinex/raw/dev/LICENSE) file for more details.
