/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QDir>
#include <QPointer>
#include <QSharedPointer>
#include <QMainWindow>

#include <KSharedConfig>

#include "core/StateSession.h"
#include "components/Document.h"

#define APPLICATION_NAME "CodeLineX"

class QFileSystemWatcher;

namespace CodeLineX {
  class EditorTabview;
  class QuickOpenView;
  class SwitchFileView;
  class EditorLayout;
  class Overlay;

  namespace Core {
    class ActionCollection;
  }

  namespace Layout {
    class Shell;
  }

  class MainWindow: public QMainWindow, public Core::StateSession {
    Q_OBJECT

    KSharedConfigPtr _config;

    Layout::Shell* _shell = nullptr;

    QuickOpenView* _quickOpenView = nullptr;
    SwitchFileView* _switchFileView = nullptr;

    bool _menuBarDisplayed = true;
    bool _showPathInTitlebar = false;
    QRect _windowGeometryFullscreenCache;

    Core::ActionCollection* _actionCollection = nullptr;

    QString _workspaceDir = QDir::homePath() + QDir::separator() + "Workspace";
    QString _themeFile = QDir::currentPath() + QDir::separator()
      + "themes" + QDir::separator() + "dark.css";

    QStringList _projects;

    QFileSystemWatcher* _themeWatcher = nullptr;

    struct ActionDescription {
      bool isSeparator = false;
      const char* icon = nullptr;
      const char* name = nullptr;
      Document::StandardAction action;
      QKeySequence key;
    };

    public:
      MainWindow();
      ~MainWindow() override;

      QString editorSchema();
      QList<Document*> modifiedDocuments() const;

    public slots:
      void focusCurrentDocument();

    public slots:
      void setTitle(const QString&);
      void setEditorSchema(const QString&);
      void setCurrentDocument(Document*);
      void applyDisplayMenu();
      void openUrl(const QUrl&);
      void showCopyFilePathContextMenu(const QPoint&, const QUrl&);
      void showFileContextMenu(const QPoint&, const QUrl&);

    protected slots:
      void updateActionsOnModification(bool modifier);
      void updateSplitActionsOnViewCountChange();

    protected:
      inline void createActions();
      inline void createGlobalActions();
      inline QAction* actionMenu(ActionDescription&);
      inline void createActionsMenu(QMenu*, QList<ActionDescription>);
      inline void createProjectActions();
      inline void createFileActions();
      inline void createEditActions();
      inline void createViewActions();
      inline void createToolsActions();
      inline void createWindowActions();
      inline void createSettingsActions();
      inline void createHelpActions();

    protected:
      void setFileMenuActions(QMenu*, bool enableSaveEncoding = true, bool enableGlobalAction = false);
      void setFileMenuCopyActions(QMenu*, const QUrl&);
      void setFileMenuCloseActions(QMenu*, bool enableGlobalAction = false);
      void closeEvent(QCloseEvent*) override;

    protected slots:
      void toogleFullscreen();
      void loadTheme();
      void loadSchema(const QString&);

      void onViewCreate(EditorTabview*);

    protected:
      inline void saveCurrentSettings();
      inline QString projectrc(QDir projectDir) const;
      inline void openProject(const QString& path);
      inline void restore();
      inline void saveCurrentProject();
      inline bool closeCurrentProject();

      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;
  };
}  // namespace CodeLineX

#endif  // MAINWINDOW_H_
