/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entities/GenericUnixConfigFile.h"

#include <fstream>
#include <sstream>

namespace CodeLineX {
  namespace Entity {
    GenericUnixConfigFile::GenericUnixConfigFile(const std::string& filename, bool ignoreComments) {
      std::ifstream ifs(filename);
      std::string line;

      while (std::getline(ifs, line)) {
        if (line.empty()) {
          continue;
        }

        if (ignoreComments && line == "#") {
          continue; // Ignore comment lines
        }

        _entries.push_back(line);
      }
    }

    const GenericUnixConfigEntries& GenericUnixConfigFile::entries() const {
      return _entries;
    }
  }  // namespace Entity
}  // namespace CodeLineX
