/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENTITIES_ABSTRACTPROJECT_H_
#define ENTITIES_ABSTRACTPROJECT_H_

#include <string>

namespace CodeLineX {
  namespace Entity {
    namespace Private {
      class AbstractProjectIgnoreFiles;
    }

    typedef Private::AbstractProjectIgnoreFiles* AbstractProjectIgnoreFilesPtr;

    class AbstractProject {
      std::string _name;
      std::string _path;

      protected:
        Private::AbstractProjectIgnoreFiles* _ignoreFiles = nullptr;

      protected:
        explicit AbstractProject(const std::string& projectPath);

      public:
        AbstractProject(AbstractProject const&) = delete;
        void operator=(AbstractProject const&) = delete;

        const std::string& name() const;
        const std::string& path() const;
        std::string path(const std::string&) const;
        AbstractProjectIgnoreFilesPtr ignoreFiles() const;

        void setName(const std::string& name);
        void setPath(const std::string& path);
    };

    typedef AbstractProject* AbstractProjectPtr;
  }  // namespace Entity
}  // namespace CodeLineX

#endif  // ENTITIES_ABSTRACTPROJECT_H_
