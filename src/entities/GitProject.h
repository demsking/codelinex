/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENTITIES_GITPROJECT_H_
#define ENTITIES_GITPROJECT_H_

#include <string>

#include "AbstractProject.h"

namespace CodeLineX {
  namespace Entity {
    class GitProject: public AbstractProject {
      public:
        explicit GitProject(const std::string& projectPath);
    };
  }  // namespace Entity
}  // namespace CodeLineX

#endif  // ENTITIES_GITPROJECT_H_
