/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entities/AbstractProject.h"

#include <string>

#include "utils/FileSystemUtils.h"

namespace CodeLineX {
  namespace Entity {
    AbstractProject::AbstractProject(const std::string& projectPath)
      : _path(projectPath) {}

    const std::string& AbstractProject::name() const {
      return _name;
    }

    const std::string& AbstractProject::path() const {
      return _path;
    }

    std::string AbstractProject::path(const std::string& filename) const {
      return FileSystemUtils::join({ _path, filename });
    }

    AbstractProjectIgnoreFilesPtr AbstractProject::ignoreFiles() const {
      return _ignoreFiles;
    }

    void AbstractProject::setPath(const std::string& path) {
      _path = path;
    }

    void AbstractProject::setName(const std::string& name) {
      _name = name;
    }
  }  // namespace Entity
}  // namespace CodeLineX
