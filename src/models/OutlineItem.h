/**
 * HPX Stacked Panel - A fully stylized StackedWidget implementation
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELS_OUTLINEITEM_H_
#define MODELS_OUTLINEITEM_H_

#include <QStandardItem>
#include <KTextEditor/Range>

namespace CodeLineX {
  class OutlineItem: public QStandardItem {
    public:
      typedef KTextEditor::Range Range;

      enum Role {
        NameRole = Qt::UserRole + 1000,
        RangeRole
      };

      OutlineItem() = default;

      QString name() const;
      void setName(const QString&);

      Range range() const;
      void setRange(const Range&);
  };
} // namespace CodeLineX

#endif // MODELS_OUTLINEITEM_H_
