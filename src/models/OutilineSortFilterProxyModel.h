/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELS_OUTILINESORTFILTERPROXYMODEL_H_
#define MODELS_OUTILINESORTFILTERPROXYMODEL_H_

#include <QSortFilterProxyModel>
#include <KTextEditor/Range>

#include "models/OutlineItem.h"

namespace CodeLineX {
  class OutilineSortFilterProxyModel: public QSortFilterProxyModel {
    public:
      explicit OutilineSortFilterProxyModel(QObject* parent = nullptr);

    protected:
      bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
  };
} // namespace CodeLineX

#endif // MODELS_OUTILINESORTFILTERPROXYMODEL_H_
