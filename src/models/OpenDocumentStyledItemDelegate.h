/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELS_OPENDOCUMENTSTYLEDITEMDELEGATE_H_
#define MODELS_OPENDOCUMENTSTYLEDITEMDELEGATE_H_

#include <QStyledItemDelegate>

class QSize;
class QPainter;
class QModelIndex;
class QStyleOptionViewItem;

namespace CodeLineX {
  class OpenDocumentStyledItemDelegate: public QStyledItemDelegate {
    Q_OBJECT

    int _paddingLeft = 10;
    int _paddingRight = 10;
    int _buttonWidth = 24;

    QStyle::State _state = QStyle::State_Enabled;

    public:
      explicit OpenDocumentStyledItemDelegate(QObject* parent = nullptr);

    private:
      void paint(QPainter*, const QStyleOptionViewItem&, const QModelIndex&) const override;

    protected:
      bool editorEvent(
        QEvent*,
        QAbstractItemModel*,
        const QStyleOptionViewItem&,
        const QModelIndex&) override;

    signals:
      void buttonClicked(const QModelIndex &index);
  };
}  // namespace CodeLineX

#endif  // MODELS_OPENDOCUMENTSTYLEDITEMDELEGATE_H_
