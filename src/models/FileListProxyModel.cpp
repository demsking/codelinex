/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/FileListProxyModel.h"

#include <QModelIndex>

#include "models/FileItem.h"

namespace CodeLineX {
  FileListProxyModel::FileListProxyModel(QObject* parent): QSortFilterProxyModel(parent) {
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    setRecursiveFilteringEnabled(true);
  }

  void FileListProxyModel::addIgnoreWildcards(const QStringList& wildcards) {
    _ignoreWildcardsRe.setPattern(wildcards.join("|"));
  }

  bool FileListProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
    auto index = sourceModel()->index(sourceRow, FileItem::Role::DESC, sourceParent);
    auto relativePath = index.model()->data(index).toString();

    return !relativePath.contains(_ignoreWildcardsRe) && relativePath.contains(filterRegExp());
  }
}  // namespace CodeLineX
