/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELS_FILELISTMODEL_H_
#define MODELS_FILELISTMODEL_H_

#include <QDir>
#include <QStandardItemModel>

#include "FileItem.h"

namespace HPX {
  class TabviewItem;
}

namespace CodeLineX {
  class Document;
  class UnloadDocument;

  class FileListModel: public QStandardItemModel {
    Q_OBJECT

    public:
      explicit FileListModel(QObject* parent = nullptr);

      int findRow(const QString &text, FileItem::Role);
      void appendRow(const QUrl& url, const QDir& base = QDir::root());
      void appendRow(Document*, const QDir& base = QDir::root());
      void appendRow(HPX::TabviewItem*, const QDir& base = QDir::root());
      void appendRow(UnloadDocument*, const QDir& base = QDir::root());

    protected:
      void connectDocument(Document* document,
                           QStandardItem* titleItem,
                           QStandardItem* descItem,
                           QStandardItem* urlItem,
                           const QDir& base);

    protected slots:
      void onDocumentFocusIn(Document*);
      void onDocumentClosed(Document*);
  };
}  // namespace CodeLineX

#endif  // MODELS_FILELISTMODEL_H_
