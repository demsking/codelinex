/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/OpenDocumentStyledItemDelegate.h"

#include <QUrl>
#include <QPainter>
#include <QApplication>
#include <QMouseEvent>

#include "models/OpenDocumentListModel.h"

namespace CodeLineX {
  OpenDocumentStyledItemDelegate::OpenDocumentStyledItemDelegate(QObject* parent): QStyledItemDelegate(parent) {
  }

  void OpenDocumentStyledItemDelegate::paint(
    QPainter* painter,
    const QStyleOptionViewItem& option,
    const QModelIndex& index
  ) const {
    auto indexModel = index.model()->index(index.row(), 0);
    auto name = indexModel.data(OpenDocumentListModel::Role::DOCUMENT_NAME).toString();

    QStyleOptionViewItem opt = option;

    initStyleOption(&opt, index);

    opt.text = "";
    opt.textElideMode = Qt::TextElideMode::ElideMiddle;

    QStyle* style = opt.widget ? opt.widget->style() : QApplication::style();

    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);

    auto nameRect(option.rect);
    auto top = index.row() * option.rect.height();

    nameRect.setTop(top);
    nameRect.setLeft(_paddingLeft);
    nameRect.setHeight(option.rect.height());
    nameRect.setWidth(option.rect.width() - _buttonWidth - _paddingRight);

    auto buttonRect(nameRect);

    buttonRect.setTop(top + 2);
    buttonRect.setLeft(nameRect.width() + 8);
    buttonRect.setRight(nameRect.right() + _buttonWidth);
    buttonRect.setHeight(option.rect.height() - 4);
    buttonRect.setWidth(_buttonWidth);

    QStyleOptionButton button;

    button.rect = buttonRect;
    button.icon = QIcon::fromTheme(QStringLiteral("close-document"));
    button.state = _state | QStyle::State_Enabled;
    button.palette = QPalette(QColor(0, 0, 0, 0));
    button.features = QStyleOptionButton::Flat;

    button.type = option.state & (QStyle::State_MouseOver | QStyle::State_Selected)
      ? QStyleOptionButton::SO_Button
      : QStyleOptionButton::SO_Default;

    QApplication::style()->drawItemText(painter, nameRect, Qt::AlignLeft, QPalette(), true, name);
    QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);
  }

  bool OpenDocumentStyledItemDelegate::editorEvent(
    QEvent* event,
    QAbstractItemModel* model,
    const QStyleOptionViewItem& option,
    const QModelIndex& index
  ) {
    Q_UNUSED(model)

    if (event->type() != QEvent::MouseButtonPress && event->type() != QEvent::MouseButtonRelease) {
      // ignoring other mouse event and reseting button's state
      _state = QStyle::State_Raised;

      return true;
    }

    QRect buttonRect(option.rect);
    auto top = index.row() * option.rect.height();

    buttonRect.setTop(top + 2);
    buttonRect.setLeft((option.rect.right() - _buttonWidth) + 8);
    buttonRect.setHeight(option.rect.height() - 4);
    buttonRect.setWidth(_buttonWidth);

    QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);

    if (!buttonRect.contains(mouseEvent->pos())) {
      _state = QStyle::State_Raised;

      return true;
    }

    if (event->type() == QEvent::MouseButtonPress) {
      _state = QStyle::State_Sunken;
    } else if (event->type() == QEvent::MouseButtonRelease) {
      _state = QStyle::State_Raised;

      emit buttonClicked(index);
    }

    return true;
  }
}  // namespace CodeLineX
