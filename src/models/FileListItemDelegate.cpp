/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/FileListItemDelegate.h"

#include <QPainter>
#include <QApplication>

#include "models/FileItem.h"

namespace CodeLineX {
  void FileListItemDelegate::paint(
    QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index
  ) const {
    QModelIndex nameIndex = index.model()->index(index.row(), FileItem::Role::NAME);
    QModelIndex relativePathIndex = index.model()->index(index.row(), FileItem::Role::DESC);

    auto name = index.model()->data(nameIndex).toString();
    auto relativePath = index.model()->data(relativePathIndex).toString();

    QStyleOptionViewItem opt = option;
    initStyleOption(&opt, index);

    // draw correct background
    opt.text = "";
    opt.textElideMode = Qt::TextElideMode::ElideMiddle;

    QStyle* style = opt.widget ? opt.widget->style() : QApplication::style();

    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);

    auto firstLineRect = option.rect;

    firstLineRect.setTop(option.rect.top() + _paddingTop);
    firstLineRect.setLeft(_paddingLeft);
    firstLineRect.setHeight(option.rect.height() / 2);
    firstLineRect.setWidth(option.rect.width() - (firstLineRect.left() * 2));

    auto secondLineRect = firstLineRect;

    secondLineRect.setTop(firstLineRect.top() + firstLineRect.height() - _paddingTop);
    secondLineRect.setHeight(firstLineRect.height() / 2 + _paddingTop);

    QApplication::style()->drawItemText(painter, firstLineRect, Qt::AlignTop | Qt::AlignLeft, QPalette(), true, name);
    QApplication::style()->drawItemText(painter, secondLineRect,
      Qt::AlignTop | Qt::AlignLeft, QPalette(), true, relativePath, QPalette::ColorRole::Dark);
  }

  QSize FileListItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const {
    QSize result = QStyledItemDelegate::sizeHint(option, index);

    result.setHeight(50);

    return result;
  }
}  // namespace CodeLineX
