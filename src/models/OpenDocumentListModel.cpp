/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/OpenDocumentListModel.h"

#include "components/Document.h"
#include "components/UnloadDocument.h"
#include "components/EditorTabview.h"

namespace CodeLineX {
  OpenDocumentListModel::OpenDocumentListModel(QObject* parent): QStandardItemModel(parent) {
    setItemRoleNames({
      { Role::DOCUMENT_OID, "oid" },
      { Role::DOCUMENT_NAME, "name" },
      { Role::DOCUMENT_URL, "url" }
    });
  }

  int OpenDocumentListModel::findRow(const QString& text) {
    for (auto i = 0; i < rowCount(); i++) {
      auto oidIndex = index(i, 0);
      auto currentTextValue = data(oidIndex, DOCUMENT_OID).value<QString>();

      if (QString::localeAwareCompare(currentTextValue, text) == 0) {
        return i;
      }
    }

    return -1;
  }

  void OpenDocumentListModel::appendRow(Document* document) {
    auto row = findRow(document->objectId());

    if (row == -1) {
      auto itemElement = new QStandardItem(document->name());

      itemElement->setToolTip(document->url().toLocalFile());
      itemElement->setData(document->objectId(), DOCUMENT_OID);
      itemElement->setData(document->name(), DOCUMENT_NAME);
      itemElement->setData(document->url(), DOCUMENT_URL);

      QStandardItemModel::appendRow(itemElement);

      connectDocument(document, itemElement);
    } else {
      connectDocument(document, item(row));
    }
  }

  void OpenDocumentListModel::appendRow(UnloadDocument* unloadDocument) {
    auto item = new QStandardItem(unloadDocument->name());

    item->setToolTip(unloadDocument->url().toLocalFile());
    item->setData(unloadDocument->objectId(), DOCUMENT_OID);
    item->setData(unloadDocument->name(), DOCUMENT_NAME);
    item->setData(unloadDocument->url(), DOCUMENT_URL);

    QStandardItemModel::appendRow(item);

    connect(unloadDocument, &UnloadDocument::loaded, this, [this, unloadDocument](Document* document) {
      Q_UNUSED(document)
      unloadDocument->disconnect(this);
    });
  }

  void OpenDocumentListModel::removeRow(const QString& oid) {
    auto row = findRow(oid);

    if (row > -1) {
      QStandardItemModel::removeRow(row);
    }
  }

  void OpenDocumentListModel::removeRow(Document* document) {
    removeRow(document->objectId());
  }

  void OpenDocumentListModel::removeRow(UnloadDocument* document) {
    removeRow(document->objectId());
  }

  void OpenDocumentListModel::connectDocument(Document* document, QStandardItem* item) {
    connect(document, &Document::nameChanged, this, [item](const QString& name) {
      item->setText(name);
    });

    connect(document, &Document::urlChanged, this, [item](const QUrl& url) {
      item->setToolTip(url.toLocalFile());
    });

    connect(document, &Document::closed, this, [this, document]() {
      document->disconnect(this);

      if (rowCount()) {
        onDocumentClosed(document);
      }
    });
  }

  void OpenDocumentListModel::onDocumentClosed(Document* document) {
    const int row = findRow(document->objectId());

    Q_ASSERT(row > -1);

    takeRow(row);
  }
}  // namespace CodeLineX
