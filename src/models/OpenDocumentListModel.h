/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELS_OPENDOCUMENTLISTMODEL_H_
#define MODELS_OPENDOCUMENTLISTMODEL_H_

#include <QStandardItemModel>

namespace CodeLineX {
  class Document;
  class UnloadDocument;

  class OpenDocumentListModel: public QStandardItemModel {
    Q_OBJECT

    public:
      enum Role {
        DOCUMENT_OID = Qt::UserRole + 50,
        DOCUMENT_NAME,
        DOCUMENT_URL
      };

      explicit OpenDocumentListModel(QObject* parent = nullptr);

      int findRow(const QString& text);

    public slots:
      void appendRow(Document*);
      void appendRow(UnloadDocument*);
      void removeRow(const QString& oid);
      void removeRow(Document*);
      void removeRow(UnloadDocument*);

    protected slots:
      void onDocumentClosed(Document*);

    protected:
      void connectDocument(Document* document, QStandardItem*);
  };
}  // namespace CodeLineX

#endif  // MODELS_OPENDOCUMENTLISTMODEL_H_
