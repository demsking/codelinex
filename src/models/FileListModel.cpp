/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/FileListModel.h"

#include <QFileInfo>

#include <hpx-tabview/TabviewItem.h>
#include <hpx-tabview/TabviewWidget.h>

#include "components/Document.h"
#include "components/UnloadDocument.h"
#include "components/EditorTabview.h"
#include "utils/FileSystemUtils.h"

namespace CodeLineX {
  FileListModel::FileListModel(QObject* parent): QStandardItemModel(parent) {
    setItemRoleNames({
      { FileItem::Role::OID, "oid" },
      { FileItem::Role::NAME, "name" },
      { FileItem::Role::DESC, "desc" },
      { FileItem::Role::URL, "url" }
    });
  }

  int FileListModel::findRow(const QString& text, FileItem::Role role) {
    for (auto i = 0; i < rowCount(); i++) {
      auto currentTextValue = data(index(i, role)).value<QString>();

      if (QString::localeAwareCompare(currentTextValue, text) == 0) {
        return i;
      }
    }

    return -1;
  }

  void FileListModel::appendRow(const QUrl& url, const QDir& base) {
    QFileInfo fileInfo(url.toLocalFile());

    QStandardItemModel::appendRow({
      new QStandardItem(url.toString()),
      new QStandardItem(fileInfo.fileName()),
      new QStandardItem(FileSystemUtils::relativePath(url, base)),
      new QStandardItem(url.toString())
    });
  }

  void FileListModel::appendRow(Document* document, const QDir& base) {
    const QUrl& url = document->url();
    auto oidItem = new QStandardItem(document->objectId());
    auto titleItem = new QStandardItem(document->name());
    auto descItem = new QStandardItem(FileSystemUtils::relativePath(url, base));
    auto urlItem = new QStandardItem(url.toString());

    QStandardItemModel::appendRow({ oidItem, titleItem, descItem, urlItem });
    connectDocument(document, titleItem, descItem, urlItem, base);
  }

  void FileListModel::appendRow(HPX::TabviewItem* item, const QDir& base) {
    const QUrl& url = item->data(EditorTabview::TabviewItemRole::UrlRole).toUrl();
    HPX::TabviewWidget* widget = item->widget();
    auto document = qobject_cast<Document*>(widget);
    auto oidItem = new QStandardItem(document->objectId());
    auto titleItem = new QStandardItem(item->title());
    auto descItem = new QStandardItem(FileSystemUtils::relativePath(url, base));
    auto urlItem = new QStandardItem(url.toString());

    QStandardItemModel::appendRow({ oidItem, titleItem, descItem, urlItem });
    connectDocument(document, titleItem, descItem, urlItem, base);
  }

  void FileListModel::appendRow(UnloadDocument* unloadDocument, const QDir& base) {
    const QUrl& url = unloadDocument->url();
    auto oidItem = new QStandardItem(unloadDocument->objectId());
    auto titleItem = new QStandardItem(unloadDocument->name());
    auto descItem = new QStandardItem(FileSystemUtils::relativePath(url, base));
    auto urlItem = new QStandardItem(url.toString());

    QStandardItemModel::appendRow({ oidItem, titleItem, descItem, urlItem });

    connect(unloadDocument, &UnloadDocument::loaded, this,
            [this, titleItem, descItem, urlItem, base, unloadDocument](Document* document) {
      unloadDocument->disconnect(this);
      connectDocument(document, titleItem, descItem, urlItem, base);
    });
  }

  void FileListModel::connectDocument(Document* document,
    QStandardItem* titleItem,
    QStandardItem* descItem,
    QStandardItem* urlItem,
    const QDir& base
  ) {
    connect(document, &Document::nameChanged, this, [titleItem](const QString& name) {
      titleItem->setText(name);
    });

    connect(document, &Document::urlChanged, this, [descItem, urlItem, base](const QUrl& url) {
      descItem->setText(FileSystemUtils::relativePath(url, base));
      urlItem->setData(url, FileItem::Role::URL);
    });

    connect(document, &Document::focusIn, this, [this, document]() {
      onDocumentFocusIn(document);
    });

    connect(document, &Document::closed, this, [this, document]() {
      document->disconnect(this);

      if (rowCount()) {
        onDocumentClosed(document);
      }
    });

    onDocumentFocusIn(document);
  }

  void FileListModel::onDocumentFocusIn(Document* document) {
    const int row = findRow(document->objectId(), FileItem::Role::OID);
    const QList<QStandardItem*> items = takeRow(row);

    insertRow(0, items);
  }

  void FileListModel::onDocumentClosed(Document* document) {
    const int row = findRow(document->objectId(), FileItem::Role::OID);

    Q_ASSERT(row > -1);

    takeRow(row);
  }
}  // namespace CodeLineX
