/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "models/OutilineSortFilterProxyModel.h"
#include "models/OutlineItem.h"

namespace CodeLineX {
  OutilineSortFilterProxyModel::OutilineSortFilterProxyModel(QObject* parent): QSortFilterProxyModel(parent) {
  }

  bool OutilineSortFilterProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const {
    switch (sortRole()) {
      case OutlineItem::NameRole: {
        const auto leftString = left.data(OutlineItem::NameRole).value<QString>();
        const auto rightString = right.data(OutlineItem::NameRole).value<QString>();

        return QString::localeAwareCompare(leftString, rightString) <= 0;
      }

      case OutlineItem::RangeRole: {
        const auto leftRange = left.data(OutlineItem::RangeRole).value<OutlineItem::Range>();
        const auto rightRange = right.data(OutlineItem::RangeRole).value<OutlineItem::Range>();

        return leftRange < rightRange;
      }
    }

    return false;
  }
} // namespace CodeLineX
