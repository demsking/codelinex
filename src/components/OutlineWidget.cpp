/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/OutlineWidget.h"

#include <QState>
#include <QStateMachine>
#include <QStandardItemModel>
#include <QItemSelectionModel>

#include <hpx-stacked-panel/StackedPanelToolbox.h>
#include <hpx-stacked-panel/LandingWidget.h>

#include "components/Document.h"
#include "components/PushButton.h"
#include "components/TreeView.h"

#include "models/OutlineItem.h"
#include "models/OutilineSortFilterProxyModel.h"

#include "layouts/Shell.h"

namespace CodeLineX {
  QMap<QString, std::shared_ptr<QStandardItemModel>> OutlineWidget::_outlineCache;

  OutlineWidget::OutlineWidget(const LSP::Manager::Pointer& manager, QWidget* parent)
    : AbstractLinkedPanel(parent),
      _lspManager(manager) {
    setProperty("OutlineWidget", true);

    _proxyModel = new OutilineSortFilterProxyModel();

    _sortButton = new PushButton(this);
    _sortStateMachine = new QStateMachine(this);
    _treeView = new TreeView(this);

    auto emptyWidget = new HPX::LandingWidget(tr("No outline available"), this);
    auto loadingWidget = new HPX::LandingWidget(tr("Loading outline..."), this);

    emptyWidget->setObjectName("OutlineEmptyWidget");
    loadingWidget->setObjectName("OutlineLoadingWidget");

    setStateWidget(State::Empty, emptyWidget);
    setStateWidget(State::Loading, loadingWidget);
    setStateWidget(State::Ready, _treeView);

    initSortButton();
    toolbox()->addButton(_sortButton);

    _treeView->setModel(_proxyModel);
    _treeView->setSortingEnabled(true);

    connect(_treeView, &TreeView::activated, this, &OutlineWidget::goToSymbol);
    connect(_treeView, &TreeView::clicked, this, &OutlineWidget::goToSymbol);

    connect(this, &OutlineWidget::focusIn, [this]() {
      if (_currentDocument) {
        setCurrentDocument(_currentDocument);
      }
    });
  }

  Document* OutlineWidget::currentDocument() const {
    return _currentDocument;
  }

  void OutlineWidget::initSortButton() {
    const QIcon declarationSortIcon = QIcon::fromTheme(QStringLiteral("view-sort"));
    const QIcon ascendingSortIcon = QIcon::fromTheme(QStringLiteral("view-sort-ascending"));
    const QIcon descendingSortIcon = QIcon::fromTheme(QStringLiteral("view-sort-descending"));

    _declarationSortState = new QState();
    _ascendingSortState = new QState();
    _descendingSortState = new QState();

    _declarationSortState->assignProperty(_sortButton, "checked", false);
    _declarationSortState->assignProperty(_sortButton, "icon", declarationSortIcon);
    _declarationSortState->assignProperty(_sortButton, "toolTip", tr("Ascending sort"));
    _declarationSortState->addTransition(_sortButton, SIGNAL(clicked()), _ascendingSortState);

    _ascendingSortState->assignProperty(_sortButton, "checked", true);
    _ascendingSortState->assignProperty(_sortButton, "icon", ascendingSortIcon);
    _ascendingSortState->assignProperty(_sortButton, "toolTip", tr("Descending sort"));
    _ascendingSortState->addTransition(_sortButton, SIGNAL(clicked()), _descendingSortState);

    _descendingSortState->assignProperty(_sortButton, "checked", true);
    _descendingSortState->assignProperty(_sortButton, "icon", descendingSortIcon);
    _descendingSortState->assignProperty(_sortButton, "toolTip", tr("Declaration sort"));
    _descendingSortState->addTransition(_sortButton, SIGNAL(clicked()), _declarationSortState);

    _sortStateMachine->addState(_declarationSortState);
    _sortStateMachine->addState(_ascendingSortState);
    _sortStateMachine->addState(_descendingSortState);
    _sortStateMachine->setInitialState(_declarationSortState);
    _sortStateMachine->start();

    _sortButton->setCheckable(true);
    _sortButton->setFixedSize(20, 20);
    _sortButton->setIconSize(_sortButton->size());

    connect(_declarationSortState, &QState::entered, this, &OutlineWidget::sortByDeclaration);
    connect(_ascendingSortState, &QState::entered, this, &OutlineWidget::sortByNameAsc);
    connect(_descendingSortState, &QState::entered, this, &OutlineWidget::sortByNameDesc);
  }

  void OutlineWidget::clearTreeviewModel() {
    auto model = _treeView->model();

    if (model) {
      model->removeRows(0, model->rowCount());
      setCurrentState(State::Empty);
    }
  }

  bool OutlineWidget::isExpanable(QStandardItem* item) const {
    return _treeView->isExpanded(_proxyModel->mapFromSource(_outline->indexFromItem(item)));
  }

  QStandardItem* OutlineWidget::getCurrentItem(QStandardItem* item, int line) {
    // first traverse the child items to have deepest match!
    // only do this if our stuff is expanded
    if (item == _outline->invisibleRootItem() || isExpanable(item)) {
      for (int i = 0; i < item->rowCount(); i++) {
        auto childItem = item->child(i);
        auto currentItem = getCurrentItem(childItem, line);

        if (currentItem) {
          return currentItem;
        }
      }
    }

    // does the line match our item?
    auto range = item->data(OutlineItem::RangeRole).value<KTextEditor::Range>();

    return range.overlapsLine(line) ? item : nullptr;
  }

  void OutlineWidget::selectSymbol(const KTextEditor::Cursor& cursor) {
    auto item = getCurrentItem(_outline->invisibleRootItem(), cursor.line());

    if (item) {
      auto index = _proxyModel->mapFromSource(_outline->indexFromItem(item));

      _treeView->setCurrentIndex(index);
    }
  }

  void OutlineWidget::setCurrentDocument(Document* document) {
    loadDocumentOutline(document);
  }

  void OutlineWidget::sortByDeclaration() {
    _proxyModel->setSortRole(OutlineItem::RangeRole);
    _treeView->sortByColumn(0, Qt::AscendingOrder);
  }

  void OutlineWidget::sortByNameAsc() {
    _proxyModel->setSortRole(OutlineItem::NameRole);
    _treeView->sortByColumn(0, Qt::AscendingOrder);
  }

  void OutlineWidget::sortByNameDesc() {
    _proxyModel->setSortRole(OutlineItem::NameRole);
    _treeView->sortByColumn(0, Qt::DescendingOrder);
  }

  void OutlineWidget::onLinkButtonCheck(bool checked) {
    if (!_currentDocument) {
      return;
    }

    if (checked) {
      connect(_currentDocument, &Document::cursorPositionChanged,
              this, &OutlineWidget::selectSymbol, Qt::UniqueConnection);

      selectSymbol(_currentDocument->cursorPosition());
    } else {
      disconnect(_currentDocument, &Document::cursorPositionChanged,
                 this, &OutlineWidget::selectSymbol);
    }
  }

  void OutlineWidget::loadDocumentOutline(Document* document, bool useCache) {
    // Only load Online if the Symbols Outline widget is active
    if (isActive()) {
      auto oid = document->objectId();

      // traying to load document outline from cache
      if (useCache && OutlineWidget::_outlineCache.contains(oid)) {
        _outline = OutlineWidget::_outlineCache.value(oid);

        if (_outline.get()) {
          return setOutlineSourceModel(document, _outline);
        }

        // invalidate corrupted document's outline cache
        invalidateCacheOf(oid);
      }

      if (_currentDocument) {
        _currentDocument->disconnect(this);
      }

      _currentDocument = document;

      findDocumentServer(document);

      return;
    }

    // If the Symbols Outline widget is not active, store the document pointer
    // to _currentDocument and exit the function call.
    // Outline will be loaded when signal SymbolsOutlineWidget::focusIn will be send.
    _currentDocument = document;
  }

  void OutlineWidget::findDocumentServer(Document* document) {
    auto doc = document->doc();
    auto server = _lspManager->findServer(doc.data(), [this, document](const QSharedPointer<LSPClientServer>& server) {
      if (!server.isNull()) {
        sendDocumentSymbolsRequest(document, server);
      } else {
        setCurrentState(State::Empty);
      }
    });

    if (server == nullptr) {
      setCurrentState(State::Empty);
    }
  }

  void OutlineWidget::sendDocumentSymbolsRequest(Document* document, QSharedPointer<LSPClientServer> server) {
    auto doc = document->doc();
    auto handler = utils::mem_fun(&OutlineWidget::onDocumentSymbolsResponse, this);

    setCurrentState(State::Loading);
    emit loading();

    server->documentSymbols(doc->url(), this, handler);
  }

  void OutlineWidget::onDocumentSymbolsResponse(const QList<LSPSymbolInformation>& outline) {
    if (!_currentDocument->isReady()) {
      return;
    }

    auto newModel = std::make_shared<QStandardItemModel>();

    makeNodes(outline, newModel.get(), nullptr);

    _outline = newModel;

    OutlineWidget::_outlineCache.insert(_currentDocument->objectId(), _outline);
    setOutlineSourceModel(_currentDocument, _outline);

    emit loaded();
  }

  void OutlineWidget::invalidateCacheOf(const QString& documentObjectId) {
    OutlineWidget::_outlineCache.remove(documentObjectId);

    if (_currentDocument && _currentDocument->objectId() == documentObjectId) {
      clearTreeviewModel();
    }
  }

  void OutlineWidget::connectDocument(Document* document) {
    connect(_currentDocument, &Document::cursorPositionChanged,
            this, &OutlineWidget::selectSymbol, Qt::UniqueConnection);

    connect(document, &Document::saved, [this, document]() {
      loadDocumentOutline(document, false);
      connectDocument(document);
    });

    connect(document, &Document::closed, [this, document]() {
      disconnectDocument(document);
    });
  }

  void OutlineWidget::disconnectDocument(Document* document) {
    document->disconnect(this);

    if (_currentDocument == document) {
      clearTreeviewModel();

      _currentDocument = nullptr;
    }

    invalidateCacheOf(document->objectId());
  }

  void OutlineWidget::setOutlineSourceModel(Document* document, std::shared_ptr<QStandardItemModel> model) {
    _currentDocument = document;

    if (model->rowCount() == 0) {
      setCurrentState(State::Empty);
    } else {
      _proxyModel->setSourceModel(model.get());

      auto configuration = _sortStateMachine->configuration();

      if (configuration.contains(_ascendingSortState)) {
        sortByNameAsc();
      } else if (configuration.contains(_descendingSortState)) {
        sortByNameDesc();
      } else {
        sortByDeclaration();
      }

      _treeView->expandAll();
      selectSymbol(document->cursorPosition());
      connectDocument(document);

      setCurrentState(State::Ready);
    }
  }

  const QIcon* OutlineWidget::getIcon(const LSPSymbolInformation& symbol, OutlineItem* parent) const {
    static const QIcon ICON_PKG = QIcon::fromTheme(QStringLiteral("code-block"));
    static const QIcon ICON_CLASS = QIcon::fromTheme(QStringLiteral("code-class"));
    static const QIcon ICON_TYPEDEF = QIcon::fromTheme(QStringLiteral("code-typedef"));
    static const QIcon ICON_FUNCTION = QIcon::fromTheme(QStringLiteral("code-function"));
    static const QIcon ICON_VAR = QIcon::fromTheme(QStringLiteral("code-variable"));

    const QIcon* icon = nullptr;

    switch (symbol.kind) {
      case LSPSymbolKind::File:
      case LSPSymbolKind::Module:
      case LSPSymbolKind::Namespace:
      case LSPSymbolKind::Package:
        if (symbol.children.count() == 0) {
          return nullptr;
        }

        icon = &ICON_PKG;
        break;

      case LSPSymbolKind::Class:
      case LSPSymbolKind::Interface:
        icon = &ICON_CLASS;
        break;

      case LSPSymbolKind::Enum:
        icon = &ICON_TYPEDEF;
        break;

      case LSPSymbolKind::Method:
      case LSPSymbolKind::Function:
      case LSPSymbolKind::Constructor:
        icon = &ICON_FUNCTION;
        break;

      // all others considered/assumed Variable
      case LSPSymbolKind::Variable:
      case LSPSymbolKind::Constant:
      case LSPSymbolKind::String:
      case LSPSymbolKind::Number:
      case LSPSymbolKind::Property:
      case LSPSymbolKind::Field:
      default:
        // skip local variable
        // property, field, etc unlikely in such case anyway
        if (parent && parent->icon().cacheKey() == ICON_FUNCTION.cacheKey()) {
          return nullptr;
        }

        icon = &ICON_VAR;
    }

    return icon;
  }

  void OutlineWidget::makeNodes(
    const QList<LSPSymbolInformation>& symbols,
    QStandardItemModel* model,
    OutlineItem* parent
  ) {
    for (const auto& symbol: symbols) {
      const QIcon* icon = getIcon(symbol, parent);

      if (icon == nullptr) {
        continue;
      }

      auto node = new OutlineItem();
      const QString tooltip = symbol.name + symbol.detail;

      if (parent) {
        parent->appendRow(node);
      } else {
        model->appendRow(node);
      }

      node->setIcon(*icon);
      node->setText(symbol.name);
      node->setToolTip(tooltip);
      node->setName(symbol.name);
      node->setRange(symbol.range);

      // recurse children
      makeNodes(symbol.children, model, node);
    }
  }

  void OutlineWidget::goToSymbol(const QModelIndex& index) {
    const auto range = index.data(OutlineItem::RangeRole).value<OutlineItem::Range>();

    if (range.isValid()) {
      _currentDocument->view()->setCursorPosition(range.start());
    }
  }
}  // namespace CodeLineX
