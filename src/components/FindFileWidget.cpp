/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/FindFileWidget.h"

#include <QEvent>
#include <QObject>
#include <QWidget>
#include <QKeyEvent>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <CuteIcons.h>
#include <CuteMaterialIcons.h>

#include "components/Label.h"
#include "components/LineEdit.h"
#include "components/PushButton.h"
#include "components/IconButton.h"

#define BUTTONS_LAYOUT_SPACING 5

namespace CodeLineX {
  FindFileWidget::FindFileWidget(QWidget* parent): QFrame(parent) {
    setProperty("FindFileWidget", true);

    _mainLayout = new QHBoxLayout(this);
    _inputsLayout = new QVBoxLayout;
    _buttonsLayout = new QVBoxLayout;
    _findButtonsLayout = new QHBoxLayout;
    _replaceButtonsLayout = new QHBoxLayout;
    _actionsLayout = new QHBoxLayout;
    _queryLineEdit = new LineEdit(this);
    _replaceLineEdit = new LineEdit;
    _findNextButton = new IconButton(CUTE_MDI_ARROW_FORWARD, this);
    _findPreviousButton = new IconButton(CUTE_MDI_ARROW_BACK, this);
    _replaceButton = new IconButton(CUTE_MDI_DONE);
    _replaceAllButton = new IconButton(CUTE_MDI_DONE_ALL);
    _closeButton = new IconButton(CUTE_MDI_CLOSE, this);

    _findNextButton->setDisabled(true);
    _findNextButton->setToolTip(tr("Find Next Match"));

    _findPreviousButton->setDisabled(true);
    _findPreviousButton->setToolTip(tr("Find Previous Match"));

    _closeButton->setToolTip(tr("Close"));
    _closeButton->setProperty("CloseButton", true);

    _queryLineEdit->setPlaceholderText(tr("Find"));
    _replaceLineEdit->setPlaceholderText(tr("Replace"));

    _replaceButton->setDisabled(true);
    _replaceButton->setToolTip(tr("Replace"));

    _replaceAllButton->setDisabled(true);
    _replaceAllButton->setToolTip(tr("Replace All"));

    _findButtonsLayout->setMargin(0);
    _findButtonsLayout->setSpacing(BUTTONS_LAYOUT_SPACING);
    _findButtonsLayout->addWidget(_findPreviousButton);
    _findButtonsLayout->addWidget(_findNextButton);

    _replaceButtonsLayout->setMargin(0);
    _replaceButtonsLayout->setSpacing(BUTTONS_LAYOUT_SPACING);
    _replaceButtonsLayout->addWidget(_replaceButton);
    _replaceButtonsLayout->addWidget(_replaceAllButton);

    _inputsLayout->setMargin(0);
    _inputsLayout->setSpacing(0);
    _inputsLayout->addWidget(_queryLineEdit);
    _inputsLayout->addWidget(_replaceLineEdit);

    _buttonsLayout->setMargin(0);
    _buttonsLayout->setSpacing(0);
    _buttonsLayout->addLayout(_findButtonsLayout);
    _buttonsLayout->addLayout(_replaceButtonsLayout);

    _actionsLayout->setMargin(0);
    _actionsLayout->addWidget(_closeButton);
    _actionsLayout->setAlignment(Qt::AlignTop);

    _mainLayout->setMargin(0);
    _mainLayout->addLayout(_inputsLayout);
    _mainLayout->addLayout(_buttonsLayout);
    _mainLayout->addLayout(_actionsLayout);

    connectQueryLineEdit();
    setFocusProxy(_queryLineEdit);

    connect(_closeButton, &IconButton::pressed, this, &FindFileWidget::aboutToClose);
    connect(_findPreviousButton, &IconButton::pressed, this, &FindFileWidget::findPreviousMatch);
    connect(_findNextButton, &IconButton::pressed, this, &FindFileWidget::findNextMatch);
    connect(_replaceButton, &IconButton::pressed, this, &FindFileWidget::replaceNextMatch);
    connect(_replaceAllButton, &IconButton::pressed, this, &FindFileWidget::replaceAllMatch);
  }

  FindFileWidget::~FindFileWidget() {
    delete _queryLineEdit;
    delete _replaceLineEdit;
    delete _findNextButton;
    delete _findPreviousButton;
    delete _replaceButton;
    delete _replaceAllButton;
    delete _closeButton;
    delete _findButtonsLayout;
    delete _replaceButtonsLayout;
    delete _inputsLayout;
    delete _buttonsLayout;
    delete _actionsLayout;
    delete _mainLayout;
  }

  void FindFileWidget::setQuery(const QString& pattern) {
    const bool empty = pattern.isEmpty();

    _findNextButton->setDisabled(empty);
    _findPreviousButton->setDisabled(empty);

    _replaceButton->setDisabled(empty);
    _replaceAllButton->setDisabled(empty);

    _queryLineEdit->disconnect(this);
    _queryLineEdit->setText(pattern);

    connectQueryLineEdit();
  }

  void FindFileWidget::selectAll() {
    _queryLineEdit->selectAll();
  }

  void FindFileWidget::showReplaceLayout() {
    _replaceLineEdit->show();
    _replaceButton->show();
    _replaceAllButton->show();
  }

  void FindFileWidget::hideReplaceLayout() {
    _replaceLineEdit->hide();
    _replaceButton->hide();
    _replaceAllButton->hide();
  }

  void FindFileWidget::connectQueryLineEdit() {
    connect(_queryLineEdit, &LineEdit::textChanged, this, &FindFileWidget::findNextMatchLine);
    connect(_queryLineEdit, &LineEdit::returnPressed, this, &FindFileWidget::findNextMatch);
  }

  void FindFileWidget::findPreviousMatch() {
    const QString& pattern = _queryLineEdit->text();

    if (!pattern.isEmpty()) {
      emit findPrevious(pattern, Document::SearchOptions::Backwards);
    }
  }

  void FindFileWidget::findNextMatch() {
    const QString& pattern = _queryLineEdit->text();

    if (!pattern.isEmpty()) {
      emit findNext(pattern, Document::SearchOptions::Default, true);
    }
  }

  void FindFileWidget::findNextMatchLine(const QString& pattern) {
    const bool empty = pattern.isEmpty();

    if (!empty) {
      emit findNext(pattern, Document::SearchOptions::Default, true);
    }

    _findNextButton->setDisabled(empty);
    _findPreviousButton->setDisabled(empty);

    _replaceButton->setDisabled(empty);
    _replaceAllButton->setDisabled(empty);
  }

  void FindFileWidget::replaceNextMatch() {
    const QString pattern = _queryLineEdit->text();
    const Document::SearchOptions options = Document::SearchOptions::Default;
    const QString replaceText = _replaceLineEdit->text();

    emit replace(pattern, options, replaceText, false);
  }

  void FindFileWidget::replaceAllMatch() {
    const QString pattern = _queryLineEdit->text();
    const Document::SearchOptions options = Document::SearchOptions::Default;
    const QString replaceText = _replaceLineEdit->text();

    emit replace(pattern, options, replaceText, true);
  }

  bool FindFileWidget::eventFilter(QObject* obj, QEvent* event) {
    if (event->type() == QEvent::KeyPress) {
      auto keyEvent = static_cast<QKeyEvent*>(event);

      if (keyEvent->modifiers() & Qt::SHIFT && keyEvent->key() == Qt::Key_F3) {
        findPreviousMatch();

        return true;
      }

      if (keyEvent->key() == Qt::Key_F3) {
        findNextMatch();

        return true;
      }
    }

    return QFrame::eventFilter(obj, event);
  }
}  // namespace CodeLineX
