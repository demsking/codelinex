/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_SWITCHFILEVIEW_H_
#define COMPONENTS_SWITCHFILEVIEW_H_

#include "layouts/Overlay.h"

class QObject;
class QWidget;
class QEvent;

namespace CodeLineX {
  class FileList;
  class FileListModel;

  class SwitchFileView: public Layout::Overlay {
    Q_OBJECT

    FileList* _fileView = nullptr;
    int _factor = 0;

    public:
      explicit SwitchFileView(QWidget* parent);
      ~SwitchFileView() override;

      void setModel(FileListModel*);

    public slots:
      void setCurrentIndex(int);

    signals:
      void selected(const QString& oid);

    public slots:
      void show() override;

    protected:
      void next();
      inline int nextRowNumber(int currentRowNumber) const;
      inline void emitSelected();

    protected:
      bool eventFilter(QObject* obj, QEvent* event) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_SWITCHFILEVIEW_H_
