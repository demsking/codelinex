﻿/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/Document.h"

#include <QDir>
#include <QColor>
#include <QWidget>
#include <QVBoxLayout>

#include <KTextEditor/Editor>
#include <KTextEditor/Document>
#include <KTextEditor/ConfigInterface>

#include <KSharedConfig>
#include <KConfigGroup>
#include <KActionCollection>

static const QString CONFIG_KEY_SELECTION_RANGE = QStringLiteral("Selection Range");
static const QString CONFIG_KEY_CURSOR_POSITION = QStringLiteral("Cursor Position");
static const QString CONFIG_KEY_FIRST_DISPLAYED_LINE = QStringLiteral("First Displayed Line");

static const QString VIEW_CONFIG_KEY_BACKGROUND_COLOR = QStringLiteral("background-color");
static const QString VIEW_CONFIG_KEY_SELECTION_COLOR = QStringLiteral("selection-color");
static const QString VIEW_CONFIG_KEY_SEARCH_HIGHLIGHT_COLOR = QStringLiteral("search-highlight-color");
static const QString VIEW_CONFIG_KEY_REPLACE_HIGHLIGHT_COLOR = QStringLiteral("replace-highlight-color");
static const QString VIEW_CONFIG_KEY_ICON_BORDER_COLOR = QStringLiteral("icon-border-color");
static const QString VIEW_CONFIG_KEY_FOLDING_MARKER_COLOR = QStringLiteral("folding-marker-color");
static const QString VIEW_CONFIG_KEY_LINE_NUMBER_COLOR = QStringLiteral("line-number-color");
static const QString VIEW_CONFIG_KEY_CURRENT_LINE_NUMBER_COLOR = QStringLiteral("current-line-number-color");
static const QString VIEW_CONFIG_KEY_FONT = QStringLiteral("font");
static const QString VIEW_CONFIG_KEY_SCHEME = QStringLiteral("scheme");

namespace CodeLineX {
  QMap<QUrl, QSharedPointer<KTextEditor::Document>> Document::_refs;

  Document::Document(QWidget* parent): TabviewWidget(parent), Core::StateSession(metaObject()) {
    _layout = new QVBoxLayout(this);

    _layout->setMargin(0);
    _layout->setSpacing(0);

    setLayout(_layout);
    setProperty("Editor", true);

    // create a new document with nullptr as parent.
    // this is needed because the created doc pointer may be shared
    // between many other Documents, so it should be deleted only
    // when the last Document itself is deleted.
    auto doc = KTextEditor::Editor::instance()->createDocument(nullptr);

    setDoc(QSharedPointer<KTextEditor::Document>(doc));
  }

  Document::Document(const QUrl& url, QWidget* parent): Document(parent) {
    open(url);
  }

  bool Document::isReady() const {
    return _view != nullptr;
  }

  QString Document::name() const {
    if (_doc->isModified()) {
      return _doc->documentName() + " *";
    }

    return _doc->documentName();
  }

  QUrl Document::url() const {
    return _doc->url();
  }

  QString Document::path() const {
    return url().toLocalFile();
  }

  QString Document::encoding() const {
    return _doc->encoding();
  }

  QString Document::endOfLine() const {
    return _doc->endOfLine(1).toString();
  }

  QString Document::mimeType() const {
    return _doc->mimeType();
  }

  QString Document::mode() const {
    return _doc->mode();
  }

  Document::InputMode Document::viewInputMode() const {
    return _view->viewInputMode();
  }

  QString Document::viewInputModeHuman() const {
    return _view->viewInputModeHuman();
  }

  Document::ViewMode Document::viewMode() const {
    return _view->viewMode();
  }

  QString Document::viewModeHuman() const {
    return _view->viewModeHuman();
  }

  Document::Cursor Document::cursorPosition() const {
    return _view->cursorPosition();
  }

  QString Document::wordAt(const Cursor& cursor) const {
    return _doc->wordAt(cursor);
  }

  bool Document::selection() const {
    return _view->selection();
  }

  QString Document::selectionText() const {
    return _view->selectionText();
  }

  bool Document::isModified() const {
    return _doc->isModified();
  }

  bool Document::isReadOnly() const {
    return _doc->isReadWrite();
  }

  void Document::setWritable(bool readwrite) {
    _doc->setReadWrite(readwrite);
  }

  const QFont& Document::font() const {
    return _font;
  }

  void Document::setFont(const QFont& font) {
    _font = font;
    _configView->setConfigValue(VIEW_CONFIG_KEY_FONT, font);
  }

  void Document::increaseFontSize(qreal step) {
    _font.setPointSizeF(_font.pointSizeF() + step);

    _configView->setConfigValue(VIEW_CONFIG_KEY_FONT, _font);
  }

  void Document::decreaseFontSize(qreal step) {
    if ((_font.pointSizeF() - step) > 0) {
      _font.setPointSizeF(_font.pointSizeF() - step);
    }

    _configView->setConfigValue(VIEW_CONFIG_KEY_FONT, _font);
  }

  void Document::resetFontSize() {
    setFont(_initialFont);
  }

  QSharedPointer<KTextEditor::Document> Document::doc() const {
    return _doc;
  }

  KTextEditor::View* Document::view() const {
    return _view;
  }

  QList<KTextEditor::View*> Document::views() const {
    return _doc->views();
  }

  QAction* Document::action(Document::Action id) const {
    QString name;

    switch (id) {
      case Action::DYNAMIC_WORD_WRAP:
        name = "view_dynamic_word_wrap";
        break;

      case Action::DYNAMIC_WORD_WRAP_INDICATOR:
        name = "dynamic_word_wrap_indicators";
        break;

      case Action::DYNAMIC_WORD_WRAP_MARKER:
        name = "view_word_wrap_marker";
        break;

      case Action::EXPORT_HTML:
        name = "file_export_html";
        break;

      case Action::RELOAD:
        name = "file_reload";
        break;

      case Action::SAVE_AS_WITH_ENCODING:
        name = "file_save_as_with_encoding";
        break;

      case Action::SHOW_NON_PRINTABLE_SPACES:
        name = "view_non_printable_spaces";
        break;

      case Action::ZOOM_IN:
        name = "view_inc_font_sizes";
        break;

      case Action::ZOOM_OUT:
        name = "view_dec_font_sizes";
        break;
    }

    return _view->action(name.toStdString().c_str());
  }

  QAction* Document::action(const StandardAction id) const {
    return _view->action(KStandardAction::name(id));
  }

  Document::ActionCollection* Document::actionCollection() const {
    return _view->actionCollection();
  }

  QVector<Document::Range> Document::searchText(const Range& range,
                                                const QString& pattern,
                                                const SearchOptions options) const {
    return _doc->searchText(range, pattern, options);
  }

  KTextEditor::View* Document::createView(QWidget* parent) {
    return _doc->createView(parent);
  }

  bool Document::reload() {
    return _doc->documentReload();
  }

  void Document::setDoc(const QSharedPointer<KTextEditor::Document>& value) {
    if (!_doc.isNull() && _doc.data() != value.data()) {
      _doc.clear();
    }

    _doc = value;
    _view = _doc->createView(this);

    _configView = qobject_cast<KTextEditor::ConfigInterface*>(_view);
    _configDoc = qobject_cast<KTextEditor::ConfigInterface*>(_doc.data());

    _initialFont = _configView->configValue(VIEW_CONFIG_KEY_FONT).value<QFont>();
    _font = _initialFont;

    _view->setProperty("EditorView", true);
    _view->setStatusBarEnabled(false);

    setFocusProxy(_view);
    disableViewActionShortcuts();

    _layout->addWidget(_view);

    _configView->setConfigValue("line-numbers", true);
    _configView->setConfigValue("icon-bar", false);
    _configView->setConfigValue("folding-bar", true);
    _configView->setConfigValue("folding-preview", true);
    _configView->setConfigValue("dynamic-word-wrap", false);
    _configView->setConfigValue("modification-markers", true);
    _configView->setConfigValue("word-count", false);
    _configView->setConfigValue("scrollbar-minimap", true);
    _configView->setConfigValue("scrollbar-preview", true);
    _configView->setConfigValue("allow-mark-menu", true);
//    configView->setConfigValue("font", QFont());

    connect(_view, &KTextEditor::View::focusIn, this, [this](KTextEditor::View*) {
      emit focusIn();
    });

    connect(_view, &KTextEditor::View::focusOut, this, [this](KTextEditor::View*) {
      emit focusOut();
    });

    connect(_view, &KTextEditor::View::cursorPositionChanged, this, [this](KTextEditor::View*, const Cursor pos) {
      emit cursorPositionChanged(pos);
    });

    connect(_view, &KTextEditor::View::viewModeChanged, this, [this](KTextEditor::View*, const ViewMode& mode) {
      emit viewModeChanged(mode);
    });

    connect(_view, &KTextEditor::View::viewInputModeChanged, this, [this](KTextEditor::View*, InputMode) {
      emit viewInputModeChanged(_view->viewInputModeHuman());
    });

    connect(_doc.data(), &KTextEditor::Document::documentSavedOrUploaded, this, &Document::saved);

    connect(_doc.data(), &KTextEditor::Document::documentUrlChanged, this, [this](KTextEditor::Document*) {
      emit urlChanged(url());
    });

    connect(_doc.data(), &KTextEditor::Document::documentNameChanged, this, [this](KTextEditor::Document*) {
      emit nameChanged(name());
    });

    connect(_doc.data(), &KTextEditor::Document::modifiedChanged, this, [this](KTextEditor::Document* doc) {
      emit nameChanged(name());
      emit modifiedChanged(doc->isModified());
    });
  }

  void Document::disableViewActionShortcuts() {
    static const QList<KStandardAction::StandardAction> actions = {
      KStandardAction::Save,
      // Edit Menu
      KStandardAction::Undo,
      KStandardAction::Redo,
      KStandardAction::Cut,
      KStandardAction::Copy,
      KStandardAction::Copy,
      KStandardAction::Paste,
      KStandardAction::SelectAll,
      KStandardAction::Deselect,
      KStandardAction::Find,
      KStandardAction::Replace
    };

    for (const KStandardAction::StandardAction& action: actions) {
      _view->action(KStandardAction::name(action))->setShortcut(QKeySequence::UnknownKey);
    }

    action(Action::RELOAD)->setDisabled(true);
    action(Action::ZOOM_IN)->setDisabled(true);
    action(Action::ZOOM_OUT)->setDisabled(true);
  }

  Document::Range Document::findBackwards(const QString& pattern, const SearchOptions options, bool followCursor) {
    const Range documentRange = _doc->documentRange();
    const Cursor start = followCursor
        ? selection()
          ? _view->selectionRange().start()
          : _view->cursorPosition()
        : documentRange.end();
    const Cursor end = documentRange.start();
    const Range searchRange(start, end);
    const QVector<Range> ranges = _doc->searchText(searchRange, pattern, options);

    const Range range = ranges.first();

    if (range.isValid()) {
      _view->setCursorPosition(range.start());
      _view->setSelection(range);
    } else if (followCursor) {
      return findBackwards(pattern, options, false);
    }

    return range;
  }

  Document::Range Document::findForwards(const QString& pattern, const SearchOptions options, bool followCursor) {
    const Range documentRange = _doc->documentRange();
    const Cursor start = followCursor
        ? selection()
          ? _view->selectionRange().end()
          : _view->cursorPosition()
        : documentRange.start();
    const Cursor end = documentRange.end();
    const Range searchRange(start, end);
    const QVector<Range> ranges = _doc->searchText(searchRange, pattern, options);

    const Range range = ranges.first();

    if (range.isValid()) {
      _view->setCursorPosition(range.end());
      _view->setSelection(range);
    } else if (followCursor) {
      return findForwards(pattern, options, false);
    }

    return range;
  }

  bool Document::replaceText(const Range& range, const QString& text) {
    return _doc->replaceText(range, text);
  }

  void Document::setBackgroundColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_BACKGROUND_COLOR, value);
  }

  void Document::setSelectionColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_SELECTION_COLOR, value);
  }

  void Document::setSearchHighlightColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_SEARCH_HIGHLIGHT_COLOR, value);
  }

  void Document::setReplaceHighlightColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_REPLACE_HIGHLIGHT_COLOR, value);
  }

  void Document::setIconBorderColorColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_ICON_BORDER_COLOR, value);
  }

  void Document::setFoldingMarkerColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_FOLDING_MARKER_COLOR, value);
  }

  void Document::setLineNumberColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_LINE_NUMBER_COLOR, value);
  }

  void Document::setCurrentLineNumberColor(const QColor& value) {
    setConfigValue(VIEW_CONFIG_KEY_CURRENT_LINE_NUMBER_COLOR, value);
  }

  void Document::setScheme(const QString& value) {
    setConfigValue(VIEW_CONFIG_KEY_SCHEME, value);

    if (_view) {
      _view->render(this);
    }
  }

  bool Document::setEncoding(const QString& value) {
    return _doc->setEncoding(value);
  }

  void Document::open(const QUrl& url) {
    if (Document::_refs.contains(url)) {
      setDoc(Document::_refs.value(url));
    } else {
      _doc->openUrl(url);
    }

    _view->render(this);
  }

  void Document::save() {
    _doc->documentSave();
  }

  void Document::saveAs(const QString& fileName) {
    _doc->saveAs(QUrl::fromLocalFile(fileName));
  }

  void Document::close() {
    delete _view;

    if (_doc->views().isEmpty()) {
      closeFile();
    } else {
      _view = nullptr;

      _doc->disconnect(this);

      emit closed();
    }
  }

  void Document::closeFile() {
    Document::_refs.remove(_doc->url());

    for (auto view: _doc->views()) {
      delete view;
    }

    _view = nullptr;

    _doc->disconnect(this);
    _doc->closeUrl(true);

    emit closed();
  }

  void Document::setConfigValue(const QString& property, const QVariant& value) {
    if (_view == nullptr) {
      return;
    }

    _configView->setConfigValue(property, value);
  }

  bool Document::_readStateConfig(const KConfigGroup& config) {
    const QString selectionRange = config.readEntry(CONFIG_KEY_SELECTION_RANGE, Range().toString());
    const QString cursorPositionCoordinates = config.readEntry(CONFIG_KEY_CURSOR_POSITION, Cursor().toString());
    const int firstDisplayedLine = config.readEntry(CONFIG_KEY_FIRST_DISPLAYED_LINE, 0);
    Cursor scrollPosition = Cursor(firstDisplayedLine, 0);
    const QString documentUrl = config.readEntry("URL");

    open(documentUrl);

    _doc->readSessionConfig(config);
    _view->readSessionConfig(config);

    _view->setSelection(Range::fromString(selectionRange));
    _view->setCursorPosition(Cursor::fromString(cursorPositionCoordinates));
    _view->setScrollPosition(scrollPosition);

    return true;
  }

  bool Document::_writeStateConfig(KConfigGroup& config) {
    _doc->writeSessionConfig(config);
    _view->writeSessionConfig(config);

    config.writeEntry(CONFIG_KEY_SELECTION_RANGE, _view->selectionRange().toString());
    config.writeEntry(CONFIG_KEY_CURSOR_POSITION, _view->cursorPosition().toString());
    config.writeEntry(CONFIG_KEY_FIRST_DISPLAYED_LINE, _view->firstDisplayedLine(KTextEditor::View::VisibleLine));

    return true;
  }
}  // namespace CodeLineX
