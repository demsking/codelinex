/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_FINDFILEWIDGET_H_
#define COMPONENTS_FINDFILEWIDGET_H_

#include <QFrame>
#include "components/Document.h"

class QEvent;
class QString;
class QObject;
class QWidget;
class QPushButton;
class QHBoxLayout;
class QVBoxLayout;

namespace CodeLineX {
  class LineEdit;
  class IconButton;
  class PushButton;

  class FindFileWidget: public QFrame {
    Q_OBJECT

    QHBoxLayout* _mainLayout = nullptr;
    QVBoxLayout* _inputsLayout = nullptr;
    QVBoxLayout* _buttonsLayout = nullptr;
    QHBoxLayout* _findButtonsLayout = nullptr;
    QHBoxLayout* _replaceButtonsLayout = nullptr;
    QHBoxLayout* _actionsLayout = nullptr;
    IconButton* _closeButton = nullptr;
    LineEdit* _queryLineEdit = nullptr;
    LineEdit* _replaceLineEdit = nullptr;

    IconButton* _findNextButton = nullptr;
    IconButton* _findPreviousButton = nullptr;

    IconButton* _replaceButton = nullptr;
    IconButton* _replaceAllButton = nullptr;

    public:
      explicit FindFileWidget(QWidget* parent);
      ~FindFileWidget() override;

      void setQuery(const QString& pattern);

    public slots:
      void selectAll();
      void showReplaceLayout();
      void hideReplaceLayout();

    protected:
      inline void connectQueryLineEdit();

    protected slots:
      void findPreviousMatch();
      void findNextMatch();
      void findNextMatchLine(const QString& pattern);
      void replaceNextMatch();
      void replaceAllMatch();

    signals:
      void aboutToClose();
      void findNext(const QString& pattern,
                    const Document::SearchOptions,
                    bool followCursor);
      void findPrevious(const QString& pattern, const Document::SearchOptions);
      void replace(const QString& pattern,
                   const Document::SearchOptions,
                   const QString& replaceText,
                   bool replaceAll);

    protected:
      bool eventFilter(QObject*, QEvent*) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_FINDFILEWIDGET_H_
