/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/FindWidget.h"

#include <QEvent>
#include <QWidget>
#include <QKeyEvent>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <CuteIcons.h>
#include <CuteMaterialIcons.h>

#include "components/Label.h"
#include "components/LineEdit.h"

namespace CodeLineX {
  FindWidget::FindWidget(QWidget* parent): QFrame(parent) {
    setProperty("FindWidget", true);

    _titleLabel = new Label(tr("Find"), this);
    _optionsDescriptionLabel = new Label(tr("Find with Options:"), this);
    _headerOptionsLayout = new QHBoxLayout();
    _closeButton = new QPushButton(tr("Close"), this);
    _headerLayout = new QHBoxLayout();

    QFont closeFont = CuteMaterialIcons::font();
    closeFont.setBold(true);

    _closeButton->setProperty("CloseButton", true);
    _closeButton->setFont(closeFont);
    _closeButton->setText(CUTE_MDI_CLOSE);
    _closeButton->setToolTip(tr("Close"));

    _optionsDescriptionLabel->setProperty("OptionsDescriptionLabel", true);

    _headerLayout->addWidget(_titleLabel);
    _headerLayout->addStretch();
    _headerLayout->addWidget(_optionsDescriptionLabel);
    _headerLayout->addLayout(_headerOptionsLayout);
    _headerLayout->addWidget(_closeButton);

    _bodyLayout = new QVBoxLayout();
    _queryInputLayout = new QHBoxLayout();
    _queryLineEdit = new LineEdit(this);
    _queryInputLayout->addWidget(_queryLineEdit);
    _bodyLayout->addLayout(_queryInputLayout);

    _mainLayout = new QVBoxLayout(this);
    _mainLayout->addLayout(_headerLayout);
    _mainLayout->addLayout(_bodyLayout);

    setFocusProxy(_queryLineEdit);

    connect(_closeButton, &QPushButton::pressed, this, &FindWidget::hide);
  }

  FindWidget::~FindWidget() {
    delete _titleLabel;
    delete _optionsDescriptionLabel;
    delete _headerOptionsLayout;
    delete _closeButton;
    delete _headerLayout;
    delete _queryLineEdit;
    delete _queryInputLayout;
    delete _bodyLayout;
    delete _mainLayout;
  }

  void FindWidget::hide() {
    QWidget::hide();
    emit hidden();
  }

  bool FindWidget::eventFilter(QObject* obj, QEvent* event) {
    if (event->type() == QEvent::KeyPress) {
      auto keyEvent = static_cast<QKeyEvent*>(event);

      if (keyEvent->key() == Qt::Key_Escape) {
        hide();

        return true;
      }
    }

    return QFrame::eventFilter(obj, event);
  }
}  // namespace CodeLineX
