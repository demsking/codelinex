/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/StatusBar.h"

#include <QLabel>
#include <QStatusBar>
#include <QHBoxLayout>

#include <KConfigGroup>

#include "core/Menu.h"
#include "components/Document.h"
#include "utils/FileSystemUtils.h"

namespace CodeLineX {
  StatusBar::StatusBar(QWidget* parent): QFrame(parent), Core::StateSession(metaObject()) {
    _fileTypeLabel = new QLabel();
    _fileCursorPositionLabel = new QLabel();
    _fileEncodingLabel = new QLabel();
    _fileEndingLineLabel = new QLabel();
    _filePathLabel = new QLabel();
    _editorModeLabel = new QLabel();

    _layout = new QHBoxLayout(this);

    _editorModeLabel->setToolTip(tr("Editor Mode"));

    _layout->setMargin(0);
    _layout->setSpacing(0);
    _layout->setAlignment(Qt::AlignVCenter);

    _filePathLabel->setFocusPolicy(Qt::NoFocus);
    _filePathLabel->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(_filePathLabel, &QLabel::customContextMenuRequested,
            this, &StatusBar::requestContextFileMenu);

    const QList<QLabel*> items = {
      _filePathLabel,
      _fileCursorPositionLabel,
      _editorModeLabel,
//      fileEndingLineLabel,
      _fileEncodingLabel,
      _fileTypeLabel
    };

    const int lastIndex = items.length() - 1;

    for (int i = 0; i <= lastIndex; i++) {
      items[i]->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
      items[i]->setProperty("StatusBarItem", i);
      _layout->addWidget(items[i]);

      if (i == 0) {
        _layout->addStretch();
      }
    }

    items[0]->setProperty("StatusBarFirstItem", true);
    items[0]->setAlignment(Qt::AlignLeft| Qt::AlignVCenter);
    items[lastIndex]->setProperty("StatusBarLastItem", true);

    setLayout(_layout);

    setProperty("StatusBar", true);
  }

  StatusBar::~StatusBar() {
    QLayoutItem* item = nullptr;

    while ((item = _layout->takeAt(0))) {
      delete item;
    }

    delete _layout;
  }

  void StatusBar::setFileCursorPosition(const Document::Cursor& pos) {
    const int line = pos.line() + 1;
    const int column = pos.column() + 1;
    const QString text = QString("%1:%2").arg(line).arg(column);
    const QString tooltip = QString(tr("Line %1, Column %2")).arg(line).arg(column);

    _fileCursorPositionLabel->setText(text);
    _fileCursorPositionLabel->setToolTip(tooltip);
  }

  void StatusBar::setViewMode(Document* document) {
    if (document->viewMode() >= Document::ViewMode::ViModeNormal) {
      _editorModeLabel->setText(document->viewModeHuman());
      _editorModeLabel->show();
    } else {
      _editorModeLabel->hide();
    }
  }

  void StatusBar::setFileEncodingLabel(Document* document) {
    auto encoding = document->encoding();
    auto tooltip = QString(tr("This file uses %1 encoding")).arg(encoding);

    _fileEncodingLabel->setText(encoding);
    _fileEncodingLabel->setToolTip(tooltip);
  }

  void StatusBar::setCurrentProjectDir(const QDir& value) {
    _currentProjectDir = value;
  }

  void StatusBar::onNewDocumentAdded(Document* document) {
    setCurrentDocument(document);

    connect(document, &Document::focusIn, this, [this, document]() {
      setCurrentDocument(document);
    });

    connect(document, &Document::cursorPositionChanged, this, &StatusBar::setFileCursorPosition);
    connect(document, &Document::viewModeChanged, this, [this, document]() {
      setViewMode(document);
    });

    connect(document, &Document::closed, [this, document]() {
      document->disconnect(this);
    });
  }

  void StatusBar::setCurrentDocument(Document* document) {
    auto mode = document->mode() == "Normal"
      ? document->mimeType()
      : document->mode();

    _fileTypeLabel->setText(mode);
    _fileEndingLineLabel->setText(document->endOfLine());
    _filePathLabel->setText(FileSystemUtils::relativePath(document->url(), _currentProjectDir));

    setViewMode(document);
    setFileEncodingLabel(document);
    setFileCursorPosition(document->cursorPosition());

    _currentDocument = document;
  }

  void StatusBar::requestContextFileMenu(const QPoint& pos) {
    if (_currentDocument) {
      emit triggerCopyFilePathContextMenu(mapToGlobal(pos), _currentDocument->url());
    }
  }

  bool StatusBar::_readStateConfig(const KConfigGroup&) {
    return false;
  }

  bool StatusBar::_writeStateConfig(KConfigGroup&) {
    return false;
  }
}  // namespace CodeLineX
