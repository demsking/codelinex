/**
 * HPX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_UNLOADDOCUMENT_H_
#define COMPONENTS_UNLOADDOCUMENT_H_

#include <QUrl>
#include <QMap>

#include <KConfigGroup>
#include <hpx-tabview/TabviewWidget.h>

#include "core/StateSession.h"

class QWidget;

namespace HPX {
  class TabviewItem;
}

namespace CodeLineX {
  class Document;

  class UnloadDocument: public HPX::TabviewWidget, public Core::StateSession {
    Q_OBJECT

    QUrl _url;
    QString _name;
    QMap<QString, QString> _entries;
    KConfigGroup _config;
    HPX::TabviewItem* _item = nullptr;

    public:
      explicit UnloadDocument(QWidget* parent = nullptr);

      QUrl url() const;
      void setUrl(const QUrl& url);

      QString name() const;
      void setName(const QString& name);

      QMap<QString, QString> entries() const;
      void setEntries(const QMap<QString, QString>& entries);

      KConfigGroup config() const;
      void setConfig(const KConfigGroup& config);

      HPX::TabviewItem* item() const;
      void setItem(HPX::TabviewItem* item);

    // TabviewWidget interface
    public:
      void close() override;
      void setFocus() override;

    // StateSession interface
    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;

    signals:
      void loaded(Document*);
  };
}  // namespace CodeLineX

#endif // COMPONENTS_UNLOADDOCUMENT_H_
