/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_EXPLORERWIDGET_H_
#define COMPONENTS_EXPLORERWIDGET_H_

#include <QUrl>
#include <QSet>
#include <QFrame>
#include "core/StateSession.h"
#include "components/AbstractLinkedPanel.h"

class QDir;
class QVBoxLayout;
class QFileSystemModel;

class KConfigGroup;

namespace CodeLineX {
  class TreeView;
  class FileIconProvider;

  namespace Layout {
    class Shell;
  }

  class ExplorerWidget: public AbstractLinkedPanel, public Core::StateSession {
    Q_OBJECT

    QString _dirname;
    FileIconProvider* _iconProvider = nullptr;
    QSharedPointer<QFileSystemModel> _model;

    TreeView* _treeView = nullptr;

    QUrl _currentFileUrl;

    QSet<QString> _loadedPaths;

    public:
      explicit ExplorerWidget(QWidget* parent = nullptr);
      ~ExplorerWidget() override;

      QString dirname() const;
      QDir rootDirectory() const;
      QString rootPath() const;

      void setModel(const QSharedPointer<QFileSystemModel>&);

    signals:
      void rootPathChanged(const QString&);
      void rootDirectoryChanged(const QDir&);
      void currentFileChanged(const QUrl&);

    public slots:
      void setCurrentFileUrl(const QUrl&);
      void onLinkButtonCheck(bool checked) override;
      void setRootPath(const QString&);

    protected slots:
      void setCurrentIndex(const QModelIndex&);
      void showContextFileMenu(const QPoint&);

    protected:
      inline QStringList expandedPaths() const;

    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_EXPLORERWIDGET_H_
