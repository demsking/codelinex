/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/FileList.h"

#include <QWidget>
#include <QVariant>
#include <QStandardItemModel>

#include "models/FileListItemDelegate.h"

namespace CodeLineX {
  FileList::FileList(QWidget* parent): ListView(parent) {
    setProperty("FileList", true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setItemDelegate(new FileListItemDelegate);
    setWordWrap(true);
  }

  QStandardItemModel* FileList::model() const {
    return static_cast<QStandardItemModel*>(ListView::model());
  }

  QVariant FileList::data(FileItem::Role role) const {
    auto index = ListView::model()->index(currentRow(), role);
    auto value = ListView::model()->data(index);

    return value;
  }

  int FileList::currentRow() const {
    return currentIndex().row();
  }

  void FileList::setCurrentRow(const int& row) {
    QStandardItemModel* fileListModel = model();
    QModelIndex index = fileListModel->index(row, 0);

    setCurrentIndex(index);

    emit currentIndexChanged(this, row);
  }
}  // namespace CodeLineX
