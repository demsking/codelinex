/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/OpenDocumentsWidget.h"

#include <QUrl>

#include "models/OpenDocumentListModel.h"
#include "models/OpenDocumentStyledItemDelegate.h"

#include "components/ListView.h"

namespace CodeLineX {
  OpenDocumentsWidget::OpenDocumentsWidget(QWidget* parent): HPX::StackedPanelFrame(parent) {
    _listView = new ListView(this);
    _itemDelegate = new OpenDocumentStyledItemDelegate(this);

    _listView->setProperty("OpenDocumentsListView", true);
    _listView->setContextMenuPolicy(Qt::CustomContextMenu);
    _listView->setItemDelegate(_itemDelegate);

    setProperty("OpenDocumentsWidget", true);
    setStateWidget(State::Ready, _listView);

    connect(_itemDelegate, &OpenDocumentStyledItemDelegate::buttonClicked,
            this, &OpenDocumentsWidget::closeDocument);

    connect(_listView, &ListView::activated, this, &OpenDocumentsWidget::switchToDocument);
    connect(_listView, &ListView::clicked, this, &OpenDocumentsWidget::switchToDocument);

    connect(_listView, &ListView::customContextMenuRequested,
            this, &OpenDocumentsWidget::requestContextFileMenu);
  }

  void OpenDocumentsWidget::setModel(const QSharedPointer<OpenDocumentListModel>& model) {
    _documentListModel = model;

    _listView->setModel(model.data());
  }

  void OpenDocumentsWidget::setCurrentDocumentId(const QString& oid) {
    auto row = _documentListModel->findRow(oid);

    if (row > -1) {
      _listView->setCurrentIndex(_documentListModel->index(row, 0));
    }
  }

  void OpenDocumentsWidget::requestContextFileMenu(const QPoint& pos) {
    auto index = _listView->indexAt(pos);

    if (index.isValid()) {
      auto url = index.data(OpenDocumentListModel::Role::DOCUMENT_URL).toUrl();

      emit triggerFileContextMenuRequest(mapToGlobal(pos), url);
    }
  }

  void OpenDocumentsWidget::switchToDocument(const QModelIndex& index) {
    auto oid = index.data(OpenDocumentListModel::Role::DOCUMENT_OID).toString();

    emit currentDocumentIdChanged(oid);
  }

  void OpenDocumentsWidget::closeDocument(const QModelIndex& index) {
    auto oid = index.data(OpenDocumentListModel::Role::DOCUMENT_OID).toString();

    emit triggerFileCloseRequest(oid);
  }
}  // namespace CodeLineX
