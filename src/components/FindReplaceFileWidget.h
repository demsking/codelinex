/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_FINDREPLACEFILEWIDGET_H_
#define COMPONENTS_FINDREPLACEFILEWIDGET_H_

#include "layouts/Floating.h"

class QEvent;
class QWidget;

namespace CodeLineX {
  class FindFileWidget;

  class FindReplaceFileWidget: public Layout::Floating {
    Q_OBJECT

    FindFileWidget* _findFileWidget = nullptr;

    public:
      explicit FindReplaceFileWidget(QWidget* parent);
      ~FindReplaceFileWidget() override;

      FindFileWidget* widget() const;

    public slots:
      void showReplaceLayout();
      void hideReplaceLayout();

    protected:
      void onParentResize(const QSize&) override;
      bool eventFilter(QObject*, QEvent*) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_FINDREPLACEFILEWIDGET_H_
