/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_OUTLINEWIDGET_H_
#define COMPONENTS_OUTLINEWIDGET_H_

#include <QMap>
#include <KTextEditor/Cursor>
#include <memory>

#include "lib/lsp/Manager.h"
#include "lib/lspclient/lspclientserver.h"
#include "lib/lspclient/lspclientservermanager.h"
#include "components/AbstractLinkedPanel.h"

#include "models/OutlineItem.h"

class QState;
class QStandardItem;
class QStateMachine;
class QStandardItemModel;

namespace CodeLineX {
  class TreeView;
  class Document;
  class PushButton;
  class OutlineItem;
  class OutilineSortFilterProxyModel;

  // (oid -> QStandardItemModel)
  typedef QMap<QString, std::shared_ptr<QStandardItemModel>> OutlineWidgetCache;

  class OutlineWidget: public AbstractLinkedPanel {
    Q_OBJECT

    Document* _currentDocument = nullptr;
    std::shared_ptr<QStandardItemModel> _outline;
    OutilineSortFilterProxyModel* _proxyModel = nullptr;
    QSharedPointer<LSPClientServerManager> _lspManager;

    PushButton* _sortButton = nullptr;
    QStateMachine* _sortStateMachine = nullptr;
    QState* _declarationSortState = nullptr;
    QState* _ascendingSortState = nullptr;
    QState* _descendingSortState = nullptr;

    TreeView* _treeView = nullptr;

    // (oid -> QStandardItemModel)
    static QMap<QString, std::shared_ptr<QStandardItemModel>> _outlineCache;

    public:
      explicit OutlineWidget(const LSP::Manager::Pointer&, QWidget* parent = nullptr);

      Document* currentDocument() const;

    signals:
      void loading();
      void loaded();

    protected:
      void initSortButton();
      void clearTreeviewModel();

      bool isExpanable(QStandardItem*) const;
      QStandardItem* getCurrentItem(QStandardItem*, int line);

    public slots:
      void selectSymbol(const KTextEditor::Cursor&);
      void setCurrentDocument(Document*);
      void sortByDeclaration();
      void sortByNameAsc();
      void sortByNameDesc();

    protected slots:
      void onLinkButtonCheck(bool checked) override;

    protected:
      void loadDocumentOutline(Document*, bool useCache = true);
      void findDocumentServer(Document*);
      void sendDocumentSymbolsRequest(Document*, QSharedPointer<LSPClientServer>);
      void onDocumentSymbolsResponse(const QList<LSPSymbolInformation>&);
      void invalidateCacheOf(const QString& documentObjectId);
      inline void connectDocument(Document*);
      inline void disconnectDocument(Document*);
      void setOutlineSourceModel(Document*, std::shared_ptr<QStandardItemModel>);

    protected:
      inline const QIcon* getIcon(const LSPSymbolInformation&, OutlineItem* parent) const;

      void makeNodes(
        const QList<LSPSymbolInformation>& symbols,
        QStandardItemModel* model,
        OutlineItem* parent);

      void goToSymbol(const QModelIndex&);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_OUTLINEWIDGET_H_
