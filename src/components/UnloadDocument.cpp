/**
 * HPX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/UnloadDocument.h"

#include <hpx-tabview/TabviewItem.h>

#include "components/Document.h"

namespace CodeLineX {
  UnloadDocument::UnloadDocument(QWidget* parent)
    : HPX::TabviewWidget(parent),
      Core::StateSession(metaObject()) {}

  QUrl UnloadDocument::url() const {
    return _url;
  }

  void UnloadDocument::setUrl(const QUrl& url) {
    _url = url;
  }

  QString UnloadDocument::name() const {
    return _name;
  }

  void UnloadDocument::setName(const QString& name) {
    _name = name;
  }

  QMap<QString, QString> UnloadDocument::entries() const {
    return _entries;
  }

  void UnloadDocument::setEntries(const QMap<QString, QString>& entries) {
    _entries = entries;
  }

  KConfigGroup UnloadDocument::config() const {
    return _config;
  }

  void UnloadDocument::setConfig(const KConfigGroup& config) {
    _config = config;
  }

  HPX::TabviewItem* UnloadDocument::item() const {
    return _item;
  }

  void UnloadDocument::setItem(HPX::TabviewItem* item) {
    _item = item;
  }

  void UnloadDocument::close() {
    emit closed();
  }

  void UnloadDocument::setFocus() {
    const QString& oid = objectId();
    Document* document = Object::get<Document>(oid, parentWidget());

    document->readStateConfig(oid, _config);
    _item->setWidget(document);

    emit loaded(document);

    document->setFocus();
  }

  bool UnloadDocument::_readStateConfig(const KConfigGroup&) {
    return true;
  }

  bool UnloadDocument::_writeStateConfig(KConfigGroup& config) {
    for (const QString& key: _entries.keys()) {
      config.writeEntry(key, _entries[key]);
    }

    return true;
  }
}  // namespace CodeLineX
