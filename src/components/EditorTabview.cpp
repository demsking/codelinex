/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/EditorTabview.h"

#include <QStyle>
#include <QWidget>
#include <QKeyEvent>
#include <QVBoxLayout>
#include <QApplication>
#include <QCoreApplication>

#include <KConfigGroup>

#include <hpx-tabview/Tabview.h>
#include <hpx-tabview/TabviewModel.h>
#include <hpx-tabview/TabviewItem.h>
#include <hpx-tabview/TabviewWidget.h>
#include <hpx-tabview/TabviewBar.h>
#include <hpx-tabview/TabviewBarButton.h>

#include "components/UnloadDocument.h"
#include "components/FindFileWidget.h"
#include "components/FindReplaceFileWidget.h"

#define CONFIG_KEY_CURRENT_INDEX "Current Index"
#define CONFIG_KEY_CHILDREN "Children"

namespace CodeLineX {
  EditorTabview::EditorTabview(QWidget* parent): EditorLayoutWidget(parent), Core::StateSession(metaObject()) {
    setProperty("ViewDocument", true);

    _model = new HPX::TabviewModel(this);

    _tabview = new HPX::Tabview(this);
    _tabview->setModel(_model);
    _tabview->disableCtrlTabNavigation();

    _layout = new QVBoxLayout(this);
    _layout->setMargin(0);
    _layout->setSpacing(0);
    _layout->addWidget(_tabview);

    setFocusProxy(_tabview);

    connect(_tabview, &HPX::Tabview::focusIn, this, &EditorTabview::polish);
    connect(_tabview, &HPX::Tabview::focusIn, this, &EditorTabview::focused);
    connect(_tabview, &HPX::Tabview::focusOut, this, &EditorTabview::polish);

    connect(_model, &HPX::TabviewModel::rowsRemoved, this, [this]() {
      if (_tabview->isEmpty()) {
        emit closed();
      }
    });

    connect(_model, &HPX::TabviewModel::rowsInserted, this, [this](const QModelIndex& parent, int first, int last) {
      for (int i = first; i <= last; i++) {
        auto index = _model->index(i, 0, parent);
        auto document = index.data(HPX::TabviewItem::WidgetRole).value<Document*>();
        auto tabButton = _tabview->tabviewBar()->tabButton(i);

        if (document) {
          tabButton->setContextMenuPolicy(Qt::CustomContextMenu);

          connect(tabButton, &HPX::TabviewBarButton::customContextMenuRequested, [this, document](const QPoint& pos) {
            emit triggerFileContextMenuRequest(mapToGlobal(pos), document->url());
          });
        }
      }
    });
  }

  int EditorTabview::currentIndex() const {
    return _tabview->currentIndex();
  }

  HPX::TabviewWidget* EditorTabview::widget(int index) {
    return _model->widget(index);
  }

  HPX::TabviewWidget* EditorTabview::widget(const QString& oid) {
    for (int i = 0; i < _model->rowCount(); i++) {
      HPX::TabviewWidget* widget = _model->widget(i);
      auto object = dynamic_cast<Core::Object*>(widget);

      if (object->objectId() == oid) {
        return widget;
      }
    }

    return nullptr;
  }

  Document* EditorTabview::document(int index) const {
    return qobject_cast<Document*>(_model->widget(index));
  }

  Document* EditorTabview::document(const QString& oid) {
    HPX::TabviewWidget* widgetPtr = widget(oid);

    if (widgetPtr) {
      if (_unloadDocuments.contains(oid)) {
        widgetPtr->setFocus();
      }

      return qobject_cast<Document*>(widget(oid));
    }

    return nullptr;
  }

  Document* EditorTabview::currentDocument() const {
    if (isEmpty()) {
      return nullptr;
    }

    return document(currentIndex());
  }

  HPX::TabviewWidget* EditorTabview::currentWidget() const {
    return _model->widget(currentIndex());
  }

  void EditorTabview::setCurrentWidget(HPX::TabviewWidget* widget) {
    _tabview->setCurrentWidget(widget);
  }

  QList<Document*> EditorTabview::documents() const {
    QList<Document*> docs;

    for (int i = 0; i < _model->rowCount(); i++) {
      HPX::TabviewWidget* widget = _model->widget(i);
      auto document = qobject_cast<Document*>(widget);

      if (document) {
        docs.push_back(document);
      }
    }

    return docs;
  }

  int EditorTabview::count() const {
    return _tabview->count();
  }

  int EditorTabview::indexOf(const QUrl& url) const {
    for (int i = 0; i < _model->rowCount(); i++) {
      HPX::TabviewWidget* widget = _model->widget(i);
      auto document = qobject_cast<Document*>(widget);

      if (document) {
        if (document->url() == url) {
          return i;
        }
      } else {
        auto object = dynamic_cast<Core::Object*>(widget);
        const QString& oid = object->objectId();

        if (_unloadDocuments.contains(oid)) {
          UnloadDocument* unloadDocument = _unloadDocuments[oid];

          if (unloadDocument->url() == url) {
            return i;
          }
        }
      }
    }

    return -1;
  }

  int EditorTabview::indexOf(HPX::TabviewWidget* widget) const {
    return _tabview->indexOf(widget);
  }

  bool EditorTabview::isEmpty() const {
    return _tabview->isEmpty();
  }

  void EditorTabview::setFocus() {
    currentWidget()->setFocus();
  }

  HPX::TabviewModel* EditorTabview::model() const {
    return _model;
  }

  void EditorTabview::showFindView() {
    Document* document = currentDocument();

    if (!document) {
      return;
    }

    if (_findReplaceWidget == nullptr) {
      createFindView();
    }

    const QString focusedWord = document->selection()
      ? document->selectionText()
      : document->wordAt(document->cursorPosition());

    FindFileWidget* widget = _findReplaceWidget->widget();

    if (focusedWord.isEmpty()) {
      widget->selectAll();
    } else {
      widget->setQuery(focusedWord);
    }

    if (!_findReplaceWidget->isVisible()) {
      _findReplaceWidget->show();
      _findReplaceWidget->hideReplaceLayout();
    }

    widget->setFocus();
  }

  void EditorTabview::showFindReplaceView() {
    showFindView();

    _findReplaceWidget->showReplaceLayout();
  }

  void EditorTabview::createFindView() {
    _findReplaceWidget = new FindReplaceFileWidget(this);
    FindFileWidget* widget = _findReplaceWidget->widget();

    installEventFilter(_findReplaceWidget);

    connect(widget, &FindFileWidget::findNext, this, &EditorTabview::onFindNext);
    connect(widget, &FindFileWidget::findPrevious, this, &EditorTabview::onFindPrevious);
    connect(widget, &FindFileWidget::replace, this, &EditorTabview::onReplace);

    connect(_findReplaceWidget, &FindReplaceFileWidget::closed, this, [this]() {
      setFocus();
    });
  }

  void EditorTabview::onFindNext(const QString& pattern, const SearchOptions options, bool followCursor) {
    Document* document = currentDocument();

    if (document) {
      document->findForwards(pattern, options, followCursor);
    }
  }

  void EditorTabview::onFindPrevious(const QString& pattern, const SearchOptions options) {
    Document* document = currentDocument();

    if (document) {
      document->findBackwards(pattern, options, true);
    }
  }

  void EditorTabview::onReplace(
    const QString& pattern,
    const SearchOptions options,
    const QString& replaceText,
    bool replaceAll
  ) {
    Document* document = currentDocument();

    if (document) {
      do {
        Document::Range range = document->findForwards(pattern, options, true);

        if (range.isValid()) {
          document->replaceText(range, replaceText);
        } else {
          break;
        }
      } while (replaceAll);
    }
  }

  void EditorTabview::connectDocument(Document* document, HPX::TabviewItem* item) {
    connect(document, &Document::nameChanged, this, [item](const QString& name) {
      item->setTitle(name);
    });

    connect(document, &Document::urlChanged, this, [item](const QUrl& url) {
      item->setToolTip(url.toLocalFile());
    });

    connect(document, &Document::focusIn, this, &EditorTabview::focused);
  }

  void EditorTabview::onDocumentClose(Document* document) {
    document->disconnect(this);
  }

  void EditorTabview::untitleDocument() {
    openDocument(new Document(this));
  }

  Document* EditorTabview::openDocument(const QUrl& url) {
    const int index = indexOf(url);

    if (index != -1) {
      _tabview->setCurrentIndex(index);
      _tabview->widget(index)->setFocus();

      return document(index);
    }

    if (_tabview->count() == 1) {
      Document* doc = document(static_cast<int>(0));

      if (doc->url().isEmpty()) {
        doc->open(url);

        return doc;
      }
    }

    return openDocument(new Document(url, this));
  }

  Document* EditorTabview::openDocument(Document* document) {
    HPX::TabviewItem* item = new HPX::TabviewItem();

    item->setTitle(document->name());
    item->setToolTip(document->path());
    item->setWidget(document);
    item->setData(document->url(), TabviewItemRole::UrlRole);

    _model->add(item);

    connectDocument(document, item);

    emit tabviewItemCreated(item);
    emit documentCreated(document);

    document->setFocus();

    connect(document, &Document::closed, this, [this, document]() {
      onDocumentClose(document);
    });

    return document;
  }

  void EditorTabview::close() {
    while (_tabview->count()) {
      _tabview->widget(0)->close();
    }
  }

  void EditorTabview::polish() const {
    auto app = qobject_cast<QApplication*>(QCoreApplication::instance());
    HPX::TabviewWidget* widget = _tabview->currentWidget();

    style()->unpolish(app);
    style()->polish(app);

    widget->style()->unpolish(widget);
    widget->style()->polish(widget);
  }

  bool EditorTabview::_readStateConfig(const KConfigGroup& config) {
    const int currentIndex = config.readEntry(CONFIG_KEY_CURRENT_INDEX, 0);

    restoreChildrenState(config);
    _tabview->setCurrentIndex(currentIndex);

    HPX::TabviewWidget* widget = _tabview->currentWidget();

    if (widget) {
      widget->setFocus();
    }

    return true;
  }

  bool EditorTabview::_writeStateConfig(KConfigGroup& config) {
    if (writeChildrenState(config)) {
      config.writeEntry(CONFIG_KEY_CURRENT_INDEX, _tabview->currentIndex());

      return true;
    }

    return false;
  }

  QString EditorTabview::documentNameKey(const QString& oid) {
    return oid + QStringLiteral(".name");
  }

  QString EditorTabview::documentUrlKey(const QString& oid) {
    return oid + QStringLiteral(".url");
  }

  bool EditorTabview::writeChildrenState(KConfigGroup& config) const {
    QStringList children;

    for (int i = 0; i < _tabview->count(); i++) {
      HPX::TabviewWidget* widget = _model->widget(i);
      auto session = dynamic_cast<Core::StateSession*>(widget);
      const QString& oid = session->objectId();
      const QString& urlKey = EditorTabview::documentUrlKey(oid);
      const QString& titleKey = EditorTabview::documentNameKey(oid);

      if (_unloadDocuments.contains(oid)) {
        UnloadDocument* unloadDocument = _unloadDocuments[oid];

        config.writeEntry(urlKey, unloadDocument->url());
        config.writeEntry(titleKey, unloadDocument->name());

        unloadDocument->writeStateConfig(config);

        children.push_back(oid);
      } else {
        auto document = qobject_cast<Document*>(widget);

        if (!document->url().isEmpty()) {
          document->writeStateConfig(config);

          config.writeEntry(urlKey, document->url());
          config.writeEntry(titleKey, document->name());

          children.push_back(oid);
        }
      }
    }

    if (children.empty()) {
      return false;
    }

    config.writeEntry(CONFIG_KEY_CHILDREN, children);

    return true;
  }

  void EditorTabview::restoreChildrenState(const KConfigGroup& config) {
    const QStringList& children = config.readEntry(CONFIG_KEY_CHILDREN, QStringList());

    for (const QString& oid: children) {
      const QUrl url = config.readEntry(EditorTabview::documentUrlKey(oid), QUrl());
      const QString name = config.readEntry(EditorTabview::documentNameKey(oid), "Untitled");

      HPX::TabviewItem* item = new HPX::TabviewItem();
      UnloadDocument* unloadDocument = Object::get<UnloadDocument>(oid, this);

      unloadDocument->setName(name);
      unloadDocument->setUrl(url);
      unloadDocument->setConfig(config);
      unloadDocument->setEntries(config.group(oid).entryMap());
      unloadDocument->setItem(item);

      _unloadDocuments.insert(oid, unloadDocument);

      item->setTitle(name);
      item->setToolTip(url.toLocalFile());
      item->setWidget(unloadDocument);
      item->setData(url, TabviewItemRole::UrlRole);

      _model->add(item);

      connect(unloadDocument, &UnloadDocument::closed, this, [this, unloadDocument]() {
        _unloadDocuments.remove(unloadDocument->objectId());

        delete unloadDocument;
      });

      connect(unloadDocument, &UnloadDocument::loaded, this, [this, item, unloadDocument](Document* document) {
        unloadDocument->disconnect(this);
        _unloadDocuments.remove(unloadDocument->objectId());

        connectDocument(document, item);

        emit documentCreated(document);

        document->setFocus();

        connect(document, &Document::closed, this, [this, document]() {
          onDocumentClose(document);
        });

        delete unloadDocument;
      });

      emit unloadDocumentCreated(unloadDocument);
    }
  }
}  // namespace CodeLineX
