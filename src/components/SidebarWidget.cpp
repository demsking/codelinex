/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/SidebarWidget.h"

#include <QObject>
#include <QWidget>
#include <QSplitter>
#include <QVBoxLayout>
#include <QFileSystemModel>

#include <hpx-stacked-panel/StackedPanel.h>
#include <hpx-stacked-panel/StackedPanelItem.h>
#include <hpx-stacked-panel/StackedPanelModel.h>

#include "components/ExplorerWidget.h"
#include "components/OutlineWidget.h"
#include "components/Document.h"
#include "components/UnloadDocument.h"
#include "components/OpenDocumentsWidget.h"
#include "components/EditorTabview.h"

#include "models/OpenDocumentListModel.h"

#include "lib/lspclient/lspclientserver.h"
#include "lib/lspclient/lspclientservermanager.h"

static const QString CONFIG_KEY_SPLITTER_STATE = QStringLiteral("State");
static const QString CONFIG_KEY_CURRENT_ITEM = QStringLiteral("Current Item");

namespace CodeLineX {
  SidebarWidget::SidebarWidget(const LSP::Manager::Pointer& manager, QWidget* parent)
    : QFrame(parent),
      Core::StateSession(metaObject()),
     _lspManager(manager) {
    setProperty("SidebarWidget", true);

    _layout = new QVBoxLayout(this);
    _splitter = new QSplitter(this);

    _panelModel = new HPX::StackedPanelModel(this);
    _stackedPanel = new HPX::StackedPanel(this);

    _explorerWidget = new ExplorerWidget(this);

    _stackedPanel->setModel(_panelModel);
    _stackedPanel->setMinimumHeight(150);

    _splitter->addWidget(_stackedPanel);
    _splitter->setOrientation(Qt::Vertical);
    _splitter->setCollapsible(0, false);

    _layout->setMargin(0);
    _layout->setSpacing(0);
    _layout->addWidget(_splitter, 1);

    _stackedPanel->setFreezed(true);

    initProjectsPanel();
    initOpenDocumentsPanel();
    initSymbolsOutlinePanel();

    _stackedPanel->setFreezed(false);
    _stackedPanel->setCurrentIndex(0);

    connect(_explorerWidget, &ExplorerWidget::currentFileChanged,
            this, &SidebarWidget::currentFileChanged);

    connect(_openDocumentWidget, &OpenDocumentsWidget::currentDocumentIdChanged,
            this, &SidebarWidget::currentDocumentIdChanged);

    connect(_stackedPanel, &HPX::StackedPanel::currentWidgetChanged, [this](auto widget) {
      emit currentItemChanged(item(widget));
    });
  }

  ExplorerWidget* SidebarWidget::explorer() const {
    return _explorerWidget;
  }

  SidebarWidget::Item SidebarWidget::currentItem() const {
    return item(_stackedPanel->currentWidget());
  }

  void SidebarWidget::setFilesystemModel(const QSharedPointer<QFileSystemModel>& model) {
    _explorerWidget->setModel(model);
  }

  void SidebarWidget::setOpenDocumentListModel(const QSharedPointer<OpenDocumentListModel>& model) {
    _openDocumentWidget->setModel(model);
  }

  void SidebarWidget::setCurrentItem(Item item) {
    switch (item) {
      case Item::Projects:
        _stackedPanel->setCurrentWidget(_explorerWidget);
        break;

      case Item::OpenDocuments:
        _stackedPanel->setCurrentWidget(_openDocumentWidget);
        break;

      case Item::Outline:
        _stackedPanel->setCurrentWidget(_outlineWidget);
        break;
    }
  }

  void SidebarWidget::setCurrentDocument(Document* document) {
    _outlineWidget->setCurrentDocument(document);
    _explorerWidget->setCurrentFileUrl(document->url());
    _openDocumentWidget->setCurrentDocumentId(document->objectId());
  }

  SidebarWidget::Item SidebarWidget::item(HPX::StackedPanelWidget* widget) const {
    if (dynamic_cast<OpenDocumentsWidget*>(widget)) {
      return Item::OpenDocuments;
    }

    if (dynamic_cast<OutlineWidget*>(widget)) {
      return Item::Outline;
    }

    return Item::Projects;
  }

  void SidebarWidget::initProjectsPanel() {
    _panelModel->add(new HPX::StackedPanelItem(tr("Projects"), _explorerWidget));
  }

  void SidebarWidget::initSymbolsOutlinePanel() {
    _outlineWidget = new OutlineWidget(_lspManager, this);
    auto item = new HPX::StackedPanelItem(tr("Symbols Outline"), _outlineWidget);

    _panelModel->add(item);
  }

  void SidebarWidget::initOpenDocumentsPanel() {
    _openDocumentWidget = new OpenDocumentsWidget(this);
    auto item = new HPX::StackedPanelItem(tr("Open Documents"), _openDocumentWidget);

    connect(_openDocumentWidget, &OpenDocumentsWidget::triggerFileContextMenuRequest,
            this, &SidebarWidget::fileContextMenuRequested);

    connect(_openDocumentWidget, &OpenDocumentsWidget::triggerFileCloseRequest,
            this, &SidebarWidget::fileCloseRequested);

    _panelModel->add(item);
  }

  bool SidebarWidget::_readStateConfig(const KConfigGroup& config) {
    auto itemIndex = config.readEntry(CONFIG_KEY_CURRENT_ITEM, 0);
    auto item = static_cast<Item>(itemIndex);
    auto shellState = config.readEntry(CONFIG_KEY_SPLITTER_STATE, QByteArray());

    setCurrentItem(item);
    _splitter->restoreState(shellState);

    restoreChildrenState(config);

    return true;
  }

  bool SidebarWidget::_writeStateConfig(KConfigGroup& config) {
    config.writeEntry(CONFIG_KEY_CURRENT_ITEM, static_cast<int>(currentItem()));
    config.writeEntry(CONFIG_KEY_SPLITTER_STATE, _splitter->saveState());

    return writeChildrenState(config);
  }

  bool SidebarWidget::writeChildrenState(KConfigGroup& config) const {
    Q_UNUSED(config)

    return true;
  }

  void SidebarWidget::restoreChildrenState(const KConfigGroup& group) {
    Q_UNUSED(group)
  }
}  // namespace CodeLineX
