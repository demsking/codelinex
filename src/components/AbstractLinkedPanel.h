/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_ABSTRACTLINKEDPANEL_H_
#define COMPONENTS_ABSTRACTLINKEDPANEL_H_

#include <hpx-stacked-panel/StackedPanelFrame.h>

class QWidget;

namespace CodeLineX {
  class PushButton;

  class AbstractLinkedPanel: public HPX::StackedPanelFrame {
    Q_OBJECT

    PushButton* _linkButton = nullptr;

    public:
      explicit AbstractLinkedPanel(QWidget* parent);

      bool isLinkRefEnabled() const;
      void setLinkRefEnabked(bool enabled);

    public slots:
      virtual void onLinkButtonCheck(bool checked) = 0;

    protected slots:
      virtual void onCurrentState(State);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_ABSTRACTLINKEDPANEL_H_
