/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/FindReplaceFileWidget.h"

#include <QEvent>
#include <QWidget>
#include <QHBoxLayout>

#include "components/Label.h"
#include "components/LineEdit.h"
#include "components/PushButton.h"
#include "components/FindFileWidget.h"

namespace CodeLineX {
  FindReplaceFileWidget::FindReplaceFileWidget(QWidget* parent): Layout::Floating(parent) {
    _findFileWidget = new FindFileWidget(parent);

    setWidget(_findFileWidget);
    setProperty("FindReplaceFileWidget", true);

    connect(_findFileWidget, &FindFileWidget::aboutToClose, this, &FindReplaceFileWidget::hide);
  }

  FindReplaceFileWidget::~FindReplaceFileWidget() {
    delete _findFileWidget;
  }

  FindFileWidget* FindReplaceFileWidget::widget() const {
    return _findFileWidget;
  }

  void FindReplaceFileWidget::showReplaceLayout() {
    _findFileWidget->showReplaceLayout();
    _findFileWidget->setFixedHeight(56);
  }

  void FindReplaceFileWidget::hideReplaceLayout() {
    _findFileWidget->hideReplaceLayout();
    _findFileWidget->setFixedHeight(32);
  }

  void FindReplaceFileWidget::onParentResize(const QSize& size) {
    const int width = 350;
    const int height = 32;
    const int margin = 10;
    const int x = size.width() - width - margin;
    const int y = 32 + margin;

    _findFileWidget->setGeometry(x, y, width, height);
  }

  bool FindReplaceFileWidget::eventFilter(QObject* obj, QEvent* event) {
    return Layout::Floating::eventFilter(obj, event);
  }
}  // namespace CodeLineX
