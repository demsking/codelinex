/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_SIDEBARWIDGET_H_
#define COMPONENTS_SIDEBARWIDGET_H_

#include <QFrame>
#include <QSharedPointer>

#include "core/StateSession.h"
#include "lib/lsp/Manager.h"

class QWidget;
class QSplitter;
class QVBoxLayout;
class QFileSystemModel;

class KConfigGroup;
class LSPClientServer;
class LSPClientServerManager;

namespace HPX {
  class StackedPanel;
  class StackedPanelModel;
  class StackedPanelWidget;
}

namespace CodeLineX {
  class Document;
  class ExplorerWidget;
  class OutlineWidget;
  class EditorTabview;
  class OpenDocumentsWidget;
  class OpenDocumentListModel;

  class SidebarWidget: public QFrame, public Core::StateSession {
    Q_OBJECT

    QVBoxLayout* _layout = nullptr;
    QSplitter* _splitter = nullptr;

    HPX::StackedPanelModel* _panelModel = nullptr;
    HPX::StackedPanel* _stackedPanel = nullptr;

    ExplorerWidget* _explorerWidget = nullptr;
    OutlineWidget* _outlineWidget = nullptr;
    OpenDocumentsWidget* _openDocumentWidget = nullptr;

    QSharedPointer<LSPClientServerManager> _lspManager;

    public:
      enum class Item {
        Projects,
        OpenDocuments,
        Outline
      };

      explicit SidebarWidget(const LSP::Manager::Pointer&, QWidget* parent = nullptr);

      ExplorerWidget* explorer() const;
      Item currentItem() const;

      void setFilesystemModel(const QSharedPointer<QFileSystemModel>&);
      void setOpenDocumentListModel(const QSharedPointer<OpenDocumentListModel>&);

    signals:
      void currentItemChanged(Item);
      void currentFileChanged(const QUrl);
      void currentDocumentIdChanged(const QString oid);
      void fileContextMenuRequested(const QPoint&, const QUrl&);
      void fileCloseRequested(const QString& oid);

    public slots:
      void setCurrentItem(Item);
      void setCurrentDocument(Document*);

    protected:
      Item item(HPX::StackedPanelWidget*) const;
      inline void initProjectsPanel();
      inline void initSymbolsOutlinePanel();
      inline void initOpenDocumentsPanel();

    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;

    protected:
      bool writeChildrenState(KConfigGroup&) const;
      void restoreChildrenState(const KConfigGroup&);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_SIDEBARWIDGET_H_
