/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_FINDPROJECTWIDGET_H_
#define COMPONENTS_FINDPROJECTWIDGET_H_

#include "components/FindWidget.h"

class QWidget;
class QHBoxLayout;

namespace CodeLineX {
  class LineEdit;
  class PushButton;

  class FindProjectWidget: public FindWidget {
    Q_OBJECT

    PushButton* _findAllButton = nullptr;

    QHBoxLayout* _replaceInputLayout = nullptr;
    LineEdit* _replaceLineEdit = nullptr;
    PushButton* _replaceAllButton = nullptr;

    public:
      explicit FindProjectWidget(QWidget* parent);
      ~FindProjectWidget() override;

      virtual void find() = 0;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_FINDPROJECTWIDGET_H_
