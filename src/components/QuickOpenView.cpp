/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/QuickOpenView.h"

#include <QEvent>
#include <QKeyEvent>
#include <QLabel>
#include <QWidget>
#include <QLineEdit>
#include <QSortFilterProxyModel>

#include "components/FileList.h"
#include "models/FileListModel.h"
#include "models/FileListProxyModel.h"
#include "utils/FileSystemUtils.h"

#define PAGE_DOWN_KEY_INDEX_FACTOR 5

namespace CodeLineX {
  QuickOpenView::QuickOpenView(QWidget* parent): Overlay(parent) {
    setProperty("QuickOpenView", true);
    setAlignment(Qt::AlignCenter | Qt::AlignTop);

    _input = new QLineEdit;
    _fileView = new FileList;
    _emptyLabel = new QLabel(tr("No files for this filter"));

    _fileListModel = new FileListModel(this);
    _proxyModel = new FileListProxyModel(this);

    _input->setProperty("FilterInput", true);

    _emptyLabel->setProperty("EmptyLabel", true);
    _emptyLabel->setVisible(false);
    _emptyLabel->setAlignment(Qt::AlignCenter);

    _proxyModel->setSourceModel(_fileListModel);
    _proxyModel->setRecursiveFilteringEnabled(true);
    _proxyModel->addIgnoreWildcards({ "node_modules/", "dist/", "coverage/", "debug/", "release/" });

    _fileView->setModel(_proxyModel);

    addWidget(_input);
    addWidget(_emptyLabel);
    addWidget(_fileView);

    connect(_input, &QLineEdit::textChanged, _proxyModel, &FileListProxyModel::setFilterWildcard);
    connect(_fileView, &FileList::activated, this, &QuickOpenView::emitSelected);
    connect(_input, &QLineEdit::textChanged, this, [this]() {
      const int rowCount = _proxyModel->rowCount();
      const bool hidden = rowCount == 0;

      _emptyLabel->setVisible(hidden);
      _fileView->setHidden(hidden);
    });
  }

  QuickOpenView::~QuickOpenView() {
    delete _input;
    delete _fileView;
    delete _proxyModel;
    delete _fileListModel;
  }

  const QDir& QuickOpenView::rootDir() const {
    return _baseDir;
  }

  void QuickOpenView::setRootDir(const QDir& rootDir) {
    if (rootDir == _baseDir) {
      return;
    }

    QDir basedir = rootDir;
    QStringList files = FileSystemUtils::files(rootDir, 10);

    _fileListModel->clear();
    basedir.cdUp();

    for (const QString& file: files) {
      _fileListModel->appendRow(QUrl::fromLocalFile(file), basedir);
    }

    _baseDir = rootDir;
  }

  void QuickOpenView::show() {
    Overlay::show();

    _input->clear();
    _input->setFocus();

    _fileView->setCurrentRow(0);
    _fileView->scrollTo(_fileView->model()->index(0, 0));
  }

  int QuickOpenView::nextIndex(int factor) const {
    const int currentRowNumber = _fileView->currentRow();
    const int nextRow = currentRowNumber + factor;

    return nextRow >= _proxyModel->rowCount() ? 0 : nextRow;
  }

  int QuickOpenView::previousIndex(int factor) const {
    const int currentRowNumber = _fileView->currentRow();
    const int nextRow = currentRowNumber + factor;

    return nextRow < 0 ? _proxyModel->rowCount() - 1 : nextRow;
  }

  void QuickOpenView::emitSelected(const QModelIndex&) {
    hide();

    const QUrl& url = _fileView->data(FileItem::Role::URL).toUrl();

    emit selected(url);
  }

  bool QuickOpenView::eventFilter(QObject* obj, QEvent* event) {
    if (isVisible()) {
      if (event->type() == QEvent::KeyPress) {
        auto keyEvent = static_cast<QKeyEvent*>(event);

        switch (keyEvent->key()) {
          case Qt::Key_Up:
            _fileView->setCurrentRow(previousIndex(-1));
            return true;

          case Qt::Key_PageUp:
            _fileView->scrollToTop();
            _fileView->setCurrentRow(previousIndex(-PAGE_DOWN_KEY_INDEX_FACTOR));
            return true;

          case Qt::Key_Down:
            _fileView->setCurrentRow(nextIndex(+1));
            return true;

          case Qt::Key_PageDown:
            _fileView->scrollToBottom();
            _fileView->setCurrentRow(nextIndex(+PAGE_DOWN_KEY_INDEX_FACTOR));
            return true;

          case Qt::Key_Enter:
          case Qt::Key_Return:
            emitSelected(QModelIndex());
            return true;
        }

        _input->setFocus();

        return _input->eventFilter(obj, event);
      }
    }

    return Layout::Overlay::eventFilter(obj, event);
  }
}  // namespace CodeLineX
