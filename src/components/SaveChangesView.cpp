/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/SaveChangesView.h"

#include <QLabel>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "components/FileList.h"
#include "models/FileListModel.h"

namespace CodeLineX {
  SaveChangesView::SaveChangesView(QWidget* parent): QDialog(parent) {
    setModal(true);
    setFixedSize(400, 288);
    setProperty("SaveChangesView", true);
    setWindowTitle(tr("Save Documents Changes"));

    _descriptionLabel = new QLabel(tr("The following documents has been modified:"), this);
    _filelistView = new FileList(this);
    _filelistView->setProperty("FileList", false);

    _saveAllButton = new QPushButton(tr("Save All"), this);
    _doNotSaveButton = new QPushButton(tr("Do Not Save"), this);
    _cancelButton = new QPushButton(tr("Cancel"), this);

    _mainLayout = new QVBoxLayout(this);
    _buttonsLayout = new QHBoxLayout;

    _mainLayout->addWidget(_descriptionLabel);
    _mainLayout->addWidget(_filelistView);
    _mainLayout->addLayout(_buttonsLayout);

    _buttonsLayout->addWidget(_saveAllButton);
    _buttonsLayout->addWidget(_doNotSaveButton);
    _buttonsLayout->addWidget(_cancelButton);

    connect(_saveAllButton, &QPushButton::pressed, this, &SaveChangesView::accept);
    connect(_doNotSaveButton, &QPushButton::pressed, this, &SaveChangesView::reject);
    connect(_cancelButton, &QPushButton::pressed, this, &SaveChangesView::reject);
  }

  SaveChangesView::~SaveChangesView() {
    delete _descriptionLabel;
    delete _filelistView;
    delete _saveAllButton;
    delete _doNotSaveButton;
    delete _cancelButton;
    delete _buttonsLayout;
    delete _mainLayout;
  }

  FileListModel* SaveChangesView::model() const {
    return qobject_cast<FileListModel*>(_filelistView->model());
  }

  void SaveChangesView::setModel(FileListModel* model) {
    _filelistView->setModel(model);
  }
}  // namespace CodeLineX
