/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/AbstractLinkedPanel.h"

#include <QIcon>
#include <QWidget>
#include <hpx-stacked-panel/StackedPanelFrame.h>
#include <hpx-stacked-panel/StackedPanelToolbox.h>

#include "components/PushButton.h"

namespace CodeLineX {
  AbstractLinkedPanel::AbstractLinkedPanel(QWidget* parent): HPX::StackedPanelFrame(parent) {
    static auto icon = QIcon::fromTheme(QStringLiteral("link"));

    _linkButton = new PushButton(this);

    _linkButton->setIcon(icon);
    _linkButton->setCheckable(true);
    _linkButton->setChecked(true);
    _linkButton->setToolTip(tr("Synchronize with Editor"));

    toolbox()->addButton(_linkButton);

    setCurrentState(State::None);

    connect(_linkButton, &PushButton::clicked,
            this, &AbstractLinkedPanel::onLinkButtonCheck);

    connect(this, &AbstractLinkedPanel::currentStateChanged,
            this, &AbstractLinkedPanel::onCurrentState);
  }

  bool AbstractLinkedPanel::isLinkRefEnabled() const {
    return _linkButton->isChecked();
  }

  void AbstractLinkedPanel::setLinkRefEnabked(bool enabled) {
    _linkButton->setChecked(enabled);
  }

  void AbstractLinkedPanel::onCurrentState(State state) {
    auto isEmpty = state == State::None || state == State::Empty;

    toolbox()->setDisabled(isEmpty);
  }
}  // namespace CodeLineX
