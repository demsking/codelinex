/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_PUSHBUTTON_H_
#define COMPONENTS_PUSHBUTTON_H_

#include <QPushButton>

class QString;
class QWidget;

namespace CodeLineX {
  class PushButton: public QPushButton {
    Q_OBJECT

    public:
      Q_PROPERTY(bool activate WRITE setActivated)

      explicit PushButton(const QString& label, QWidget* parent = nullptr);
      explicit PushButton(QWidget* parent = nullptr);
      ~PushButton() = default;

      void setActivated(bool activated);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_PUSHBUTTON_H_
