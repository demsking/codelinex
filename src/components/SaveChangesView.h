/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_SAVECHANGESVIEW_H_
#define COMPONENTS_SAVECHANGESVIEW_H_

#include <QDialog>

class QLabel;
class QWidget;
class QVBoxLayout;
class QHBoxLayout;
class QPushButton;

namespace CodeLineX {
  class FileList;
  class FileListModel;

  class SaveChangesView: public QDialog {
    Q_OBJECT

    QLabel* _descriptionLabel = nullptr;

    FileList* _filelistView = nullptr;

    QPushButton* _saveAllButton = nullptr;
    QPushButton* _doNotSaveButton = nullptr;
    QPushButton* _cancelButton = nullptr;

    QVBoxLayout* _mainLayout = nullptr;
    QHBoxLayout* _buttonsLayout = nullptr;

    public:
      explicit SaveChangesView(QWidget* parent);
      ~SaveChangesView() override;

    FileListModel* model() const;
    void setModel(FileListModel* model);

    signals:
      void selected(const QUrl&);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_SAVECHANGESVIEW_H_
