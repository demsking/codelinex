/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/ExplorerWidget.h"

#include <QUrl>
#include <QVBoxLayout>
#include <QFileSystemModel>

#include <KConfigGroup>

#include "core/Menu.h"
#include "components/Document.h"
#include "components/TreeView.h"
#include "models/FileIconProvider.h"
#include "utils/FileSystemUtils.h"

static const QString CONFIG_KEY_PATH = QStringLiteral("Path");
static const QString CONFIG_KEY_EXPANDED_PATHS = QStringLiteral("Expanded Paths");
static const QString CONFIG_KEY_CURRENT_SELECTION = QStringLiteral("Current Selection");
static const QString CONFIG_KEY_TREE_POSITION = QStringLiteral("Tree Position");

namespace CodeLineX {
  ExplorerWidget::ExplorerWidget(QWidget* parent): AbstractLinkedPanel(parent), Core::StateSession(metaObject()) {
    setProperty("ExplorerWidget", true);

    _iconProvider = new FileIconProvider();
    _treeView = new TreeView(this);

    _treeView->setIndentation(8);
    _treeView->setFocusPolicy(Qt::NoFocus);
    _treeView->setContextMenuPolicy(Qt::CustomContextMenu);

    setStateWidget(State::Ready, _treeView);
    setCurrentState(State::Loading);

    connect(_treeView, &TreeView::activated, this, &ExplorerWidget::setCurrentIndex);

    connect(_treeView, &TreeView::customContextMenuRequested,
            this, &ExplorerWidget::showContextFileMenu);
  }

  ExplorerWidget::~ExplorerWidget() {
    delete _iconProvider;
  }

  QString ExplorerWidget::dirname() const {
    return _dirname;
  }

  QDir ExplorerWidget::rootDirectory() const {
    return _model->rootDirectory();
  }

  QString ExplorerWidget::rootPath() const {
    return _model->rootPath();
  }

  void ExplorerWidget::setModel(const QSharedPointer<QFileSystemModel>& model) {
    _model = model;

    _model->setFilter(QDir::AllEntries | QDir::Files | QDir::NoDotAndDotDot | QDir::Hidden);
    _model->setIconProvider(_iconProvider);
    _treeView->setModel(_model.data());

    _treeView->setColumnHidden(1, true);
    _treeView->setColumnHidden(2, true);
    _treeView->setColumnHidden(3, true);
  }

  void ExplorerWidget::setCurrentFileUrl(const QUrl& url) {
    if (isActive() && isLinkRefEnabled()) {
      auto index = _model->index(url.toLocalFile());

      _treeView->setCurrentIndex(index);
    }

    _currentFileUrl = url;
  }

  void ExplorerWidget::onLinkButtonCheck(bool checked) {
    if (checked && _currentFileUrl.isValid()) {
      setCurrentFileUrl(_currentFileUrl);
    }
  }

  void ExplorerWidget::setRootPath(const QString& path) {
    if (!_model->rootPath().isEmpty() && path != _model->rootPath()) {
      resetObjectId();
    }

    _dirname = QDir(path).dirName();

    _model->setRootPath(path);
    _treeView->setRootIndex(_model->index(path));

    setCurrentState(State::Ready);

    emit rootPathChanged(path);
    emit rootDirectoryChanged(path);
  }

  void ExplorerWidget::setCurrentIndex(const QModelIndex& index) {
    auto filename = _model->filePath(index);

    if (_model->fileInfo(index).isFile()) {
      emit currentFileChanged(QUrl::fromLocalFile(filename));
    } else {
      _loadedPaths.insert(filename);

      if (_treeView->isExpanded(index)) {
        _treeView->collapse(index);
      } else {
        _treeView->expand(index);
      }
    }
  }

  void ExplorerWidget::showContextFileMenu(const QPoint& pos) {
    auto index = _treeView->indexAt(pos);

    if (index.isValid()) {
      auto filename = _model->filePath(index);

      if (_model->fileInfo(index).isFile()) {
        QAction* action = nullptr;
        Menu* menu = new Menu(this);
        QUrl url(filename);

        // open file
        action = new QAction(tr("Open File"), this);

        connect(action, &QAction::triggered, [this, &filename]() {
          emit currentFileChanged(QUrl::fromLocalFile(filename));
        });

        menu->addAction(action);

        // separator
        menu->addSeparator();

        // Open containing file
        QIcon icon(QIcon::fromTheme(QStringLiteral("document-open-folder")));
        action = new QAction(icon, tr("Open Containing Folder"), this);

        connect(action, &QAction::triggered, [&url]() {
          FileSystemUtils::openInFileManager(url);
        });

        menu->addAction(action);

        // Show File Properties
        action = new QAction(tr("Show File Properties"), this);

        connect(action, &QAction::triggered, [&url]() {
          FileSystemUtils::showPropertiesDialog(url);
        });

        menu->addAction(action);

        // show file menu
        menu->setAttribute(Qt::WA_DeleteOnClose);
        menu->exec(mapToGlobal(pos));
      }
    }
  }

  QStringList ExplorerWidget::expandedPaths() const {
    QStringList paths;

    foreach(auto path, _loadedPaths) {
      if (_treeView->isExpanded(_model->index(path))) {
        paths.push_back(path);
      }
    }

    return paths;
  }

  bool ExplorerWidget::_readStateConfig(const KConfigGroup& config) {
    auto rootPath = config.readEntry(CONFIG_KEY_PATH, QDir::homePath());
    auto paths = config.readEntry(CONFIG_KEY_EXPANDED_PATHS, QStringList());
    auto defaultCurrentIndex = _model->filePath(_treeView->currentIndex());
    auto currentSelection = config.readEntry(CONFIG_KEY_CURRENT_SELECTION, defaultCurrentIndex);
    auto treePosition = config.readEntry(CONFIG_KEY_TREE_POSITION, 0);
    auto selectedIndex = _model->index(currentSelection);

    setRootPath(rootPath);

    foreach(auto path, paths) {
      _loadedPaths.insert(path);
      _treeView->expand(_model->index(path));
    }

    _treeView->scrollTo(selectedIndex);
    _treeView->setCurrentIndex(selectedIndex);
    _treeView->setTreePosition(treePosition);

    return true;
  }

  bool ExplorerWidget::_writeStateConfig(KConfigGroup& config) {
    config.writeEntry(CONFIG_KEY_PATH, rootPath());
    config.writeEntry(CONFIG_KEY_EXPANDED_PATHS, expandedPaths());
    config.writeEntry(CONFIG_KEY_CURRENT_SELECTION, _model->filePath(_treeView->currentIndex()));
    config.writeEntry(CONFIG_KEY_TREE_POSITION, _treeView->treePosition());

    // TODO(demsking) CONFIG_KEY_TREE_POSITION does not work

    return true;
  }
}  // namespace CodeLineX
