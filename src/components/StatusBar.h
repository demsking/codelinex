/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_STATUSBAR_H_
#define COMPONENTS_STATUSBAR_H_

#include <QDir>
#include <QFrame>

#include "core/StateSession.h"
#include "components/Document.h"

class QLabel;
class QStatusBar;
class QHBoxLayout;
class KConfigGroup;

namespace CodeLineX {
  class Document;

  class StatusBar: public QFrame, public Core::StateSession {
    Q_OBJECT

    QHBoxLayout* _layout = nullptr;

    QLabel* _filePathLabel = nullptr;
    QLabel* _fileCursorPositionLabel = nullptr;
    QLabel* _editorModeLabel = nullptr;
    QLabel* _fileEndingLineLabel = nullptr;
    QLabel* _fileEncodingLabel = nullptr;
    QLabel* _fileTypeLabel = nullptr;

    QDir _currentProjectDir;
    Document* _currentDocument = nullptr;

    public:
      explicit StatusBar(QWidget* parent = nullptr);
      ~StatusBar() override;

    signals:
      void triggerCopyFilePathContextMenu(const QPoint&, const QUrl&);

    protected:
      inline void setFileCursorPosition(const Document::Cursor&);
      inline void setViewMode(Document*);
      inline void setFileEncodingLabel(Document*);

    public slots:
      void setCurrentProjectDir(const QDir&);
      void onNewDocumentAdded(Document*);
      void setCurrentDocument(Document*);
      void requestContextFileMenu(const QPoint&);

    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_STATUSBAR_H_
