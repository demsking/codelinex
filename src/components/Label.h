/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_LABEL_H_
#define COMPONENTS_LABEL_H_

#include <QLabel>

class QString;
class QWidget;

namespace CodeLineX {
  class Label: public QLabel {
    Q_OBJECT

    public:
      explicit Label(const QString& label, QWidget* parent = nullptr);
      explicit Label(QWidget* parent = nullptr);
      ~Label() = default;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_LABEL_H_
