/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_EDITORTABVIEW_H_
#define COMPONENTS_EDITORTABVIEW_H_

#include <QUrl>
#include <hpx-tabview/TabviewItem.h>

#include "core/StateSession.h"
#include "layouts/EditorLayoutWidget.h"
#include "components/Document.h"

class QWidget;
class QKeyEvent;
class QVBoxLayout;
class KConfigGroup;

namespace HPX {
  class Tabview;
  class TabviewModel;
}

namespace CodeLineX {
  class Document;
  class UnloadDocument;
  class FindReplaceFileWidget;

  class EditorTabview: public EditorLayoutWidget, public Core::StateSession {
    Q_OBJECT

    HPX::Tabview* _tabview = nullptr;
    HPX::TabviewModel* _model = nullptr;
    QVBoxLayout* _layout = nullptr;

    FindReplaceFileWidget* _findReplaceWidget = nullptr;

    // (oid -> UnloadDocument)
    QMap<QString, UnloadDocument*> _unloadDocuments;

    typedef Document::SearchOptions SearchOptions;

    public:
      enum TabviewItemRole {
        UrlRole = HPX::TabviewItem::Role::WidgetRole + 128
      };

    public:
      explicit EditorTabview(QWidget* parent = nullptr);
      explicit EditorTabview(const Core::Object&, QWidget* parent = nullptr);

      QList<Document*> documents() const;

      int currentIndex() const;

      HPX::TabviewWidget* widget(int index);
      HPX::TabviewWidget* widget(const QString& oid);

      Document* document(int index) const;
      Document* document(const QString& oid);
      Document* currentDocument() const;
      HPX::TabviewWidget* currentWidget() const;
      void setCurrentWidget(HPX::TabviewWidget*);

      int count() const;
      int indexOf(const QUrl&) const;
      int indexOf(HPX::TabviewWidget*) const;

      bool isEmpty() const override;
      void setFocus();

      HPX::TabviewModel* model() const;

    public slots:
      void showFindView();
      void showFindReplaceView();

    protected:
      inline void createFindView();

    protected slots:
      void onFindNext(const QString& pattern, const SearchOptions, bool followCursor);
      void onFindPrevious(const QString& pattern, const SearchOptions);
      void onReplace(const QString& pattern,
                     const SearchOptions,
                     const QString& replaceText,
                     bool replaceAll);

    signals:
      void focused();
      void closed();
      void tabviewItemCreated(HPX::TabviewItem*);
      void unloadDocumentCreated(UnloadDocument*);
      void documentCreated(Document*);
      void triggerFileContextMenuRequest(const QPoint&, const QUrl&);

    protected:
      void connectDocument(Document*, HPX::TabviewItem*);
      void onDocumentClose(Document*);

    public slots:
      void untitleDocument();
      Document* openDocument(const QUrl&);

    protected slots:
      Document* openDocument(Document*);

    public slots:
      void close() override;
      void polish() const;

    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;

      inline static QString documentNameKey(const QString&);
      inline static QString documentUrlKey(const QString&);

      bool writeChildrenState(KConfigGroup&) const;
      void restoreChildrenState(const KConfigGroup&);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_EDITORTABVIEW_H_
