/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/SwitchFileView.h"

#include <QWidget>
#include <QKeyEvent>
#include <QApplication>

#include "components/FileList.h"
#include "models/FileItem.h"
#include "models/FileListModel.h"

namespace CodeLineX {
  SwitchFileView::SwitchFileView(QWidget* parent): Overlay(parent) {
    _fileView = new FileList(parent);
    _factor = 0;

    addWidget(_fileView);
    setProperty("SwitchFilesView", true);
  }

  SwitchFileView::~SwitchFileView() {
    delete _fileView;
  }

  void SwitchFileView::setModel(FileListModel* model) {
    _fileView->setModel(model);
  }

  void SwitchFileView::setCurrentIndex(int index) {
    _fileView->setCurrentRow(nextRowNumber(index));
  }

  void SwitchFileView::show() {
    emit opening();

    if (_fileView->model()->rowCount() < 2) {
      return;
    }

    setCurrentIndex(0);
    _fileView->scroll(0, 0);

    Overlay::show();
  }

  void SwitchFileView::next() {
    const int currentIndex = _fileView->currentRow();
    const int nextCurrentRow = nextRowNumber(currentIndex);

    _fileView->setCurrentRow(nextCurrentRow);
  }

  int SwitchFileView::nextRowNumber(int currentRowNumber) const {
    const int nextRow = currentRowNumber + _factor;

    return _factor < 0
      ? nextRow < 0
        ? _fileView->model()->rowCount() - 1
        : nextRow
      : nextRow >= _fileView->model()->rowCount()
        ? 0
        : nextRow;
  }

  void SwitchFileView::emitSelected() {
    const QString oid = _fileView->data(FileItem::Role::OID).toString();

    emit selected(oid);
  }

  bool SwitchFileView::eventFilter(QObject* obj, QEvent* event) {
    static int ctrlKey = 0;

    if (event->type() == QEvent::KeyPress) {
      auto keyEvent = static_cast<QKeyEvent*>(event);

      if (keyEvent->modifiers() & Qt::ControlModifier) {
        if (keyEvent->modifiers() & Qt::ShiftModifier) {
          if (keyEvent->key() == Qt::Key_Backtab) {
            _factor = -1;

            if (!isVisible()) {
              show();
            } else {
              next();
            }

            return true;
          }
        } else if (keyEvent->key() == Qt::Key_Tab) {
          _factor = 1;

          if (!isVisible()) {
            show();
          } else {
            next();
          }

          return true;
        } else if (ctrlKey == 0) {
          ctrlKey = keyEvent->key();

          return true;
        }
      }
    } else if (event->type() == QEvent::KeyRelease) {
      if (isVisible()) {
        auto keyEvent = static_cast<QKeyEvent*>(event);

        if (keyEvent->key() == ctrlKey) {
          emitSelected();
          hide();

          return true;
        }
      }
    }

    return Layout::Overlay::eventFilter(obj, event);
  }
}  // namespace CodeLineX
