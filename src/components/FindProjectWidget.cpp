/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components/FindProjectWidget.h"

#include <QWidget>
#include <QHBoxLayout>

#include "components/Label.h"
#include "components/LineEdit.h"
#include "components/PushButton.h"

namespace CodeLineX {
  FindProjectWidget::FindProjectWidget(QWidget* parent): FindWidget(parent) {
    setProperty("FindProjectWidget", true);

    _titleLabel->setText(tr("Find in Project"));
    _queryLineEdit->setPlaceholderText(tr("Fin in Project"));

    _findAllButton = new PushButton(tr("Find All"), this);
    _queryInputLayout->addWidget(_findAllButton);

    _replaceInputLayout = new QHBoxLayout();
    _replaceLineEdit = new LineEdit(this);
    _replaceAllButton = new PushButton(tr("Replace All"), this);
    _replaceInputLayout->addWidget(_replaceLineEdit);
    _replaceInputLayout->addWidget(_replaceAllButton);

    _mainLayout->addLayout(_replaceInputLayout);
  }

  FindProjectWidget::~FindProjectWidget() {
    delete _findAllButton;
    delete _replaceLineEdit;
    delete _replaceAllButton;
    delete _replaceInputLayout;
  }
}  // namespace CodeLineX
