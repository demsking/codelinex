/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_QUICKOPENVIEW_H_
#define COMPONENTS_QUICKOPENVIEW_H_

#include <QDir>

#include "layouts/Overlay.h"

class QLabel;
class QWidget;
class QLineEdit;
class QSortFilterProxyModel;

namespace CodeLineX {
  class FileList;
  class FileListModel;
  class FileListProxyModel;

  class QuickOpenView: public Layout::Overlay {
    Q_OBJECT

    QLineEdit* _input = nullptr;
    FileList* _fileView = nullptr;
    QLabel* _emptyLabel = nullptr;

    FileListModel* _fileListModel = nullptr;
    FileListProxyModel* _proxyModel = nullptr;

    QDir _baseDir;

    public:
      explicit QuickOpenView(QWidget* parent);
      ~QuickOpenView() override;

      const QDir& rootDir() const;
      void setRootDir(const QDir&);

      void show() override;

    signals:
      void selected(const QUrl&);

    protected:
      inline int nextIndex(int factor) const;
      inline int previousIndex(int factor) const;
      inline void emitSelected(const QModelIndex&);

      bool eventFilter(QObject* obj, QEvent* event) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_QUICKOPENVIEW_H_
