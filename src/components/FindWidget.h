/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_FINDWIDGET_H_
#define COMPONENTS_FINDWIDGET_H_

#include <QFrame>

class QEvent;
class QString;
class QObject;
class QWidget;
class QPushButton;
class QVBoxLayout;
class QHBoxLayout;

namespace CodeLineX {
  class Label;
  class LineEdit;

  enum FindOption {
    PlainText,
    WholeWords,
    EscapeSequences,
    RegularExpression
  };

  class FindWidget: public QFrame {
    Q_OBJECT

    protected:
      Label* _titleLabel = nullptr;
      Label* _optionsDescriptionLabel = nullptr;
      QVBoxLayout* _mainLayout = nullptr;
      QHBoxLayout* _headerLayout = nullptr;
      QHBoxLayout* _headerOptionsLayout = nullptr;
      QPushButton* _closeButton = nullptr;
      QVBoxLayout* _bodyLayout = nullptr;
      QHBoxLayout* _queryInputLayout = nullptr;
      LineEdit* _queryLineEdit = nullptr;

    protected:
      explicit FindWidget(QWidget* parent);
      ~FindWidget() override;

    public slots:
      void hide();

    signals:
      void query(const QString&);
      void hidden();

    protected:
      bool eventFilter(QObject*, QEvent*) override;
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_FINDWIDGET_H_
