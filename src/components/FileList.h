/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_FILELIST_H_
#define COMPONENTS_FILELIST_H_

#include "components/ListView.h"
#include "models/FileItem.h"

class QWidget;
class QVariant;
class QStandardItemModel;

namespace CodeLineX {
  class FileList: public ListView {
    Q_OBJECT

    public:
      explicit FileList(QWidget* parent = nullptr);
      ~FileList() override = default;

      QStandardItemModel* model() const;
      QVariant data(FileItem::Role) const;

      int currentRow() const;
      void setCurrentRow(const int&);

    signals:
      void currentIndexChanged(FileList*, int);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_FILELIST_H_
