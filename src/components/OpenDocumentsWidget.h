/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2019  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_OPENDOCUMENTSWIDGET_H_
#define COMPONENTS_OPENDOCUMENTSWIDGET_H_

#include <hpx-stacked-panel/StackedPanelFrame.h>

namespace CodeLineX {
  class ListView;
  class OpenDocumentListModel;
  class OpenDocumentStyledItemDelegate;

  class OpenDocumentsWidget: public HPX::StackedPanelFrame {
    Q_OBJECT

    ListView* _listView = nullptr;
    OpenDocumentStyledItemDelegate* _itemDelegate = nullptr;
    QSharedPointer<OpenDocumentListModel> _documentListModel;

    public:
      explicit OpenDocumentsWidget(QWidget* parent = nullptr);

      void setModel(const QSharedPointer<OpenDocumentListModel>&);

    signals:
      void currentDocumentIdChanged(const QString& oid);
      void triggerFileContextMenuRequest(const QPoint&, const QUrl&);
      void triggerFileCloseRequest(const QString& oid);

    public slots:
      void setCurrentDocumentId(const QString&);

    protected slots:
      void requestContextFileMenu(const QPoint&);
      void switchToDocument(const QModelIndex&);
      void closeDocument(const QModelIndex&);
  };
}  // namespace CodeLineX

#endif  // COMPONENTS_OPENDOCUMENTSWIDGET_H_
