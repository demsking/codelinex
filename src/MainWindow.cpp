/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/MainWindow.h"

#include <QDir>
#include <QFile>
#include <QThread>
#include <QMenuBar>
#include <QClipboard>
#include <QStatusBar>
#include <QApplication>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QFileSystemWatcher>
#include <QWindow>

#include <hpx-stacked-panel/StackedPanel.h>

#include "lib/LocalConfig.h"
#include "core/Menu.h"
#include "core/ActionCollection.h"
#include "components/ExplorerWidget.h"
#include "components/EditorTabview.h"
#include "components/QuickOpenView.h"
#include "components/SwitchFileView.h"
#include "components/SaveChangesView.h"
#include "layouts/EditorLayout.h"
#include "layouts/Shell.h"
#include "models/FileListModel.h"
#include "utils/FileSystemUtils.h"

static const QString CONFIG_GROUP_KTEXTEDITOR_RENDERER = QStringLiteral("KTextEditor Renderer");

static const QString CONFIG_KEY_APP_THEME = QStringLiteral("Theme");
static const QString CONFIG_KEY_APP_DISPLAY_MENU = QStringLiteral("Display Menu");
static const QString CONFIG_KEY_APP_WORKSPACE_DIR = QStringLiteral("Workspace");
static const QString CONFIG_KEY_APP_LAST_PROJECT = QStringLiteral("Last Project");
static const QString CONFIG_KEY_APP_SHOW_PATH_IN_TITLEBAR = QStringLiteral("Show Path in Titlebar");
static const QString CONFIG_KEY_APP_PROJECTS = QStringLiteral("Projects");

static const QString CONFIG_KEY_PROJECT_GEOMETRY = QStringLiteral("Geometry");

static const QString CONFIG_KEY_KTEXTEDITOR_SCHEMA = QStringLiteral("Schema");

static const QString CONFIG_PROJECT_DIR = QStringLiteral(".codeline");
static const QString CONFIG_PROJECT_GROUP_ROOT = QStringLiteral("Project");
static const QString CONFIG_PROJECT_KEY_SHELL_ID = QStringLiteral("Shell ID");

static const QString DEFAULT_KTEXTEDITOR_SCHEMA = QStringLiteral("Dark");

static const QString ACTION_PROJECT_OPEN_FOLDER = QStringLiteral("file-open-folder");
static const QString ACTION_PROJECT_RECENT_FOLDERS = QStringLiteral("recent-projects");
static const QString ACTION_PROJECT_SAVE_SETTINGS = QStringLiteral("save-project-settings");
static const QString ACTION_PROJECT_CLOSE_FOLDER = QStringLiteral("file-close-folder");

static const QString ACTION_FILE_NEW_FILE = QStringLiteral("file-new-file");
static const QString ACTION_FILE_OPEN_FILE = QStringLiteral("file-open-file");
static const QString ACTION_FILE_SAVE = QStringLiteral("file-save");
static const QString ACTION_FILE_SAVE_AS = QStringLiteral("file-save-as");
static const QString ACTION_FILE_SAVE_AS_ENCODING = QStringLiteral("file-save-as-encoding");
static const QString ACTION_FILE_RELOAD = QStringLiteral("file-reload");
static const QString ACTION_FILE_RELOAD_ALL = QStringLiteral("file-reload-all");
static const QString ACTION_FILE_EXPORT_HTML = QStringLiteral("file-export-html");
static const QString ACTION_FILE_PRINT = QStringLiteral("file-print");
static const QString ACTION_FILE_CLOSE = QStringLiteral("file-close");
static const QString ACTION_FILE_CLOSE_ALL = QStringLiteral("file-close-all");
static const QString ACTION_FILE_CLOSE_OTHER = QStringLiteral("file-close-other");
static const QString ACTION_FILE_EXIT = QStringLiteral("file-exit");

static const QString ACTION_VIEW_SPLIT = QStringLiteral("view-split");
static const QString ACTION_VIEW_SPLIT_PREVIOUS_VIEW = QStringLiteral("view-split-previous-view");
static const QString ACTION_VIEW_SPLIT_NEXT_VIEW = QStringLiteral("view-split-next-view");
static const QString ACTION_VIEW_SPLIT_HORIZONTAL = QStringLiteral("view-split-horizontal");
static const QString ACTION_VIEW_SPLIT_VERTICAL = QStringLiteral("view-split-vertical");
static const QString ACTION_VIEW_SPLIT_TOGGLE_ORIENTATION = QStringLiteral("view-split-toggle-orientation");
static const QString ACTION_VIEW_SPLIT_CLOSE_CURRENT_VIEW = QStringLiteral("view-split-close-current-view");
static const QString ACTION_VIEW_SPLIT_CLOSE_INACTIVE_VIEWS = QStringLiteral("view-split-close-inactive-views");
static const QString ACTION_VIEW_SPLIT_HIDE_INACTIVE_VIEWS = QStringLiteral("view-split-hide-inactive-views");
static const QString ACTION_VIEW_QUICK_OPEN = QStringLiteral("view-quick-open");
static const QString ACTION_VIEW_INCREASE_FONT_SIZE = QStringLiteral("view-increase-font-size");
static const QString ACTION_VIEW_DECREASE_FONT_SIZE = QStringLiteral("view-decrease-font-size");
static const QString ACTION_VIEW_RESET_FONT_SIZE = QStringLiteral("view-reset-font-size");
static const QString ACTION_VIEW_WORD_WRAP = QStringLiteral("view-word-wrap");
static const QString ACTION_VIEW_SHOW_NON_PRINTABLE_SPACES = QStringLiteral("view-show-non-printable-spaces");
static const QString ACTION_VIEW_FIND = QStringLiteral("edit-find");
static const QString ACTION_VIEW_FIND_REPLACE = QStringLiteral("edit-replace");

static const QString ACTION_HELP_VIEW_TERMS_OF_USE = QStringLiteral("help-view-terms-of-use");
static const QString ACTION_HELP_VIEW_LICENSE = QStringLiteral("help-view-license");
static const QString ACTION_HELP_VERSION = QStringLiteral("help-version");
static const QString ACTION_HELP_DOCUMENTATION = QStringLiteral("help-documentation");
static const QString ACTION_HELP_FAQ = QStringLiteral("help-faq");
static const QString ACTION_HELP_DISCUSSION = QStringLiteral("help-discussion");
static const QString ACTION_HELP_REPORT_ISSUE = QStringLiteral("help-report-issue");
static const QString ACTION_HELP_ABOUT = QStringLiteral("help-about");
static const QString ACTION_HELP_WELCOME_GUIDE = QStringLiteral("help-guide");

namespace CodeLineX {
  MainWindow::MainWindow()
    : QMainWindow(nullptr),
      Core::StateSession(APPLICATION_NAME, metaObject()) {
    setWindowTitle(APPLICATION_NAME);
    setGeometry(100, 100, 960, 650);

    _actionCollection = new Core::ActionCollection(this);

    _config = KSharedConfig::openConfig();

    _shell = new Layout::Shell(this);

    _quickOpenView = new QuickOpenView(_shell);
    _switchFileView = new SwitchFileView(_shell);

    _themeWatcher = new QFileSystemWatcher(this);

    _switchFileView->setModel(_shell->switchFileModel());

    setCentralWidget(_shell);

    connect(_quickOpenView, &QuickOpenView::opening, [this]() {
      if (_switchFileView->isVisible()) {
        _switchFileView->hide();
      }

      _quickOpenView->setRootDir(_shell->explorer()->rootDirectory());
    });

    connect(_quickOpenView, &QuickOpenView::closed, this, &MainWindow::focusCurrentDocument);

    connect(_quickOpenView, &QuickOpenView::selected, [this](const QUrl& url) {
      _shell->currentView()->openDocument(url);
    });

    connect(_switchFileView, &SwitchFileView::opening, _quickOpenView, &QuickOpenView::hide);
    connect(_switchFileView, &SwitchFileView::closed, this, &MainWindow::focusCurrentDocument);
    connect(_switchFileView, &SwitchFileView::selected, _shell, &Layout::Shell::setCurrentDocument);

    connect(_shell, &Layout::Shell::viewCreated, this, &MainWindow::onViewCreate);
    connect(_shell, &Layout::Shell::currentDocumentChanged, this, &MainWindow::setCurrentDocument);
    connect(_shell, &Layout::Shell::currentDocumentChanged, this, &MainWindow::updateActionsOnModification);
    connect(_themeWatcher, &QFileSystemWatcher::fileChanged, this, &MainWindow::loadTheme);

    connect(_shell, &Layout::Shell::triggerFileContextMenu, this, &MainWindow::showFileContextMenu);
    connect(_shell, &Layout::Shell::triggerCopyFilePathContextMenu, this, &MainWindow::showCopyFilePathContextMenu);

    restore();
  }

  MainWindow::~MainWindow() {
    _quickOpenView->deleteLater();
    _switchFileView->deleteLater();
    _shell->deleteLater();

    delete _actionCollection;
    delete _themeWatcher;
  }

  QString MainWindow::editorSchema() {
    return _config->group(CONFIG_GROUP_KTEXTEDITOR_RENDERER).readEntry(
      CONFIG_KEY_KTEXTEDITOR_SCHEMA,
      DEFAULT_KTEXTEDITOR_SCHEMA
    );
  }

  QList<Document*> MainWindow::modifiedDocuments() const {
    QList<Document*> documents;

    for (Document* document: _shell->documents()) {
      if (document->isModified()) {
        documents.push_back(document);
      }
    }

    return documents;
  }

  void MainWindow::focusCurrentDocument() {
    Document* document = _shell->currentDocument();

    if (document) {
      document->setFocus();
    }
  }

  void MainWindow::setTitle(const QString& title) {
    setWindowTitle(title + " - " + APPLICATION_NAME);
  }

  void MainWindow::setEditorSchema(const QString& schema) {
    loadSchema(schema);

    KConfigGroup group = _config->group(CONFIG_GROUP_KTEXTEDITOR_RENDERER);

    group.writeEntry(CONFIG_KEY_KTEXTEDITOR_SCHEMA, schema);
    group.sync();
  }

  void MainWindow::setCurrentDocument(Document* document) {
    if (_showPathInTitlebar) {
      QString relativePath = FileSystemUtils::relativePath(document->url(), QDir::home());

      if (document->url().isLocalFile()) {
        if (!relativePath.startsWith(QDir::rootPath())) {
          relativePath.prepend("~/");
        }
      }

      setTitle(relativePath);
    } else {
      setTitle(document->name());
    }
  }

  void MainWindow::applyDisplayMenu() {
    if (_menuBarDisplayed) {
      menuBar()->show();
    } else {
      menuBar()->hide();
    }
  }

  void MainWindow::openUrl(const QUrl& url) {
    if (url.isEmpty()) {
      return;
    }

    _shell->currentView()->openDocument(url);
  }

  void MainWindow::showCopyFilePathContextMenu(const QPoint& pos, const QUrl& url) {
    Menu* menu = new Menu(this);

    // copy file path actions
    setFileMenuCopyActions(menu, url);

    // show file menu
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->exec(pos);
  }

  void MainWindow::showFileContextMenu(const QPoint& pos, const QUrl& url) {
    Menu* menu = new Menu(this);
    QAction* action = nullptr;

    // copy file path actions
    setFileMenuCopyActions(menu, url);

    // separator
    menu->addSeparator();

    // file menu actions
    setFileMenuActions(menu, false);

    // separator
    menu->addSeparator();

    // Open containing file
    action = new QAction(QIcon::fromTheme(QStringLiteral("document-open-folder")), tr("Open Containing Folder"), this);

    connect(action, &QAction::triggered, [&url]() {
      FileSystemUtils::openInFileManager(url);
    });

    menu->addAction(action);

    // Show File Properties
    action = new QAction(tr("Show File Properties"), this);

    connect(action, &QAction::triggered, [&url]() {
      FileSystemUtils::showPropertiesDialog(url);
    });

    menu->addAction(action);

    // separator
    menu->addSeparator();

    // file close actions
    setFileMenuCloseActions(menu, false);

    // show file menu
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->exec(pos);
  }

  void MainWindow::updateActionsOnModification(bool modifier) {
    static const QStringList actions = {
      ACTION_FILE_SAVE
    };

    _actionCollection->setEnabledActions(actions, modifier);
  }

  void MainWindow::updateSplitActionsOnViewCountChange() {
    // Disable some split actions when there are only one view
    static const QStringList names = {
      ACTION_VIEW_SPLIT_PREVIOUS_VIEW,
      ACTION_VIEW_SPLIT_NEXT_VIEW,
      ACTION_VIEW_SPLIT_TOGGLE_ORIENTATION,
      ACTION_VIEW_SPLIT_CLOSE_INACTIVE_VIEWS,
      ACTION_VIEW_SPLIT_HIDE_INACTIVE_VIEWS
    };
    const bool disabled = _shell->views().count() == 1;

    _actionCollection->setDisabledActions(names, disabled);
  }

  void MainWindow::createActions() {
    createProjectActions();
    createFileActions();
    createEditActions();
    createViewActions();
    createToolsActions();
    createWindowActions();
    createSettingsActions();
    createHelpActions();
    createGlobalActions();
  }

  void MainWindow::createGlobalActions() {
    // Save App Setting
    QAction* action = new QAction(tr("Save Setting"), this);

    action->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_S);

    connect(action, &QAction::triggered, [this]() {
      saveCurrentSettings();
    });

    addAction(action);

    // Reload Theme
    action = new QAction(tr("Reload Theme"), this);

    action->setShortcut(Qt::SHIFT | Qt::Key_F5);

    connect(action, &QAction::triggered, [this]() {
      loadTheme();
    });

    addAction(action);
  }

  QAction*MainWindow::actionMenu(MainWindow::ActionDescription& desc) {
    QAction* action = desc.icon
      ? new QAction(QIcon::fromTheme(desc.icon), tr(desc.name), this)
      : new QAction(tr(desc.name), this);

    if (desc.key != QKeySequence::UnknownKey) {
      action->setShortcut(desc.key);
    }

    connect(action, &QAction::triggered, [&, desc]() {
      auto viewAction = _shell->currentDocument()->action(desc.action);

      if (viewAction) {
        viewAction->trigger();
      }
    });

    return action;
  }

  void MainWindow::createActionsMenu(QMenu* menu, QList<ActionDescription> descriptions) {
    foreach (auto desc, descriptions) {
      if (desc.isSeparator) {
        menu->addSeparator();
      } else {
        auto action = actionMenu(desc);

        menu->addAction(action);
        addAction(action);
      }
    }
  }

  void MainWindow::createProjectActions() {
    QMenu* projectMenu = menuBar()->addMenu(tr("Project"));

    // Open folder
    QAction* action = _actionCollection->addAction(ACTION_PROJECT_OPEN_FOLDER);

    action->setIcon(QIcon::fromTheme("document-open-folder"));
    action->setText(tr("Open Project"));
    action->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_O);

    connect(action, &QAction::triggered, [this]() {
      QString dirname = QFileDialog::getExistingDirectory(this, "", _workspaceDir);

      if (dirname.isEmpty() || _shell->explorer()->rootPath() == dirname) {
        return;
      }

      saveCurrentProject();

      if (closeCurrentProject()) {
        openProject(dirname);
      }
    });

    projectMenu->addAction(action);
    addAction(action);

    // Recent Projects
    action = _actionCollection->addAction(ACTION_PROJECT_RECENT_FOLDERS);

    action->setText(tr("Recent Projects"));
    projectMenu->addAction(action);

    // separator
    projectMenu->addSeparator();

    // Save Project Settings
    action = _actionCollection->addAction(ACTION_PROJECT_SAVE_SETTINGS);

    action->setText(tr("Save Project Settings"));

    connect(action, &QAction::triggered, [this]() {
      saveCurrentProject();
    });

    projectMenu->addAction(action);

    // separator
    projectMenu->addSeparator();

    // Close folder
    action = _actionCollection->addAction(ACTION_PROJECT_CLOSE_FOLDER);

    action->setIcon(QIcon::fromTheme("document-close-folder"));
    action->setText(tr("Close Project"));

    connect(action, &QAction::triggered, [this]() {
      saveCurrentProject();
      closeCurrentProject();
    });

    projectMenu->addAction(action);

    connect(projectMenu, &QMenu::aboutToShow, [this]() {
      QAction* action = _actionCollection->action(ACTION_PROJECT_RECENT_FOLDERS);
      QMenu* recentProjectsMenu = new QMenu(this);

      action->setDisabled(_projects.isEmpty());
      action->setMenu(recentProjectsMenu);

      if (action->isVisible()) {
        for (QAction* itemAction: recentProjectsMenu->actions()) {
          recentProjectsMenu->removeAction(itemAction);

          delete itemAction;
        }

        for (const QString& path: _projects) {
          QAction* pathAction = recentProjectsMenu->addAction(path);

          connect(pathAction, &QAction::triggered, this, [this, path]() {
            if (path == _shell->explorer()->rootPath()) {
              return;
            }

            saveCurrentProject();

            if (closeCurrentProject()) {
              openProject(path);
            }
          });
        }
      }
    });
  }

  void MainWindow::createFileActions() {
    QMenu* fileMenu = menuBar()->addMenu(tr("File"));

    // New file
    QAction* action = _actionCollection->addAction(ACTION_FILE_NEW_FILE);

    action->setIcon(QIcon::fromTheme("document-new"));
    action->setText(tr("New File"));
    action->setShortcuts(QKeySequence::New);

    connect(action, &QAction::triggered, [this]() {
      _shell->currentView()->untitleDocument();
    });

    fileMenu->addAction(action);
    addAction(action);

    // Open file
    action = _actionCollection->addAction(ACTION_FILE_OPEN_FILE);

    action->setIcon(QIcon::fromTheme("document-open"));
    action->setText(tr("Open File"));
    action->setShortcuts(QKeySequence::Open);

    connect(action, &QAction::triggered, [this, action]() {
      const QList<QUrl> urls = QFileDialog::getOpenFileUrls(this, action->text(), _shell->currentDocument()->url());

      for (const QUrl& url: urls) {
        openUrl(url);
      }
    });

    fileMenu->addAction(action);
    addAction(action);

    // Recent files
    action = KStandardAction::openRecent(this, SLOT(openUrl(const QUrl&)), nullptr);

    fileMenu->addAction(action);

    // separator
    fileMenu->addSeparator();

    // file menu actions
    setFileMenuActions(fileMenu, true);

    // Reload All
    action = _actionCollection->addAction(ACTION_FILE_RELOAD_ALL);

    action->setText(tr("Reload All"));

    connect(action, &QAction::triggered, [this]() {
      for (Document* document: _shell->documents()) {
        document->reload();
      }
    });

    fileMenu->addAction(action);

    // separator
    fileMenu->addSeparator();

    // Export as HTML
    action = _actionCollection->addAction(ACTION_FILE_EXPORT_HTML);

    action->setIcon(QIcon::fromTheme("document-export"));

    connect(action, &QAction::triggered, [this]() {
      _shell->currentDocument()->action(Document::Action::EXPORT_HTML)->trigger();
    });

    fileMenu->addAction(action);

    // separator
    fileMenu->addSeparator();

    // Print
    action = _actionCollection->addAction(ACTION_FILE_PRINT);

    action->setIcon(QIcon::fromTheme("document-print"));
    action->setShortcuts(QKeySequence::Print);

    connect(action, &QAction::triggered, [this]() {
      _shell->currentDocument()->action(KStandardAction::Print)->trigger();
    });

    fileMenu->addAction(action);
    addAction(action);

    // separator
    fileMenu->addSeparator();

    // close file actions
    setFileMenuCloseActions(fileMenu);

    // Exit
    action = _actionCollection->addAction(ACTION_FILE_EXIT);

    action->setIcon(QIcon::fromTheme("application-exit"));
    action->setText(tr("Exit"));
    action->setShortcuts(QKeySequence::Quit);

    connect(action, &QAction::triggered, this, &MainWindow::close);
    fileMenu->addAction(action);
    addAction(action);

    connect(fileMenu, &QMenu::aboutToShow, [this]() {
      Document* document = _shell->currentDocument();
      QAction* action = nullptr;

      if (document) {
        const QString name = document->name();

        // Save as with Encoding
        Document::Action id = Document::Action::SAVE_AS_WITH_ENCODING;
        QMenu* saveAsEncodingMenu = document->action(id)->menu();
        action = _actionCollection->action(ACTION_FILE_SAVE_AS_ENCODING);
        action->setMenu(saveAsEncodingMenu);

        // Reload
        action = _actionCollection->action(ACTION_FILE_RELOAD);
        action->setText(tr("Reload '%1'").arg(name));

        // Export HTML
        action = _actionCollection->action(ACTION_FILE_EXPORT_HTML);
        action->setText(tr("Export '%1' as HTML").arg(name));

        // Print
        action = _actionCollection->action(ACTION_FILE_PRINT);
        action->setText(tr("Print '%1'").arg(name));

        // Close
        action = _actionCollection->action(ACTION_FILE_CLOSE);
        action->setText(tr("Close '%1'").arg(name));

        // Close Other
        action = _actionCollection->action(ACTION_FILE_CLOSE_OTHER);
        action->setText(tr("Close All Except '%1'").arg(name));
      } else {
        // Reload
        action = _actionCollection->action(ACTION_FILE_RELOAD);
        action->setText(tr("Reload"));

        // Export HTML
        action = _actionCollection->action(ACTION_FILE_EXPORT_HTML);
        action->setText(tr("Export HTML"));

        // Print
        action = _actionCollection->action(ACTION_FILE_PRINT);
        action->setText(tr("Print"));

        // Close
        action = _actionCollection->action(ACTION_FILE_CLOSE);
        action->setText(tr("Close File"));

        // Close Other
        action = _actionCollection->action(ACTION_FILE_CLOSE_OTHER);
        action->setText(tr("Close Other Documents"));
      }
    });
  }

  void MainWindow::createEditActions() {
    QMenu* editMenu = menuBar()->addMenu(tr("Edit"));

    createActionsMenu(editMenu, {
      { false, "edit-undo", "Undo", Document::StandardAction::Undo, QKeySequence::Undo },
      { false, "edit-redo", "Redo", Document::StandardAction::Redo, QKeySequence::Redo },
      { true, nullptr, nullptr, Document::StandardAction::ActionNone, QKeySequence::UnknownKey },
      { false, "edit-cut", "Cut", Document::StandardAction::Cut, QKeySequence::Cut },
      { false, "edit-copy", "Copy", Document::StandardAction::Copy, QKeySequence::Copy },
      { false, "edit-paste", "Paste", Document::StandardAction::Paste, QKeySequence::Paste },
      { true, nullptr, nullptr, Document::StandardAction::ActionNone, QKeySequence::UnknownKey },
      { false, "edit-select-all", "Select All", Document::StandardAction::SelectAll, QKeySequence::SelectAll },
      { false, nullptr, "Deselect", Document::StandardAction::Deselect, QKeySequence::UnknownKey },
      { true, nullptr, nullptr, Document::StandardAction::ActionNone, QKeySequence::UnknownKey }
    });

    // Find View
    QAction* action = _actionCollection->addAction(ACTION_VIEW_FIND);
    action->setIcon(QIcon::fromTheme("edit-find"));
    action->setText(tr("Find..."));
    action->setShortcut(Qt::CTRL | Qt::Key_F);
    connect(action, &QAction::triggered, [this]() {
      _shell->currentView()->showFindView();
    });
    editMenu->addAction(action);
    addAction(action);

    // Replace View
    action = _actionCollection->addAction(ACTION_VIEW_FIND_REPLACE);
    action->setIcon(QIcon::fromTheme("edit-find-replace"));
    action->setText(tr("Replace..."));
    action->setShortcut(Qt::CTRL | Qt::Key_R);
    connect(action, &QAction::triggered, [this]() {
      _shell->currentView()->showFindReplaceView();
    });
    editMenu->addAction(action);
    addAction(action);
  }

  void MainWindow::createViewActions() {
    QMenu* viewMenu = menuBar()->addMenu(tr("View"));

    // Quick Open View
    QAction* action = _actionCollection->addAction(ACTION_VIEW_QUICK_OPEN);
    action->setIcon(QIcon::fromTheme(ACTION_VIEW_QUICK_OPEN));
    action->setText(tr("Quick Open View"));
    action->setShortcut(Qt::Key_F1);
    viewMenu->addAction(action);
    addAction(action);

    connect(action, &QAction::triggered, [this, action]() {
      action->setDisabled(true);
      _quickOpenView->show();
    });

    connect(_quickOpenView, &QuickOpenView::closed, [action]() {
      action->setDisabled(false);
    });

    viewMenu->addSeparator();

    // Split View
    QMenu* splitViewMenu = new QMenu(this);

    action = _actionCollection->addAction(ACTION_VIEW_SPLIT);
    action->setText(tr("Split View"));
    action->setMenu(splitViewMenu);

    viewMenu->addAction(action);

    // Split Previous View
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_PREVIOUS_VIEW);
    action->setText(tr("Previous Split View"));
    action->setShortcut(Qt::SHIFT | Qt::Key_F8);

    connect(action, &QAction::triggered, [this]() {
      EditorTabview* currentView = _shell->currentView();
      const QList<EditorTabview*> views = _shell->views();
      const int index = views.indexOf(currentView) - 1;
      const int prevIndex = index < 0 ? views.count() - 1 : index;

      views[prevIndex]->setFocus();
    });

    addAction(action);
    splitViewMenu->addAction(action);

    // Split Next View
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_NEXT_VIEW);
    action->setText(tr("Next Split View"));
    action->setShortcut(Qt::Key_F8);

    connect(action, &QAction::triggered, [this]() {
      EditorTabview* currentView = _shell->currentView();
      const QList<EditorTabview*> views = _shell->views();
      const int index = views.indexOf(currentView) + 1;
      const int nextIndex = index == views.count() ? 0 : index;

      views[nextIndex]->setFocus();
    });

    addAction(action);
    splitViewMenu->addAction(action);
    splitViewMenu->addSeparator();

    // Split Horizontal
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_HORIZONTAL);
    action->setIcon(QIcon::fromTheme("view-split-left-right"));
    action->setText(tr("Split Horizontal"));
    action->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_L);

    connect(action, &QAction::triggered, [this]() {
      _shell->splitView(Qt::Horizontal);
    });

    splitViewMenu->addAction(action);
    addAction(action);

    // Split Vertical
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_VERTICAL);
    action->setIcon(QIcon::fromTheme("view-split-top-bottom"));
    action->setText(tr("Split Vertical"));
    action->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_T);

    connect(action, &QAction::triggered, [this]() {
      _shell->splitView(Qt::Vertical);
    });

    splitViewMenu->addAction(action);
    addAction(action);

    // Split: Toggle Orientation
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_TOGGLE_ORIENTATION);
    action->setText(tr("Toggle Orientation"));

    connect(action, &QAction::triggered, [this]() {
      EditorLayout* currentEditorLayout = _shell->currentEditorLayout();
      const Qt::Orientation orientation = currentEditorLayout->orientation() == Qt::Horizontal
        ? Qt::Vertical
        : Qt::Horizontal;

      currentEditorLayout->setOrientation(orientation);
    });

    splitViewMenu->addAction(action);
    splitViewMenu->addSeparator();

    // Split: Close Current View
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_CLOSE_CURRENT_VIEW);
    action->setText(tr("Close Current View"));

    connect(action, &QAction::triggered, [this]() {
      _shell->currentView()->close();
    });

    splitViewMenu->addAction(action);

    // Split: Close Inactive Views
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_CLOSE_INACTIVE_VIEWS);
    action->setText(tr("Close Inactive Views"));

    connect(action, &QAction::triggered, [this]() {
      EditorTabview* currentView = _shell->currentView();

      for (EditorTabview* view: _shell->views()) {
        if (view != currentView) {
          view->close();
        }
      }
    });

    splitViewMenu->addAction(action);
    splitViewMenu->addSeparator();

    // Split: Hide Inactive Views
    action = _actionCollection->addAction(ACTION_VIEW_SPLIT_HIDE_INACTIVE_VIEWS);
    action->setText(tr("Hide Inactive Views"));
    action->setCheckable(true);

    connect(action, &QAction::triggered, [this, action]() {
      EditorTabview* currentView = _shell->currentView();
      const bool checked = action->isChecked();

      for (EditorTabview* view: _shell->views()) {
        if (view != currentView) {
          view->setHidden(checked);
        }
      }
    });

    splitViewMenu->addAction(action);

    // separator
    viewMenu->addSeparator();

    // Increase Font Size
    action = _actionCollection->addAction(ACTION_VIEW_INCREASE_FONT_SIZE);
    action->setIcon(QIcon::fromTheme("font-size-up"));
    action->setText(tr("Increase Font Size"));
    action->setShortcuts(QKeySequence::ZoomIn);

    connect(action, &QAction::triggered, [this]() {
      _shell->currentDocument()->increaseFontSize();
    });

    viewMenu->addAction(action);
    addAction(action);

    // Decrease Font Size
    action = _actionCollection->addAction(ACTION_VIEW_DECREASE_FONT_SIZE);
    action->setIcon(QIcon::fromTheme("font-size-down"));
    action->setText(tr("Decrease Font Size"));
    action->setShortcuts(QKeySequence::ZoomOut);

    connect(action, &QAction::triggered, [this]() {
      _shell->currentDocument()->decreaseFontSize();
    });

    viewMenu->addAction(action);
    addAction(action);

    // Reset Font Size
    action = _actionCollection->addAction(ACTION_VIEW_RESET_FONT_SIZE);
    action->setText(tr("Reset Font Size"));
    action->setShortcut(Qt::CTRL | Qt::Key_Asterisk);

    connect(action, &QAction::triggered, [this]() {
      _shell->currentDocument()->resetFontSize();
    });

    viewMenu->addAction(action);
    addAction(action);

    viewMenu->addSeparator();

    // Word Wrap
    action = _actionCollection->addAction(ACTION_VIEW_WORD_WRAP);
    action->setIcon(QIcon::fromTheme(ACTION_VIEW_WORD_WRAP));
    action->setText(tr("Word Wrap"));
    action->setMenu(new QMenu);
    viewMenu->addAction(action);

    viewMenu->addSeparator();

    // Show Non-Printable Spaces
    action = _actionCollection->addAction(ACTION_VIEW_SHOW_NON_PRINTABLE_SPACES);
    action->setText(tr("Show Non-Printable Spaces"));
    action->setCheckable(true);

    connect(action, &QAction::triggered, [this, action]() {
      QAction* documentAction = _shell->currentDocument()->action(Document::Action::SHOW_NON_PRINTABLE_SPACES);

      documentAction->trigger();
      action->setChecked(documentAction->isChecked());
    });

    viewMenu->addAction(action);

    connect(viewMenu, &QMenu::aboutToShow, [this]() {
      Document* document = _shell->currentDocument();

      static const QStringList names = {
        ACTION_VIEW_INCREASE_FONT_SIZE,
        ACTION_VIEW_DECREASE_FONT_SIZE,
        ACTION_VIEW_RESET_FONT_SIZE,
        ACTION_VIEW_WORD_WRAP,
        ACTION_VIEW_SHOW_NON_PRINTABLE_SPACES,
      };

      if (document) {
        _actionCollection->setEnabledActions(names, true);

        // Word Wrap
        static const QList<Document::Action> wordWrapIds = {
          Document::Action::DYNAMIC_WORD_WRAP,
          Document::Action::DYNAMIC_WORD_WRAP_INDICATOR,
          Document::Action::DYNAMIC_WORD_WRAP_MARKER
        };

        QAction* action = _actionCollection->action(ACTION_VIEW_WORD_WRAP);
        action->menu()->clear();

        for (const Document::Action& id: wordWrapIds) {
          action->menu()->addAction(document->action(id));
        }
      } else {
        _actionCollection->setDisabledActions(names, true);
      }
    });
  }

  void MainWindow::createToolsActions() {
//    QMenu* toolsMenu = menuBar()->addMenu(tr("&Tools"));
  }

  void MainWindow::createWindowActions() {
    QMenu* windowMenu = menuBar()->addMenu(tr("Window"));

    // Full Screen mode
    QAction* action = new QAction(tr("Full Screen"), this);

    action->setCheckable(true);
    action->setChecked(!isWindow());
    action->setShortcut(QKeySequence::FullScreen);

    connect(action, &QAction::triggered, [this, action]() {
      toogleFullscreen();
      action->setChecked(isFullScreen());
    });

    windowMenu->addAction(action);
    addAction(action);

    // Show/Hide left sidebar
    action = new QAction(tr("Show Left Sidebar"), this);

    action->setCheckable(true);
    action->setChecked(_shell->leftSidebarVisibility());
    action->setShortcut(Qt::ALT | Qt::Key_0);

    connect(action, &QAction::triggered,
            _shell, &Layout::Shell::setLeftSidebarVisibility);

    windowMenu->addAction(action);
    addAction(action);

    // Show/Hide right sidebar
    action = new QAction(tr("Show Right Sidebar"), this);

    action->setCheckable(true);
    action->setChecked(_shell->rightSidebarVisibility());
    action->setShortcut(Qt::ALT | Qt::SHIFT | Qt::Key_0);

    connect(action, &QAction::triggered,
            _shell, &Layout::Shell::setRightSidebarVisibility);

    windowMenu->addAction(action);
    addAction(action);
  }

  void MainWindow::createSettingsActions() {
    QMenu* settingsMenu = menuBar()->addMenu(tr("Settings"));

    // Show/Hide menu action
    QAction* action = new QAction(tr("Show Menubar"), this);
    action->setCheckable(true);
    action->setChecked(_menuBarDisplayed);
    action->setShortcut(Qt::CTRL | Qt::Key_M);
    connect(action, &QAction::triggered, [this, action]() {
      _menuBarDisplayed = !_menuBarDisplayed;

      action->setChecked(_menuBarDisplayed);
      applyDisplayMenu();
    });
    settingsMenu->addAction(action);
    addAction(action);

    settingsMenu->addSeparator();

    // Show Path in Titlebar
    action = new QAction(tr("Show Path in Titlebar"), this);
    action->setCheckable(true);
    action->setChecked(_showPathInTitlebar);
    connect(action, &QAction::triggered, [this, action]() {
      _showPathInTitlebar = !_showPathInTitlebar;

      action->setChecked(_showPathInTitlebar);
      setCurrentDocument(_shell->currentDocument());
    });
    settingsMenu->addAction(action);
  }

  void MainWindow::createHelpActions() {
    QMenu* helpMenu = menuBar()->addMenu(tr("Help"));

    // View Terms of Use
    QAction* action = _actionCollection->addAction(ACTION_HELP_VIEW_TERMS_OF_USE);
    action->setText(tr("View Terms of Use"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    // View License
    action = _actionCollection->addAction(ACTION_HELP_VIEW_LICENSE);
    action->setText(tr("View License"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    // Version
    action = _actionCollection->addAction(ACTION_HELP_VERSION);
    action->setText(tr("Version") + " " + APP_VERSION);
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    helpMenu->addSeparator();

    // Documentation
    action = _actionCollection->addAction(ACTION_HELP_DOCUMENTATION);
    action->setText(tr("Documentation"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    // Frequently Asked Questions
    action = _actionCollection->addAction(ACTION_HELP_FAQ);
    action->setText(tr("Frequently Asked Questions"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    helpMenu->addSeparator();

    // Community Discussions
    action = _actionCollection->addAction(ACTION_HELP_DISCUSSION);
    action->setText(tr("Community Discussions"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    // Report Issue
    action = _actionCollection->addAction(ACTION_HELP_REPORT_ISSUE);
    action->setText(tr("Report Issue"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    helpMenu->addSeparator();

    // About
    action = _actionCollection->addAction(ACTION_HELP_ABOUT);
    action->setText(tr("About") + " " + APPLICATION_NAME);
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);

    // Welcome Guide
    action = _actionCollection->addAction(ACTION_HELP_WELCOME_GUIDE);
    action->setText(tr("Welcome Guide"));
    action->setDisabled(true);
    connect(action, &QAction::triggered, []() {
      //
    });
    helpMenu->addAction(action);
  }

  void MainWindow::setFileMenuActions(QMenu* menu, bool enableSaveEncoding, bool enableGlobalAction) {
    // Save file
    QAction* action = _actionCollection->addAction(ACTION_FILE_SAVE);

    action->setIcon(QIcon::fromTheme(QStringLiteral("document-save")));
    action->setText(tr("Save"));
    action->setShortcuts(QKeySequence::Save);

    connect(action, &QAction::triggered, [this](){
      _shell->currentDocument()->save();
      saveCurrentProject();
    });

    menu->addAction(action);

    if (enableGlobalAction) {
      addAction(action);
    }

    // Save file as
    action = _actionCollection->addAction(ACTION_FILE_SAVE_AS);

    action->setIcon(QIcon::fromTheme(QStringLiteral("document-save-as")));
    action->setText(tr("Save As..."));
    action->setShortcuts(QKeySequence::SaveAs);

    connect(action, &QAction::triggered, [this]() {
      QString fileName = QFileDialog::getSaveFileName(this);

      if (!fileName.isEmpty()) {
        _shell->currentDocument()->saveAs(fileName);
      }
    });

    menu->addAction(action);

    if (enableGlobalAction) {
      addAction(action);
    }

    // Save file as with encoding
    if (enableSaveEncoding) {
      action = _actionCollection->addAction(ACTION_FILE_SAVE_AS_ENCODING);

      action->setIcon(QIcon::fromTheme(QStringLiteral("document-save-as")));
      action->setText(tr("Save As with Encoding..."));
      menu->addAction(action);
    }

    // Save all files
    action = _actionCollection->addAction(tr("Save All"));

    action->setIcon(QIcon::fromTheme(("document-save-all")));
    action->setText(tr("Save All"));
    action->setShortcut(Qt::CTRL | Qt::Key_L);

    connect(action, &QAction::triggered, [this]() {
      for (Document* doc: _shell->documents()) {
        if (doc->path().isEmpty()) {
          continue;
        }
        doc->save();
      }
    });

    menu->addAction(action);

    if (enableGlobalAction) {
      addAction(action);
    }

    // separator
    menu->addSeparator();

    // Reload
    action = _actionCollection->addAction(ACTION_FILE_RELOAD);

    action->setIcon(QIcon::fromTheme(QStringLiteral("view-refresh")));
    action->setText(tr("Reload File"));
    action->setShortcuts(QKeySequence::Refresh);

    connect(action, &QAction::triggered, [this]() {
      Document* document = _shell->currentDocument();

      if (document) {
        document->reload();
      }
    });

    menu->addAction(action);

    if (enableGlobalAction) {
      addAction(action);
    }
  }

  void MainWindow::setFileMenuCopyActions(QMenu* menu, const QUrl& url) {
    // Copy full path
    QAction* action = new QAction(QIcon::fromTheme(QStringLiteral("edit-copy")), tr("Copy Full Path"), this);

    connect(action, &QAction::triggered, [&url]() {
      QGuiApplication::clipboard()->setText(url.toLocalFile());
    });

    menu->addAction(action);

    // Copy filename
    action = new QAction(tr("Copy File Name"), this);

    connect(action, &QAction::triggered, [&url]() {
      QGuiApplication::clipboard()->setText(url.fileName());
    });

    menu->addAction(action);
  }

  void MainWindow::setFileMenuCloseActions(QMenu* menu, bool enableGlobalAction) {
    // Close file
    QAction* action = _actionCollection->addAction(ACTION_FILE_CLOSE);

    action->setIcon(QIcon::fromTheme(QStringLiteral("document-close")));
    action->setText(tr("Close File"));
    action->setShortcuts(QKeySequence::Close);

    connect(action, &QAction::triggered, [this]() {
      Document* document = _shell->currentDocument();

      if (document) {
        document->close();
      }
    });

    menu->addAction(action);

    if (enableGlobalAction) {
      addAction(action);
    }

    // Close All
    action = _actionCollection->addAction(ACTION_FILE_CLOSE_ALL);

    action->setText(tr("Close All"));
    connect(action, &QAction::triggered, _shell, &Layout::Shell::closeDocuments);
    menu->addAction(action);

    // Close Other Documents
    action = _actionCollection->addAction(ACTION_FILE_CLOSE_OTHER);

    action->setText(tr("Close Other Files"));

    connect(action, &QAction::triggered, [this]() {
      Document* currentDocument = _shell->currentDocument();

      for (Document* document: _shell->documents()) {
        if (document != currentDocument) {
          document->close();
        }
      }
    });

    menu->addAction(action);
  }

  void MainWindow::closeEvent(QCloseEvent*) {
    saveCurrentSettings();
    closeCurrentProject();
  }

  void MainWindow::toogleFullscreen() {
    if (isFullScreen()) {
      showNormal();
    } else {
      // Caching the window geometry before to enter
      // to the fullscreen mode.
      // This is needed to in case the exit program is
      // calling while the fullscreen is active. In this case,
      // the saveCurrentProject() function will need to save
      // the window geometry of the normal mode.
      _windowGeometryFullscreenCache = geometry();

      showFullScreen();
    }
  }

  void MainWindow::loadTheme() {
    QFile stylesheet(_themeFile);
    stylesheet.open(QFile::ReadOnly);

    QString rules = QLatin1String(stylesheet.readAll());

    setStyleSheet(rules);
  }

  void MainWindow::loadSchema(const QString& schemaName) {
    auto schemaThemeFile = QDir::currentPath() + QDir::separator()
      + "schema" + QDir::separator() + schemaName + ".ini";
    auto schemaCodeFile = QDir::currentPath() + QDir::separator()
      + "schema" + QDir::separator() + schemaName + ".schema.ini";

    LocalConfig::mergeConfigs(
      KSharedConfig::openConfig("kateschemarc"),
      KSharedConfig::openConfig(schemaThemeFile)
    );

    LocalConfig::mergeConfigs(
      KSharedConfig::openConfig("katesyntaxhighlightingrc"),
      KSharedConfig::openConfig(schemaCodeFile)
    );
  }

  void MainWindow::onViewCreate(EditorTabview* view) {
    updateSplitActionsOnViewCountChange();

    connect(view, &EditorTabview::closed, this, &MainWindow::updateSplitActionsOnViewCountChange);

    connect(view, &EditorTabview::documentCreated, this, [this, view](Document* document) {
      connect(document, &Document::focusIn, this, [this, view]() {
        // Show inactive hidden views when one of them document is focused
        if (view->isHidden()) {
          QAction* action = _actionCollection->action(ACTION_VIEW_SPLIT_HIDE_INACTIVE_VIEWS);

          view->setVisible(true);
          action->trigger();
        }
      });

      connect(document, &Document::nameChanged, this, [this, document](const QString& name) {
        if (document == _shell->currentDocument()) {
          setTitle(name);
        }
      });

      connect(document, &Document::modifiedChanged, this, &MainWindow::updateActionsOnModification);

      connect(document, &Document::closed, this, [this, document]() {
        document->disconnect(this);
      });
    });
  }

  void MainWindow::saveCurrentSettings() {
    writeStateConfig(_config);
    _config->sync();
  }

  QString MainWindow::projectrc(QDir projectDir) const {
    projectDir.cd(CONFIG_PROJECT_DIR);

    return projectDir.absoluteFilePath("projectrc");
  }

  void MainWindow::openProject(const QString& path) {
    _shell->addProject(path);

    auto projectConfig = KSharedConfig::openConfig(projectrc(path));
    auto group = projectConfig->group(CONFIG_PROJECT_GROUP_ROOT);

    auto shellId = group.readEntry(CONFIG_PROJECT_KEY_SHELL_ID, "");
    auto projectGeometry = group.readEntry(CONFIG_KEY_PROJECT_GEOMETRY, geometry());

    setGeometry(projectGeometry);

    if (!shellId.isEmpty()) {
      Document* document = _shell->currentDocument();

      _shell->readStateConfig(shellId, group);

      if (_shell->currentDocument() != document) {
        if (document && document->url().isEmpty()) {
          document->close();
        }
      }
    } else {
      _shell->splitView(Qt::Horizontal);
    }

    _projects.removeOne(path);
    _projects.push_front(path);
  }

  void MainWindow::restore() {
    QThread* thread = new QThread();

    moveToThread(thread);

    connect(thread, &QThread::started, this, [this]() {
      readStateConfig(_config);
    });

    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();
  }

  void MainWindow::saveCurrentProject() {
    QDir projectDir = _shell->explorer()->rootDirectory();
    projectDir.mkdir(CONFIG_PROJECT_DIR);

    auto windowGeometry = isFullScreen() ? _windowGeometryFullscreenCache : geometry();
    auto projectConfig = KSharedConfig::openConfig(projectrc(projectDir));
    auto group = projectConfig->group(CONFIG_PROJECT_GROUP_ROOT);

    group.deleteGroup();
    group.writeEntry(CONFIG_PROJECT_KEY_SHELL_ID, _shell->objectId());
    group.writeEntry(CONFIG_KEY_PROJECT_GEOMETRY, windowGeometry);

    _shell->writeStateConfig(group);

    projectConfig->sync();
  }

  bool MainWindow::closeCurrentProject() {
    QList<Document*> documents = modifiedDocuments();

    if (!documents.isEmpty()) {
      FileListModel* model = new FileListModel(this);
      SaveChangesView* saveChangesView = new SaveChangesView(this);

      saveChangesView->setModel(model);

      for (Document* document: documents) {
        model->appendRow(document, _shell->explorer()->rootDirectory());
      }

      const int action = saveChangesView->exec();

      delete saveChangesView;
      delete model;

      if (action == QDialog::DialogCode::Rejected) {
        return false;
      }

      for (Document* document: documents) {
        document->save();
      }
    }

    _shell->explorer()->setRootPath(_workspaceDir);
    _shell->closeDocuments();

    return true;
  }

  bool MainWindow::_readStateConfig(const KConfigGroup& config) {
    // Load Application Config
    _themeFile = config.readEntry(CONFIG_KEY_APP_THEME, _themeFile);
    _menuBarDisplayed = config.readEntry(CONFIG_KEY_APP_DISPLAY_MENU, _menuBarDisplayed);
    _showPathInTitlebar = config.readEntry(CONFIG_KEY_APP_SHOW_PATH_IN_TITLEBAR, _showPathInTitlebar);
    _workspaceDir = config.readEntry(CONFIG_KEY_APP_WORKSPACE_DIR, _workspaceDir);
    _projects = config.readEntry(CONFIG_KEY_APP_PROJECTS, _projects);

    applyDisplayMenu();
    loadTheme();
    setEditorSchema(editorSchema());

    // Load Project Config
    auto lastProject = config.readEntry(CONFIG_KEY_APP_LAST_PROJECT, "");

    if (lastProject.isEmpty()) {
      _shell->addProject(_workspaceDir);
      _shell->splitView(Qt::Horizontal);
    } else {
      openProject(lastProject);
    }

    _themeWatcher->addPath(_themeFile);

    createActions();

    return true;
  }

  bool MainWindow::_writeStateConfig(KConfigGroup& group) {
    // Save Application Config
    group.writeEntry(CONFIG_KEY_APP_THEME, _themeFile);
    group.writeEntry(CONFIG_KEY_APP_WORKSPACE_DIR, _workspaceDir);
    group.writeEntry(CONFIG_KEY_APP_LAST_PROJECT, _shell->explorer()->rootPath());
    group.writeEntry(CONFIG_KEY_APP_DISPLAY_MENU, _menuBarDisplayed);
    group.writeEntry(CONFIG_KEY_APP_SHOW_PATH_IN_TITLEBAR, _showPathInTitlebar);
    group.writeEntry(CONFIG_KEY_APP_PROJECTS, _projects);
    group.writeEntry(CONFIG_KEY_APP_PROJECTS, _shell->leftSidebarVisibility());

    // Save Project Config
    saveCurrentProject();

    return true;
  }
}  // namespace CodeLineX
