/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_FILESYSTEMUTILS_H_
#define UTILS_FILESYSTEMUTILS_H_

#include <vector>
#include <string>

class QUrl;
class QDir;
class QString;
class QStringList;

namespace CodeLineX {
  class FileSystemUtils {
    private:
      FileSystemUtils();

    public:
      static QString relativePath(const QUrl& url, const QDir& base);
      static QStringList files(const QDir& base, const int& maxDepth = 40);
      static std::string join(const std::vector<std::string>&);

      static void openInFileManager(const QUrl&);
      static void showPropertiesDialog(const QUrl&);

    protected:
      static void deepFiles(const QDir& base,
                            QStringList& files,  // NOLINT(runtime/references)
                            int currentDepth,
                            const int& maxDepth);
  };
}  // namespace CodeLineX

#endif  // UTILS_FILESYSTEMUTILS_H_
