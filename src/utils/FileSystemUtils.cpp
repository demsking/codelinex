/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils/FileSystemUtils.h"

#include <QUrl>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QFileInfoList>

#include <KFileItem>
#include <KPropertiesDialog>
#include <KIO/OpenFileManagerWindowJob>

#include <iterator>
#include <iostream>
#include <vector>
#include <numeric>
#include <string>
#include <functional>

namespace CodeLineX {
  QString FileSystemUtils::relativePath(const QUrl& url, const QDir& base) {
    if (!url.isLocalFile()) {
      return url.toString();
    }

    auto path = url.toLocalFile();
    auto relativeFilePath = base.relativeFilePath(path);

    return relativeFilePath.startsWith("..") ? path : relativeFilePath;
  }

  QStringList FileSystemUtils::files(const QDir& base, const int& maxDepth) {
    QStringList files;

    FileSystemUtils::deepFiles(base, files, 0, maxDepth);

    return files;
  }

  std::string FileSystemUtils::join(const std::vector<std::string>& filenames) {
    auto separator = QString(QDir::separator().toLatin1()).toStdString();
    auto begin = filenames.begin();
    auto end = filenames.end();

    return std::accumulate(std::next(begin), end, filenames[0], [&separator](std::string a, std::string b) {
      return a + separator + b;
    });
  }

  void FileSystemUtils::openInFileManager(const QUrl& url) {
    KIO::highlightInFileManager({ url.toLocalFile() });
  }

  void FileSystemUtils::showPropertiesDialog(const QUrl& url) {
    KFileItem fileItem(url);
    QDialog* dialod = new KPropertiesDialog(fileItem);

    dialod->setAttribute(Qt::WA_DeleteOnClose);
    dialod->show();
  }

  void FileSystemUtils::deepFiles(const QDir& directory, QStringList& files, int currentDepth, const int& maxDepth) {
    if (currentDepth >= maxDepth) {
      return;
    }

    QFileInfoList list = directory.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files);

    foreach (auto fileInfo, list) {
      if (fileInfo.isDir()) {
        FileSystemUtils::deepFiles(fileInfo.absoluteFilePath(), files, currentDepth + 1, maxDepth);
      } else {
        files << fileInfo.absoluteFilePath();
      }
    }
  }
}  // namespace CodeLineX
