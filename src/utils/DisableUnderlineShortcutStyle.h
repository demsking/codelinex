/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_DISABLEUNDERLINESHORTCUTSTYLE_H_
#define UTILS_DISABLEUNDERLINESHORTCUTSTYLE_H_

#include <QProxyStyle>

class QWidget;
class QStyleOption;
class QStyleHintReturn;

namespace CodeLineX {
  class DisableUnderlineShortcutStyle: public QProxyStyle {
    public:
      DisableUnderlineShortcutStyle();

    public:
      int styleHint(StyleHint, const QStyleOption*, const QWidget*, QStyleHintReturn*) const override;
  };
}

#endif  // UTILS_DISABLEUNDERLINESHORTCUTSTYLE_H_
