#include "LSPSettings.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <KSharedConfig>

namespace CodeLineX {
  static const QString SERVERS = QStringLiteral("servers");
  static const QString SERVER_USE = QStringLiteral("use");
  static const QString SERVER_COMMAND = QStringLiteral("command");
  static const QString SERVER_COMMAND_DEBUG = QStringLiteral("commandDebug");
  static const QString SERVER_ROOT_INDICATION_FILES = QStringLiteral("rootIndicationFileNames");
  static const QString SERVER_INIT_OPTIONS = QStringLiteral("initializationOptions");
  static const QString SERVER_URL = QStringLiteral("url");

  inline QStringList jsonToList(const QJsonArray& array) {
    QStringList list;
    QJsonArray::const_iterator it;

    for (it = array.begin(); it != array.end(); ++it) {
      list << it->toString();
    }

    return list;
  }

  inline QJsonArray listToJson(const QStringList& list) {
    QJsonArray array;
    QStringList::const_iterator it;

    for (it = list.begin(); it != list.end(); ++it) {
      array.append(*it);
    }

    return array;
  }

  inline void parseServerSetting(const QJsonObject& setting, LSPSettings::ServerConfig& server) {
    server.url = setting[SERVER_URL].toString("");
    server.command = jsonToList(setting[SERVER_COMMAND].toArray({}));
    server.commandDebug = jsonToList(setting[SERVER_COMMAND_DEBUG].toArray({}));
    server.rootIndicationFileNames = jsonToList(setting[SERVER_ROOT_INDICATION_FILES].toArray({}));
    server.initializationOptions = setting[SERVER_INIT_OPTIONS].toObject();
  }

  LSPSettings::LSPSettings(const QString& configFile): _configFile(configFile) {
  }

  QMap<QString, LSPSettings::ServerConfig>& LSPSettings::servers() {
    return _serversList;
  }

  QJsonObject LSPSettings::loadConfigFile() {
    QFile file(_configFile);

    file.open(QIODevice::ReadOnly);

    Q_ASSERT(file.isOpen());

    return QJsonDocument::fromJson(file.readAll()).object();
  }

  bool LSPSettings::load()   {
    const QJsonObject config = loadConfigFile();
    QJsonObject servers = config[SERVERS].toObject();
    QJsonObject::iterator it = servers.begin();

    while (it != servers.end()) {
      const QString langId = it.key();
      const QJsonObject setting = it.value().toObject();
      ServerConfig server;

      server.langId = langId;

      if (setting[SERVER_USE].isUndefined()) {
        parseServerSetting(setting, server);
        _serversList.insert(langId, server);

        if (_aliasList.contains(langId)) {
          ServerConfig pendingServer = _aliasList.value(langId);

          parseServerSetting(setting, pendingServer);
          _serversList.insert(pendingServer.langId, pendingServer);
        }
      } else {
        const QString aliasLangId = setting[SERVER_USE].toString();

        if (_serversList.contains(aliasLangId)) {
          server = _serversList.value(aliasLangId);

          server.langId = langId;

          _serversList.insert(langId, server);
        }

        _aliasList.insert(aliasLangId, server);
      }

      ++it;
    }

    return true;
  }

  QJsonObject LSPSettings::toJson() {
    QJsonObject servers;

    foreach (QString key, _serversList.keys()) {
      const ServerConfig server = _serversList.value(key);
      QJsonObject setting;

      if (_aliasList.contains(key)) {
        setting[SERVER_USE] = server.langId;
      } else {
        setting[SERVER_URL] = server.url;
        setting[SERVER_COMMAND] = listToJson(server.command);
        setting[SERVER_COMMAND_DEBUG] = listToJson(server.commandDebug);
        setting[SERVER_ROOT_INDICATION_FILES] = listToJson(server.rootIndicationFileNames);
        setting[SERVER_INIT_OPTIONS] = server.initializationOptions;
      }

      servers.insert(key, setting);
    }

    QJsonObject config;

    config[SERVERS] = servers;

    return config;
  }

  bool LSPSettings::sync() {
    QFile file(_configFile);

    if (!file.open(QIODevice::WriteOnly)) {
      qWarning("Couldn't open save file.");
      return false;
    }

    QJsonObject config = toJson();
    QJsonDocument doc(config);

    file.write(doc.toJson());

    return true;
  }
}
