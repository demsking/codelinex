#ifndef LIB_LSPSETTINGS_H
#define LIB_LSPSETTINGS_H

#include <QMap>
#include <QObject>
#include <QJsonObject>

namespace CodeLineX {
  class LSPSettings: public QObject {
    public:
      struct ServerConfig {
        QString langId;
        QStringList command;
        QStringList commandDebug;
        QString url;
        QStringList rootIndicationFileNames;
        QJsonObject initializationOptions;
      };

    private:
      QString _configFile;
      QMap<QString, ServerConfig> _aliasList;
      QMap<QString, ServerConfig> _serversList;

    public:
      LSPSettings(const QString& configFile);

      QMap<QString, ServerConfig>& servers();

      bool load();
      bool sync();

    protected:
      QJsonObject toJson();
      QJsonObject loadConfigFile();
  };
}  // namespace CodeLineX

#endif // LIB_LSPSETTINGS_H_
