/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_EDITORLAYOUT_H_
#define LAYOUTS_EDITORLAYOUT_H_

#include <QSplitter>

#include "core/StateSession.h"

class QWidget;
class KConfigGroup;

namespace HPX {
  class TabviewModel;
}

namespace CodeLineX {
  class EditorTabview;
  class Document;
  class EditorLayoutWidget;

  class EditorLayout: public QSplitter, public Core::StateSession {
    Q_OBJECT

    EditorTabview* _currentView = nullptr;
    HPX::TabviewModel* _model = nullptr;

    public:
      explicit EditorLayout(QWidget* parent = nullptr);
      ~EditorLayout() override;

    protected:
      using QSplitter::addWidget;

    public:
      void addWidget(EditorLayoutWidget*);
      void addWidget(EditorLayout*);

      void splitView(Qt::Orientation);

      bool isEmpty() const;
      void close();

      EditorTabview* currentView();

    signals:
      void currentLayoutChanged(EditorLayout*);
      void viewCreated(EditorTabview*);
      void currentViewChanged(EditorTabview*);
      void documentAdded(Document*);
      void closed();

    protected slots:
      void onTabviewFocused(EditorTabview*);
      void onTabviewClosed(EditorTabview*);

    protected:
      void closingCheck();

    protected:
      void connectEditorLayout(EditorLayout*);
      void connectTabview(EditorTabview* sender, EditorLayout* reciever);

    protected:
      bool _readStateConfig(const KConfigGroup&) override;
      bool _writeStateConfig(KConfigGroup&) override;

      bool writeChildrenState(KConfigGroup&) const;
      void restoreChildrenState(const KConfigGroup&);
  };
}  // namespace CodeLineX

#endif  // LAYOUTS_EDITORLAYOUT_H_
