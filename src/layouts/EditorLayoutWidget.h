/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_EDITORLAYOUTWIDGET_H_
#define LAYOUTS_EDITORLAYOUTWIDGET_H_

#include <QWidget>

namespace CodeLineX {
  class EditorLayoutWidget: public QWidget {
    public:
      using QWidget::QWidget;

      virtual bool isEmpty() const = 0;
      virtual void close() = 0;
  };
}

#endif  // LAYOUTS_EDITORLAYOUTWIDGET_H_
