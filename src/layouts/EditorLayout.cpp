﻿/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "layouts/EditorLayout.h"

#include <QWidget>
#include <KConfigGroup>

#include <hpx-tabview/TabviewModel.h>

#include "layouts/EditorLayoutWidget.h"
#include "components/EditorTabview.h"
#include "components/Document.h"

#define CONFIG_KEY_STATE "State"
#define CONFIG_KEY_ORIENTATION "Orientation"
#define CONFIG_KEY_VIEWS "Views"

namespace CodeLineX {
  EditorLayout::EditorLayout(QWidget* parent): QSplitter(parent), Core::StateSession(metaObject()) {
    setContentsMargins(QMargins());
    setProperty("EditorLayout", true);
  }

  EditorLayout::~EditorLayout() {
    for (int i = 0; i < count(); i++) {
      delete widget(i);
    }
  }

  void EditorLayout::addWidget(EditorLayoutWidget* widget) {
    QSplitter::addWidget(widget);
  }

  void EditorLayout::addWidget(EditorLayout* widget) {
    QSplitter::addWidget(widget);
  }

  void EditorLayout::splitView(Qt::Orientation orientation) {
    auto newView = new EditorTabview(this);

    emit viewCreated(newView);

    if (this->orientation() == orientation) {
      addWidget(newView);
      connectTabview(newView, this);
    } else {
      auto currentIndex = indexOf(_currentView);
      auto index = currentIndex == -1 ? 0 : currentIndex;
      auto indexEditorLayout = new EditorLayout();
      auto indexView = replaceWidget(index, indexEditorLayout);

      if (indexView) {
        indexEditorLayout->addWidget(indexView);
        indexEditorLayout->addWidget(newView);
        indexEditorLayout->setOrientation(orientation);
      } else {
        indexEditorLayout->addWidget(newView);
      }

      connectEditorLayout(indexEditorLayout);
      connectTabview(newView, indexEditorLayout);
    }

    bool documentLoaded = false;

    if (_currentView) {
      Document* currentDocument = _currentView->currentDocument();

      if (currentDocument && !currentDocument->url().isEmpty()) {
        newView->openDocument(currentDocument->url());
        documentLoaded = true;
      }
    }

    if (!documentLoaded) {
      newView->untitleDocument();
    }

    newView->setFocus();
  }

  bool EditorLayout::isEmpty() const {
    for (auto i = 0; i < QSplitter::count(); i++) {
      if (widget(i)->isVisible()) {
        return false;
      }
    }

    return true;
  }

  void EditorLayout::close() {
    for (int i = 0; i < count(); i++) {
      QWidget* siblingWidget = widget(i);
      auto siblingLayout = qobject_cast<EditorLayout*>(siblingWidget);

      if (siblingLayout) {
        siblingLayout->close();
      } else {
        auto view = qobject_cast<EditorTabview*>(siblingWidget);

        view->close();
      }
    }

    hide();
    emit closed();
  }

  EditorTabview* EditorLayout::currentView() {
    return _currentView;
  }

  void EditorLayout::onTabviewFocused(EditorTabview* view) {
    _currentView = view;

    emit currentLayoutChanged(this);
    emit currentViewChanged(view);
  }

  void EditorLayout::onTabviewClosed(EditorTabview* view) {
    view->hide();

    if (view == _currentView) {
      _currentView = nullptr;
    }

    view->deleteLater();

    closingCheck();
  }

  void EditorLayout::closingCheck() {
    if (count() == 0) {
      hide();
      emit closed();
    } else {
      auto child = qobject_cast<EditorLayout*>(widget(0));

      if (child && child->count() == 0) {
        emit closed();
      }
    }
  }

  void EditorLayout::connectEditorLayout(EditorLayout* editorLayout) {
    connect(editorLayout, &EditorLayout::currentLayoutChanged, this, &EditorLayout::currentLayoutChanged);
    connect(editorLayout, &EditorLayout::viewCreated, this, &EditorLayout::viewCreated);
    connect(editorLayout, &EditorLayout::currentViewChanged, this, &EditorLayout::currentViewChanged);
    connect(editorLayout, &EditorLayout::documentAdded, this, &EditorLayout::documentAdded);
    connect(editorLayout, &EditorLayout::closed, this, [this, editorLayout]() {
      editorLayout->deleteLater();

      closingCheck();
    });
  }

  void EditorLayout::connectTabview(EditorTabview* view, EditorLayout* reciever) {
    connect(view, &EditorTabview::focused, reciever, [reciever, view]() {
      reciever->onTabviewFocused(view);
    });

    connect(view, &EditorTabview::closed, reciever, [reciever, view]() {
      reciever->onTabviewClosed(view);
    });

    connect(view, &EditorTabview::documentCreated, reciever, &EditorLayout::documentAdded);
  }

  bool EditorLayout::_readStateConfig(const KConfigGroup& config) {
    const QByteArray state = config.readEntry(CONFIG_KEY_STATE, QByteArray());
    const int defaultOrientation = orientation() == Qt::Horizontal ? 0 : 1;
    const int orientationInt = config.readEntry(CONFIG_KEY_ORIENTATION, defaultOrientation);
    const Qt::Orientation orientationValue = orientationInt == 0 ? Qt::Horizontal : Qt::Vertical;

    setOrientation(orientationValue);
    restoreChildrenState(config);
    restoreState(state);

    return true;
  }

  bool EditorLayout::_writeStateConfig(KConfigGroup& config) {
    if (writeChildrenState(config)) {
      config.writeEntry(CONFIG_KEY_STATE, saveState());
      config.writeEntry(CONFIG_KEY_ORIENTATION, orientation() == Qt::Horizontal ? 0 : 1);

      return true;
    }

    return false;
  }

  bool EditorLayout::writeChildrenState(KConfigGroup& config) const {
    QStringList children;

    for (int i = 0; i < count(); i++) {
      QWidget* childWidget = widget(i);

      if (childWidget) {
        auto editorLayoutItem = dynamic_cast<EditorLayoutWidget*>(childWidget);

        if ((editorLayoutItem && !editorLayoutItem->isEmpty()) || childWidget->isVisible()) {
          auto childStateSession = dynamic_cast<Core::StateSession*>(childWidget);

          if (childStateSession && childStateSession->writeStateConfig(config)) {
            children.push_back(childStateSession->objectId());
          }
        }
      }
    }

    if (children.isEmpty()) {
      return false;
    }

    config.writeEntry(CONFIG_KEY_VIEWS, children);

    return true;
  }

  void EditorLayout::restoreChildrenState(const KConfigGroup& config) {
    const QStringList views = config.readEntry(CONFIG_KEY_VIEWS, QStringList());

    for (const QString& oid: views) {
      if (Core::StateSession::getClassName(config.group(oid)) == className()) {
        EditorLayout* editorLayout = Object::get<EditorLayout>(oid, this);

        connectEditorLayout(editorLayout);
        addWidget(editorLayout);
        editorLayout->readStateConfig(oid, config);

        if (editorLayout->count() == 0) {
          editorLayout->close();
        }

        continue;
      }

      EditorTabview* view = Object::get<EditorTabview>(oid, this);

      emit viewCreated(view);

      addWidget(view);
      connectTabview(view, this);
      view->readStateConfig(oid, config);

      if (view->isEmpty()) {
        view->close();
      }
    }
  }
}  // namespace CodeLineX
