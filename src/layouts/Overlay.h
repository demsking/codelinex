/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_OVERLAY_H_
#define LAYOUTS_OVERLAY_H_

#include <QObject>

class QEvent;
class QObject;
class QWidget;
class KateFadeEffect;

namespace CodeLineX {
  namespace Layout {
    class OverlayFrame;

    class Overlay: public QObject {
      Q_OBJECT

      QWidget* _parent = nullptr;
      OverlayFrame* _frame = nullptr;
      KateFadeEffect* _fadeEffect = nullptr;

      bool _visible = false;

      public:
        explicit Overlay(QWidget* parent);
        ~Overlay() override;

        QWidget* parent() const;

        void addWidget(QWidget*);
        void addStretch(int stretch = 0);

        bool isVisible() const;

        void setAlignment(Qt::Alignment);
        bool setProperty(const char* name, const QVariant& value);

      signals:
        void opening();
        void opened();
        void closed();

      public slots:
        virtual void show();
        virtual void hide();
        void resize(const QSize&);

      protected:
        bool eventFilter(QObject* obj, QEvent* event) override;
    };
  }  // namespace Layout
}  // namespace CodeLineX

#endif  // LAYOUTS_OVERLAY_H_
