/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "layouts/Overlay.h"

#include <QWidget>
#include <QKeyEvent>
#include <QVBoxLayout>
#include <QApplication>

#include "effects/KateFadeEffect.h"
#include "layouts/OverlayFrame.h"

namespace CodeLineX {
  namespace Layout {
    Overlay::Overlay(QWidget* parent)
      : QObject(nullptr),
        _parent(parent),
        _frame(new OverlayFrame),
        _fadeEffect(new KateFadeEffect(_frame)) {
      QCoreApplication::instance()->installEventFilter(this);

      connect(_fadeEffect, &KateFadeEffect::showAnimationFinished, this, &Overlay::opened);
      connect(_fadeEffect, &KateFadeEffect::hideAnimationFinished, this, &Overlay::closed);
    }

    Overlay::~Overlay() {
      delete _frame;

      _fadeEffect->deleteLater();
    }

    QWidget* Overlay::parent() const {
      return _parent;
    }

    void Overlay::addWidget(QWidget* widget) {
      _frame->addWidget(widget);
    }

    void Overlay::addStretch(int stretch) {
      _frame->addStretch(stretch);
    }

    bool Overlay::isVisible() const {
      return _visible;
    }

    void Overlay::setAlignment(Qt::Alignment alignment) {
      _frame->setAlignment(alignment);
    }

    bool Overlay::setProperty(const char* name, const QVariant& value) {
      return _frame->setProperty(name, value);
    }

    void Overlay::show() {
      emit opening();

      _visible = true;

      _frame->setParent(_parent);
      _frame->resize(_parent->size());

      _fadeEffect->fadeIn();
    }

    void Overlay::hide() {
      _fadeEffect->fadeOut();
      _visible = false;
    }

    void Overlay::resize(const QSize& size) {
      _frame->resize(size);
    }

    bool Overlay::eventFilter(QObject* obj, QEvent* event) {
      if (isVisible()) {
        auto eventType = event->type();

        if (eventType == QEvent::KeyRelease) {
          auto keyEvent = static_cast<QKeyEvent*>(event);

          if (keyEvent->key() == Qt::Key_Escape) {
            hide();

            return true;
          }
        } else if (eventType == QEvent::Resize) {
          resize(parent()->size());
        } else if (eventType == QEvent::MouseButtonPress) {
          if (obj == _frame) {
            hide();

            return true;
          }
        }
      }

      return QObject::eventFilter(obj, event);
    }
  }  // namespace Layout
}  // namespace CodeLineX
