/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_FLOATING_H_
#define LAYOUTS_FLOATING_H_

#include <QObject>

class QEvent;
class QWidget;
class KateFadeEffect;

namespace CodeLineX {
  namespace Layout {
    class Floating: public QObject {
      Q_OBJECT

      QWidget* _parent = nullptr;
      QWidget* _widget = nullptr;
      KateFadeEffect* _fadeEffect = nullptr;

      bool _visible = false;

      public:
        explicit Floating(QWidget* parent);
        ~Floating() override;

        QWidget* parent() const;
        QWidget* widget() const;
        void setWidget(QWidget*);

        bool isVisible() const;

      signals:
        void opening();
        void opened();
        void closed();

      public slots:
        virtual void show();
        virtual void hide();

      protected:
        virtual void onParentResize(const QSize&);

      protected:
        bool eventFilter(QObject* obj, QEvent* event) override;
    };
  }  // namespace Layout
}  // namespace CodeLineX

#endif  // LAYOUTS_FLOATING_H_
