/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "layouts/Shell.h"

#include <QTimer>
#include <QObject>
#include <QSplitter>
#include <QVBoxLayout>
#include <QFileSystemModel>

#include <hpx-tabview/TabviewItem.h>
#include <hpx-tabview/TabviewModel.h>

#include <hpx-stacked-panel/StackedPanel.h>
#include <hpx-stacked-panel/StackedPanelItem.h>
#include <hpx-stacked-panel/StackedPanelModel.h>

#include "core/StateSession.h"
#include "components/ExplorerWidget.h"
#include "components/OutlineWidget.h"
#include "components/Document.h"
#include "components/UnloadDocument.h"
#include "components/StatusBar.h"
#include "components/EditorTabview.h"
#include "components/OpenDocumentsWidget.h"
#include "components/SidebarWidget.h"
#include "layouts/EditorLayout.h"

#include "models/FileListModel.h"
#include "models/OpenDocumentListModel.h"

#include "lib/lspclient/lspclientserver.h"
#include "lib/lspclient/lspclientservermanager.h"

static const QString CONFIG_KEY_SHELL_STATE = QStringLiteral("Shell State");
static const QString CONFIG_KEY_LAST_FOCUSED_DOCUMENT = QStringLiteral("Last Focused Document");
static const QString CONFIG_KEY_CHILDREN = QStringLiteral("Children");

static const QString CONFIG_KEY_LEFT_SIDEBAR_VISIBILITY = QStringLiteral("Left Sidebar Visibility");
static const QString CONFIG_KEY_RIGHT_SIDEBAR_VISIBILITY = QStringLiteral("Right Sidebar Visibility");

static const QString OBJECT_NAME_LEFTSIDEBAR = QStringLiteral("LeftSidebar");
static const QString OBJECT_NAME_RIGHTSIDEBAR = QStringLiteral("RightSidebar");

static const int DEFAULT_EXPLORER_WIDTH = 200;

namespace CodeLineX {
  namespace Layout {
    Shell::Shell(QWidget* parent): QFrame(parent), Core::StateSession(metaObject()) {
      setProperty("Shell", true);

      _layout = new QVBoxLayout(this);
      _splitter = new QSplitter(this);

      _fileSystemModel.reset(new QFileSystemModel(this));
      _openDocumentListModel.reset(new OpenDocumentListModel(this));

      _currentEditorLayout = new EditorLayout(this);
      _statusbar = new StatusBar(this);
      _switchFileModel = new FileListModel(this);

      _lspManager = LSPClientServerManager::instance(this);

      _leftSidebar = createSidebar(OBJECT_NAME_LEFTSIDEBAR);
      _rightSidebar = createSidebar(OBJECT_NAME_RIGHTSIDEBAR);

      _mainEditorLayout = _currentEditorLayout;

      _leftSidebar->setCurrentItem(SidebarWidget::Item::Projects);
      _rightSidebar->setCurrentItem(SidebarWidget::Item::Outline);

      _splitter->addWidget(_leftSidebar);
      _splitter->addWidget(_currentEditorLayout);
      _splitter->addWidget(_rightSidebar);

      _splitter->setProperty("ShellLayout", true);
      _splitter->setCollapsible(0, false);
      _splitter->setCollapsible(2, false);

      _layout->setMargin(0);
      _layout->setSpacing(0);
      _layout->addWidget(_splitter, 1);
      _layout->addWidget(_statusbar);

      connect(_statusbar, &StatusBar::triggerCopyFilePathContextMenu,
              this, &Shell::triggerCopyFilePathContextMenu);

      connect(_mainEditorLayout, &EditorLayout::closed, [this]() {
        _mainEditorLayout->show();
        _mainEditorLayout->splitView(_mainEditorLayout->orientation());
      });

      connectEditorLayout(_currentEditorLayout);
    }

    void Shell::addProject(const QString& path) {
      _leftSidebar->explorer()->setRootPath(path);
      _rightSidebar->explorer()->setRootPath(path);
    }

    QSharedPointer<LSPClientServerManager> Shell::lspManager() const {
      return _lspManager;
    }

    ExplorerWidget* Shell::explorer() const {
      return _leftSidebar->explorer();
    }

    StatusBar* Shell::statusbar() const {
      return  _statusbar;
    }

    EditorTabview* Shell::view(const QString& viewId) const {
      return _views[viewId];
    }

    QList<EditorTabview*> Shell::views() const {
      return _views.values();
    }

    Document* Shell::document(const QString& documentId) const {
      for (EditorTabview* view: _views) {
        Document* document = view->document(documentId);

        if (document) {
          return document;
        }
      }

      return nullptr;
    }

    QList<Document*> Shell::documents() const {
      QList<Document*> docs;

      for (EditorTabview* view: _views) {
        for (Document* document: view->documents()) {
          docs << document;
        }
      }

      return docs;
    }

    EditorLayout* Shell::currentEditorLayout() const {
      return _currentEditorLayout;
    }

    EditorTabview* Shell::currentView() const {
      return _currentEditorLayout->currentView();
    }

    Document* Shell::currentDocument() const {
      EditorLayout* localCurrentEditorLayout = currentEditorLayout();

      if (localCurrentEditorLayout) {
        EditorTabview* currentView = localCurrentEditorLayout->currentView();

        if (currentView) {
          return currentView->currentDocument();
        }
      }

      return nullptr;
    }

    FileListModel* Shell::switchFileModel() const {
      return _switchFileModel;
    }

    bool Shell::leftSidebarVisibility() const {
      return _leftSidebar->isVisible();
    }

    bool Shell::rightSidebarVisibility() const {
      return _rightSidebar->isVisible();
    }

    void Shell::setCurrentDocument(const QString& documentOid) {
      for (EditorTabview* view: _views) {
        HPX::TabviewWidget* widget = view->widget(documentOid);

        if (widget) {
          widget->setFocus();

          // Update the widget pointer
          widget = view->widget(documentOid);

          // Retrieve the document pointer
          auto document = qobject_cast<Document*>(widget);

          if (document) {
            view->setCurrentWidget(document);
            emit currentDocumentChanged(document);
          }

          break;
        }
      }
    }

    void Shell::setLeftSidebarVisibility(bool visible) {
      _leftSidebar->setVisible(visible);
    }

    void Shell::setRightSidebarVisibility(bool visible) {
      _rightSidebar->setVisible(visible);
    }

    void Shell::closeDocuments() {
      _switchFileModel->clear();
      _mainEditorLayout->close();
      _views.clear();
      _lspManager.reset();
    }

    void Shell::splitView(Qt::Orientation orientation) {
      _currentEditorLayout->splitView(orientation);
    }

    SidebarWidget* Shell::createSidebar(const QString& objectName) {
      auto sidebar = new SidebarWidget(_lspManager, this);

      sidebar->setObjectName(objectName);
      sidebar->setMinimumWidth(150);
      sidebar->setMaximumWidth(500);

      sidebar->setFilesystemModel(_fileSystemModel);
      sidebar->setOpenDocumentListModel(_openDocumentListModel);

      connect(sidebar, &SidebarWidget::fileContextMenuRequested, this, &Shell::triggerFileContextMenu);
      connect(sidebar, &SidebarWidget::fileCloseRequested, this, &Shell::fileCloseRequested);

      connect(sidebar, &SidebarWidget::currentFileChanged, [this](const QUrl& url) {
        currentView()->openDocument(url);
      });

      connect(sidebar->explorer(), &ExplorerWidget::rootDirectoryChanged,
              _statusbar, &StatusBar::setCurrentProjectDir);

      connect(sidebar, &SidebarWidget::currentDocumentIdChanged,
              this, &Shell::setCurrentDocument);

      return sidebar;
    }

    void Shell::connectSidebars(EditorTabview* view) {
      connect(view, SIGNAL(documentCreated(Document*)),
              _openDocumentListModel.data(), SLOT(appendRow(Document*)));

      connect(view, SIGNAL(unloadDocumentCreated(UnloadDocument*)),
              _openDocumentListModel.data(), SLOT(appendRow(UnloadDocument*)));

      connect(view, &EditorTabview::documentCreated, this, [this](Document* document) {
        connect(document, &Document::closed, this, [this, document]() {
          document->disconnect(this);
          _openDocumentListModel->removeRow(document);
        });

        connect(document, &Document::focusIn, this, [this, document]() {
          _leftSidebar->setCurrentDocument(document);
          _rightSidebar->setCurrentDocument(document);
        });
      });
    }

    void Shell::connectEditorLayout(EditorLayout* editorLayout) {
      connect(editorLayout, &EditorLayout::currentLayoutChanged, this, [this](EditorLayout* editorLayout) {
        _currentEditorLayout = editorLayout;
      });

      connect(editorLayout, &EditorLayout::viewCreated, this, [this, editorLayout](EditorTabview* view) {
        QDir basedir = _leftSidebar->explorer()->rootDirectory();

        basedir.cdUp();

        _views.insert(view->objectId(), view);

        _currentEditorLayout = editorLayout;

        connectSidebars(view);

        connect(view, &EditorTabview::triggerFileContextMenuRequest, this, &Shell::triggerFileContextMenu);

        connect(view, &EditorTabview::documentCreated, _statusbar, &StatusBar::onNewDocumentAdded);

        connect(view, &EditorTabview::unloadDocumentCreated, this, [this, basedir](UnloadDocument* unloadDocument) {
          _switchFileModel->appendRow(unloadDocument, basedir);
        });

        connect(view, &EditorTabview::tabviewItemCreated, this, [this, basedir](HPX::TabviewItem* item) {
          _switchFileModel->appendRow(item, basedir);
        });

        connect(view, &EditorTabview::documentCreated, this, [this](Document* document) {
          connect(document, &Document::focusIn, this, [this, document]() {
            emit currentDocumentChanged(document);
          });

          connect(document, &Document::closed, this, [this, document]() {
            onDocumentClose(document);
          });
        });

        connect(view, &EditorTabview::closed, this, [this, view]() {
          view->disconnect(this);
          _views.remove(view->objectId());
        });
      });

      connect(editorLayout, &EditorLayout::viewCreated, this, &Shell::viewCreated);
    }

    void Shell::onDocumentClose(Document* document) {
      document->disconnect(this);

      // select the next document
      if (_switchFileModel->rowCount()) {
        QStandardItem* item = _switchFileModel->item(0, FileItem::Role::OID);
        const QString nextDocumentId = item->text();
        Document* nextDocument = this->document(nextDocumentId);

        nextDocument->setFocus();
      } else {
        _currentEditorLayout = _mainEditorLayout;
      }
    }

    void Shell::fileCloseRequested(const QString& oid) {
      auto doc = document(oid);

      if (doc) {
        doc->close();
      }
    }

    bool Shell::_readStateConfig(const KConfigGroup& config) {
      const QByteArray shellState = config.readEntry(CONFIG_KEY_SHELL_STATE, QByteArray());
      const QString lastFocusedDocumentId = config.readEntry(CONFIG_KEY_LAST_FOCUSED_DOCUMENT, "");

      if (shellState.isEmpty()) {
        _splitter->setSizes({ DEFAULT_EXPLORER_WIDTH, width() - DEFAULT_EXPLORER_WIDTH });
      } else {
        _splitter->restoreState(shellState);
      }

      restoreChildrenState(config);

      QTimer::singleShot(300, this, [this, lastFocusedDocumentId]() {
        // Ensure all views release the focus
        for (EditorTabview* view: _views.values()) {
          view->setFocus();
        }

        // focus the lastest focused document
        if (!lastFocusedDocumentId.isEmpty()) {
          Document* lastFocusedDocument = this->document(lastFocusedDocumentId);

          if (lastFocusedDocument) {
            lastFocusedDocument->setFocus();
          }
        }
      });

      return true;
    }

    bool Shell::_writeStateConfig(KConfigGroup& config) {
      Document* localCurrentDocument = currentDocument();

      if (localCurrentDocument && !localCurrentDocument->url().isEmpty()) {
        config.writeEntry(CONFIG_KEY_LAST_FOCUSED_DOCUMENT, localCurrentDocument->objectId());
      }

      config.writeEntry(CONFIG_KEY_SHELL_STATE, _splitter->saveState());

      return writeChildrenState(config);
    }

    bool Shell::writeChildrenState(KConfigGroup& config) const {
      QStringList children;

      if (_leftSidebar->writeStateConfig(config)) {
        config.writeEntry(CONFIG_KEY_LEFT_SIDEBAR_VISIBILITY, _leftSidebar->isVisible());
        children.push_back(_leftSidebar->objectId());
      }

      if (_mainEditorLayout->writeStateConfig(config)) {
        children.push_back(_mainEditorLayout->objectId());
      }

      if (_rightSidebar->writeStateConfig(config)) {
        config.writeEntry(CONFIG_KEY_RIGHT_SIDEBAR_VISIBILITY, _rightSidebar->isVisible());
        children.push_back(_rightSidebar->objectId());
      }

      if (_statusbar->writeStateConfig(config)) {
        children.push_back(_statusbar->objectId());
      }

      if (!children.isEmpty()) {
        config.writeEntry(CONFIG_KEY_CHILDREN, children);

        return true;
      }

      return false;
    }

    void Shell::restoreChildrenState(const KConfigGroup& group) {
      for (const QString& oid: group.readEntry(CONFIG_KEY_CHILDREN, QStringList())) {
        if (Core::StateSession::getClassName(group.group(oid)) == _leftSidebar->className()) {
          SidebarWidget* sidebar = Core::StateSession::getObjectName(group.group(oid)) == OBJECT_NAME_LEFTSIDEBAR
            ? _leftSidebar
            : _rightSidebar;

          auto SIDEBAR_VISIBILITY = sidebar == _leftSidebar
            ? CONFIG_KEY_LEFT_SIDEBAR_VISIBILITY
            : CONFIG_KEY_RIGHT_SIDEBAR_VISIBILITY;

          auto visibility = group.readEntry(SIDEBAR_VISIBILITY, true);

          sidebar->readStateConfig(oid, group);
          sidebar->setVisible(visibility);

          continue;
        }

        _mainEditorLayout->readStateConfig(oid, group);
      }

      if (_mainEditorLayout->count() == 0) {
        splitView(Qt::Horizontal);
      }

      emit ready();
    }
  }  // namespace Layout
}  // namespace CodeLineX
