/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_OVERLAYFRAME_H_
#define LAYOUTS_OVERLAYFRAME_H_

#include <QWidget>

class QFrame;
class QVBoxLayout;
class QGridLayout;
class QApplication;
class KateFadeEffect;

namespace CodeLineX {
  namespace Layout {
    class OverlayFrame: public QWidget {
      QGridLayout* _layout = nullptr;
      QFrame* _container = nullptr;
      QVBoxLayout* _containerLayout = nullptr;

      public:
        OverlayFrame();
        ~OverlayFrame() override;

        void addWidget(QWidget*);
        void addStretch(int stretch = 0);
        void setAlignment(Qt::Alignment);
    };
  }  // namespace Layout
}  // namespace CodeLineX

#endif  // LAYOUTS_OVERLAYFRAME_H_
