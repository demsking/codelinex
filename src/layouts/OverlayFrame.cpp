/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "layouts/OverlayFrame.h"

#include <QFrame>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QApplication>

#include "effects/KateFadeEffect.h"

namespace CodeLineX {
  namespace Layout {
    OverlayFrame::OverlayFrame(): QWidget(nullptr) {
      setWindowFlags(Qt::Sheet | Qt::FramelessWindowHint | Qt::Dialog | Qt::WindowStaysOnTopHint);
      setProperty("Overlay", true);

      _containerLayout = new QVBoxLayout;
      _containerLayout->setMargin(0);
      _containerLayout->setSpacing(0);

      _container = new QFrame(this);
      _container->setProperty("OverlayFrame", true);
      _container->setLayout(_containerLayout);
      _container->adjustSize();

      _layout = new QGridLayout(this);
      _layout->setMargin(0);
      _layout->setSpacing(0);
      _layout->addWidget(_container, 0, 0);
      _layout->setAlignment(_container, Qt::AlignCenter | Qt::AlignVCenter);
    }

    OverlayFrame::~OverlayFrame() {
      delete _containerLayout;
      delete _container;
      delete _layout;
    }

    void OverlayFrame::addWidget(QWidget* widget) {
      _containerLayout->addWidget(widget);
    }

    void OverlayFrame::addStretch(int stretch) {
      _containerLayout->addStretch(stretch);
    }

    void OverlayFrame::setAlignment(Qt::Alignment alignment) {
      _layout->setAlignment(_container, alignment);
    }
  }  // namespace Layout
}  // namespace CodeLineX
