/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "layouts/Floating.h"

#include <QWidget>
#include <QKeyEvent>

#include "effects/KateFadeEffect.h"

namespace CodeLineX {
  namespace Layout {
    Floating::Floating(QWidget* parent)
      : QObject(nullptr),
        _parent(parent) {}

    Floating::~Floating() {
      if (_fadeEffect) {
        // TODO(demsking) _fadeEffect deletion does not works
        // _fadeEffect->deleteLater();
      }
    }

    QWidget* Floating::parent() const {
      return _parent;
    }

    QWidget* Floating::widget() const {
      return _widget;
    }

    void Floating::setWidget(QWidget* widget) {
      if (_fadeEffect) {
        delete _fadeEffect;
      }

      _widget = widget;
      _fadeEffect = new KateFadeEffect(widget);

      connect(_fadeEffect, &KateFadeEffect::showAnimationFinished, this, &Floating::opened);
      connect(_fadeEffect, &KateFadeEffect::hideAnimationFinished, this, &Floating::closed);
    }

    bool Floating::isVisible() const {
      return _visible;
    }

    void Floating::show() {
      emit opening();

      _visible = true;

      onParentResize(_parent->size());

      _fadeEffect->fadeIn();

      if (_widget) {
        _widget->setFocus();
      }
    }

    void Floating::hide() {
      _fadeEffect->fadeOut();
      _visible = false;
    }

    void Floating::onParentResize(const QSize& size) {
      if (_widget) {
        _widget->resize(size);
      }
    }

    bool Floating::eventFilter(QObject* obj, QEvent* event) {
      if (isVisible()) {
        auto eventType = event->type();

        if (eventType == QEvent::KeyRelease) {
          auto keyEvent = static_cast<QKeyEvent*>(event);

          if (keyEvent->key() == Qt::Key_Escape) {
            hide();

            return true;
          }
        } else if (eventType == QEvent::Resize) {
          onParentResize(_parent->size());
        }

        if (_widget) {
          return _widget->eventFilter(obj, event);
        }
      }

      return QObject::eventFilter(obj, event);
    }
  }  // namespace Layout
}  // namespace CodeLineX
