/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTS_SHELL_H_
#define LAYOUTS_SHELL_H_

#include <QList>
#include <QMap>
#include <QHash>
#include <QFrame>

#include "core/StateSession.h"

class QWidget;
class QSplitter;
class QVBoxLayout;
class QFileSystemModel;

class KConfigGroup;
class LSPClientServer;
class LSPClientServerManager;

namespace HPX {
  class StackedPanelModel;
}

namespace CodeLineX {
  class ExplorerWidget;
  class Document;
  class StatusBar;
  class EditorTabview;
  class EditorLayout;
  class FileListModel;
  class SidebarWidget;
  class OpenDocumentListModel;

  namespace Layout {
    class Shell: public QFrame, public Core::StateSession {
      Q_OBJECT

      QVBoxLayout* _layout = nullptr;
      QSplitter* _splitter = nullptr;

      QSharedPointer<QFileSystemModel> _fileSystemModel;
      QSharedPointer<OpenDocumentListModel> _openDocumentListModel;

      SidebarWidget* _leftSidebar = nullptr;
      SidebarWidget* _rightSidebar = nullptr;
      EditorLayout* _mainEditorLayout = nullptr;
      EditorLayout* _currentEditorLayout = nullptr;
      StatusBar* _statusbar = nullptr;

      QHash<QString, EditorTabview*> _views;

      Document* _currentDocument = nullptr;

      FileListModel* _switchFileModel = nullptr;

      QSharedPointer<LSPClientServerManager> _lspManager;

      public:
        explicit Shell(QWidget* parent = nullptr);

        void addProject(const QString& projectPath);

        QSharedPointer<LSPClientServerManager> lspManager() const;

        ExplorerWidget* explorer() const;
        StatusBar* statusbar() const;
        EditorTabview* view(const QString& viewId) const;
        QList<EditorTabview*> views() const;
        Document* document(const QString& documentId) const;
        QList<Document*> documents() const;

        EditorLayout* currentEditorLayout() const;
        EditorTabview* currentView() const;
        Document* currentDocument() const;

        FileListModel* switchFileModel() const;

        bool leftSidebarVisibility() const;
        bool rightSidebarVisibility() const;

      signals:
        void ready();
        void viewCreated(EditorTabview*);
        void currentDocumentChanged(Document*);
        void triggerFileContextMenu(const QPoint&, const QUrl&);
        void triggerCopyFilePathContextMenu(const QPoint&, const QUrl&);

      public slots:
        void setCurrentDocument(const QString& documentOid);
        void setLeftSidebarVisibility(bool visible);
        void setRightSidebarVisibility(bool visible);

      public slots:
        void closeDocuments();

      public:
        void splitView(Qt::Orientation);

      protected:
        SidebarWidget* createSidebar(const QString& objectName);
        void connectSidebars(EditorTabview*);
        void connectEditorLayout(EditorLayout*);

      protected slots:
        void onDocumentClose(Document*);
        void fileCloseRequested(const QString& oid);

      protected:
        bool _readStateConfig(const KConfigGroup&) override;
        bool _writeStateConfig(KConfigGroup&) override;

      protected:
        bool writeChildrenState(KConfigGroup&) const;
        void restoreChildrenState(const KConfigGroup&);
    };
  }  // namespace Layout
}  // namespace CodeLineX

#endif  // LAYOUTS_SHELL_H_
