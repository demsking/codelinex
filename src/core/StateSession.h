﻿/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_STATESESSION_H_
#define CORE_STATESESSION_H_

#include <KConfigGroup>
#include <KSharedConfig>

#include "core/Object.h"

namespace CodeLineX {
  namespace Core {
    class StateSession: public Object {
      protected:
        using Object::Object;

        virtual ~StateSession() = default;

        static QString getObjectId(const KConfigGroup& config);
        static QString getObjectName(const KConfigGroup& config);
        static QString getClassName(const KConfigGroup& config);

      public:
        bool readStateConfig(const QString& oid, KSharedConfigPtr config);
        bool readStateConfig(KSharedConfigPtr config);
        bool readStateConfig(const KConfigGroup& parent);
        bool readStateConfig(const QString& oid, const KConfigGroup& parent);

        bool writeStateConfig(KSharedConfigPtr config);
        bool writeStateConfig(KConfigGroup& parent);  // NOLINT(runtime/references)

        KConfigGroup group(const KConfigGroup& parent) const;

      protected:
        void resetObjectId();

      protected:
        virtual bool _readStateConfig(const KConfigGroup&) = 0;
        virtual bool _writeStateConfig(KConfigGroup&) = 0;
    };
  }  // namespace Core
}  // namespace CodeLineX

#endif  // CORE_STATESESSION_H_
