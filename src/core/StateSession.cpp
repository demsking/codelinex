/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core/StateSession.h"

#include <QWidget>

static const QString CONFIG_KEY_OBJECT_ID = QStringLiteral("Object ID");
static const QString CONFIG_KEY_CLASS_NAME = QStringLiteral("Class Name");
static const QString CONFIG_KEY_OBJECT_NAME = QStringLiteral("Object Name");

namespace CodeLineX {
  namespace Core {
    QString StateSession::getObjectId(const KConfigGroup& config) {
      return config.readEntry(CONFIG_KEY_OBJECT_ID, "");
    }

    QString StateSession::getObjectName(const KConfigGroup& config) {
      return config.readEntry(CONFIG_KEY_OBJECT_NAME, "");
    }

    QString StateSession::getClassName(const KConfigGroup& config) {
      return config.readEntry(CONFIG_KEY_CLASS_NAME, "");
    }

    bool StateSession::readStateConfig(const QString& oid, KSharedConfigPtr config) {
      return readStateConfig(config->group(oid));
    }

    bool StateSession::readStateConfig(KSharedConfigPtr config) {
      return readStateConfig(objectId(), config);
    }

    bool StateSession::readStateConfig(const KConfigGroup& parent) {
      auto group = this->group(parent);

      return _readStateConfig(group);
    }

    bool StateSession::readStateConfig(const QString& oid, const KConfigGroup& parent) {
      _objectId = oid;

      auto widget = dynamic_cast<QWidget*>(this);

      if (widget) {
        auto group = this->group(parent);
        auto name = group.readEntry(CONFIG_KEY_OBJECT_NAME);

        if (!name.isEmpty()) {
          widget->setObjectName(name);
        }
      }

      return readStateConfig(parent);
    }

    bool StateSession::writeStateConfig(KSharedConfigPtr config) {
      KConfigGroup group = config->group(objectId());

      group.deleteGroup();

      return writeStateConfig(group);
    }

    bool StateSession::writeStateConfig(KConfigGroup& parent) {
      KConfigGroup group = this->group(parent);

      if (_writeStateConfig(group)) {
        group.writeEntry(CONFIG_KEY_OBJECT_ID, objectId());
        group.writeEntry(CONFIG_KEY_CLASS_NAME, className());

        auto widget = dynamic_cast<QWidget*>(this);

        if (widget) {
          group.writeEntry(CONFIG_KEY_OBJECT_NAME, widget->objectName());
        }

        return true;
      }

      return false;
    }

    KConfigGroup StateSession::group(const KConfigGroup& parent) const {
      return parent.name() == objectId() ? parent : parent.group(objectId());
    }

    void StateSession::resetObjectId() {
      _objectId = Core::Object::uniqueId();
    }
  }  // namespace Core
}  // namespace CodeLineX
