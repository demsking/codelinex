/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_OBJECT_H_
#define CORE_OBJECT_H_

#include <QString>

class QWidget;

namespace CodeLineX {
  namespace Core {
    class Object {
      protected:
        QString _objectId;
        QString _className;

      public:
        Object(const Object&) = default;
        Object(const QString& oid, const QString& className);

      protected:
        Object(const QString& oid, const QMetaObject* metaObject);
        explicit Object(const QMetaObject* metaObject);

        template<class T>
        static T* get(const QString& oid, QWidget* parent = nullptr) {
          T* ref = new T(parent);
          auto object = dynamic_cast<Object*>(ref);

          if (object) {
            object->_objectId = oid;
          }

          return ref;
        }

      public:
        static QString uniqueId();

        virtual ~Object() = default;

        QString objectId() const;
        QString className() const;
    };
  }  // namespace Core
}  // namespace CodeLineX

#endif  // CORE_OBJECT_H_
