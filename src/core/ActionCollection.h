/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_ACTIONCOLLECTION_H_
#define CORE_ACTIONCOLLECTION_H_

#include <KActionCollection>

namespace CodeLineX {
  namespace Core {
    class ActionCollection: public KActionCollection {
      public:
        using KActionCollection::KActionCollection;

        /**
         * Set enabled value to actions
         *
         * @param names Names of actions to update
         * @param enabled Enabled value to apply
         */
        void setEnabledActions(const QStringList& names, bool enabled = true);

        /**
         * Set disabled value to actions
         *
         * @param names Names of actions to update
         * @param disabled Disabled value to apply
         */
        void setDisabledActions(const QStringList& names, bool disabled = true);
    };
  }  // namespace Core
}  // namespace CodeLineX

#endif // CORE_ACTIONCOLLECTION_H_
