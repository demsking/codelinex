/**
 * CodeLineX - The powerful lightweight code editor
 *
 * Copyright (C) 2018  Sébastien Demanou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core/Object.h"

#include <QUuid>
#include <QWidget>
#include <QMetaObject>

namespace CodeLineX {
  namespace Core {
    Object::Object(const QString& oid, const QString& className)
      : _objectId(oid),
        _className(className) {}

    Object::Object(const QString& oid, const QMetaObject* metaObject)
      : Object(oid, metaObject->className()) {}

    Object::Object(const QMetaObject* metaObject)
      : Object(Object::uniqueId(), metaObject) {}

    QString Object::uniqueId() {
      auto oid = QUuid::createUuid().toString().toStdString();

      return oid.substr(1, oid.length() - 2).c_str();
    }

    QString Object::objectId() const {
      return _objectId;
    }

    QString Object::className() const {
      return _className;
    }
  }  // namespace Core
}  // namespace CodeLineX

